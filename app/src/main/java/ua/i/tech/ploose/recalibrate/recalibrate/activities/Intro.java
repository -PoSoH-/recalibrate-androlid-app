package ua.i.tech.ploose.recalibrate.recalibrate.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.LinkedList;
import java.util.List;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.IntroPageAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.wheelAdapters.UserLoginAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogStart;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogTrainingSetup;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomStringListener;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnDialogFragmentListener;
import ua.i.tech.ploose.recalibrate.recalibrate.models.ProgramInfo;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

public class Intro extends Base implements OnDialogFragmentListener, CustomStringListener {

    private final String TAG = Intro.class.getSimpleName() + " : ";
    public static final String INFO_SCREEN = "DATA.INFO.SCREEN";

    private FrameLayout []circleIndicator;

    private FirebaseDatabase database;
    private DatabaseReference dataRef;

    private FirebaseAuth auth;
    private FirebaseAuth.AuthStateListener authListener;

    private RelativeLayout hiScreen;
    private ScrollView loginSection;
    private RelativeLayout settingsScreen;
    private RelativeLayout settingsWorkout;
    private LinearLayout workoutContainer;

    private TextView btnGetStartHiScreen;

    private WheelView wheelGoal;
    private WheelView wheelLevel;

    private EditText personData[];

    private UserCheckIn userCheckIn;

    private List<UserTraining> workoutsList = new LinkedList<>();
    private List<UserCheckIn> userCheckIns = new LinkedList<>();
    private List<LinearLayout> workoutViewCollection = new LinkedList<>();
    private String [] goalValue; // = new ArrayList<>();
    private String [] levelValue;
    private int goalPosition = 0;
    private int levelPosition = 0;

    private EditText [] registrationInfo;
    private TextView [] genderSelection;
    private TextView btnChangeRegistered;
    private boolean isRegistered = false;
    private TextView btnRegistration;

    private RelativeLayout firstNameView;
    private RelativeLayout lastNameView;
    private LinearLayout selectedGenderView;
    private RelativeLayout confirmationView;

    private ProgramInfo information = null;
    private User currentUser = new User();

    private int currentView = -1;

    public static void show(AppCompatActivity activity, ProgramInfo information){
        Intent intent = new Intent(activity, Intro.class);
        intent.putExtra(INFO_SCREEN, information);
        if(information != null) {
            Recalibrate.Preferences.setStringPreferences(activity,
                    information.getCheckIn(),
                    Recalibrate.Preferences.TEXT_CHECK_IN);
            Recalibrate.Preferences.setStringPreferences(activity,
                    information.getTrackProgress(),
                    Recalibrate.Preferences.TEXT_TRACK_PROGRESS);
            Recalibrate.Preferences.setStringPreferences(activity,
                    information.getReviewPerformance(),
                    Recalibrate.Preferences.TEXT_PREVIEW_PERFORMANCE);
            Recalibrate.Preferences.setStringPreferences(activity,
                    information.getAbout(),
                    Recalibrate.Preferences.TEXT_ABOUT);
            Recalibrate.Preferences.setStringPreferences(activity,
                    information.getTermsConditions(),
                    Recalibrate.Preferences.TEXT_TERMS_CONDITIONS);
        }
        activity.startActivity(intent);
        activity.overridePendingTransition(R.anim._show_object_animation
                , R.anim._hide_object_animation);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_intro);
        setLabel(R.string.intro_label);
        information = (ProgramInfo) getIntent().getSerializableExtra(INFO_SCREEN);
        ViewPager vp = (ViewPager) findViewById(R.id.introVP);
        IntroPageAdapter adapter = new IntroPageAdapter(getSupportFragmentManager());
        vp.setAdapter(adapter);
        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onPageSelected(int position) {
                hideIndicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        registrationInfo = new EditText[]{
                (EditText) findViewById(R.id.introUserMail),       // pos 0
                (EditText) findViewById(R.id.introUserPassword),   // pos 1
                (EditText) findViewById(R.id.introUserConfirm),    // pos 2

                (EditText) findViewById(R.id.introUserFirstName),  // pos 3
                (EditText) findViewById(R.id.introUserLastName)    // pos 4
        };

        genderSelection = new TextView[]{
                (TextView) findViewById(R.id.introButtonMan),  // pos male position
                (TextView) findViewById(R.id.introButtonWoman) // pos female position
        };

        btnChangeRegistered = (TextView) findViewById(R.id.introButtonChange);
        btnRegistration = (TextView) findViewById(R.id.introBntRegistration);
        confirmationView = (RelativeLayout) findViewById(R.id.introBlockUserConfirm);
        firstNameView = (RelativeLayout) findViewById(R.id.introBlockUserFirsName);
        lastNameView = (RelativeLayout) findViewById(R.id.introBlockUserLastName);
        selectedGenderView = (LinearLayout) findViewById(R.id.introBlockUserGender);

        circleIndicator = new FrameLayout[]{
                (FrameLayout) findViewById(R.id.introF),
                (FrameLayout) findViewById(R.id.introS),
                (FrameLayout) findViewById(R.id.introT),
        };

        hiScreen = (RelativeLayout) findViewById(R.id.introHiScreen);
        loginSection = (ScrollView) findViewById(R.id.intro_log_reg_scroll);
//        loginScreen = (RelativeLayout) findViewById(R.id.intro_log_reg_block);
        settingsScreen = (RelativeLayout) findViewById(R.id.introSettingsView);
        btnGetStartHiScreen = (TextView) findViewById(R.id.btnIntroStart);

        String[] goalData = getResources().getStringArray(R.array.intro_settings_collection_goal);
        goalValue = getResources().getStringArray(R.array.intro_settings_collection_goal_value);
        String[] levelData = getResources().getStringArray(R.array.intro_settings_collection_level);
        levelValue = getResources().getStringArray(R.array.intro_settings_collection_level_value);

        wheelGoal = (WheelView) findViewById(R.id.introWheelGoal);
        wheelGoal.disableShadows();
        wheelLevel = (WheelView) findViewById(R.id.introWheelLevel);
        wheelLevel.disableShadows();
        initGoal(wheelGoal, goalData);
        initLevel(wheelLevel, levelData);

        personData = new EditText[]{
                (EditText) findViewById(R.id.introSettingsAge),
                (EditText) findViewById(R.id.introSettingsWeight),
                (EditText) findViewById(R.id.introSettingsHeight),
                (EditText) findViewById(R.id.introSettingsNeck),
                (EditText) findViewById(R.id.introSettingsChest),
                (EditText) findViewById(R.id.introSettingsWaist),
                (EditText) findViewById(R.id.introSettingsHips),
                (EditText) findViewById(R.id.introSettingsThighs)
        };

        settingsWorkout = (RelativeLayout) findViewById(R.id.introSettingsWorkoutSelect);
        TextView saveWorkout = (TextView) findViewById(R.id.introBntWorkoutSave);

        TextView addWorkout = (TextView) findViewById(R.id.inputWorkoutSectionAdd);
        addWorkout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DialogSettings.show(Intro.this);
                DialogTrainingSetup.show(Intro.this);
            }
        });

        TextView btnSave = (TextView) findViewById(R.id.introBntSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(changeInputData()) {

                    showProgressBar();

//                    currentUser.setGoal(userCheckIn.getGoal());
//                    currentUser.setLevel(userCheckIn.getLevel());
                    currentUser.setId(Recalibrate.Helper.createObjectId());
                    userCheckIn.setGoal(currentUser.getGoal());
                    Recalibrate.Calc.calcScore(getApplicationContext()
                            , workoutsList
                            , currentUser);
                    Recalibrate.Calc.calcCheckInProgress(getApplicationContext()
                            , null
                            , userCheckIn
                            , currentUser);
                    Recalibrate.Calc.calcDeficitSurplus(getApplicationContext()
                            , userCheckIn
                            , currentUser);
                    Recalibrate.Calc.calcBMR(getApplicationContext()
                            , userCheckIn
                            , currentUser);

                    userCheckIn.setCreated(Recalibrate.Helper.createCorrectDate());
                    userCheckIn.setRecords(Recalibrate.Helper.createRecord());

                    userCheckIns.add(userCheckIn);
                    currentUser.printLogValueField();
                    userCheckIn.printfieldValueChekIn();
                    currentUser.setStatus(getString(R.string.intro_view_reg_status_complete));
                    Backend.saveCheckInObject(userCheckIns, new Backend.OnRecalibrateSaveOnSuccess() {
                        @Override
                        public void onSuccess() {
                            Recalibrate.LOG(TAG, "OnSuccess");
//                            Main.show(Intro.this);
                            MainTable.show(Intro.this, Recalibrate.KEY.ROTATE_L);
                            Backend.saveCurrentUser(currentUser);
                        }

                        @Override
                        public void onError() {
                            Toast.makeText(Intro.this, "Save error", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        userCheckIn = new UserCheckIn();
        workoutContainer = (LinearLayout) findViewById(R.id.inputContainerWorkout);

        listenerStartInit();
        listenerRegistrationInit();
        listenerRegistrationGender();

        database = FirebaseDatabase.getInstance();
        auth = FirebaseAuth.getInstance();
        authListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };


        if(TextUtils.equals(currentUser.getStatus(), "")){
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if(user != null) {
                Backend.loadUserData(new Backend.OnRecalibrateUserListener() {
                    @Override
                    public void onSuccess(User result) {
                        if (result != null) {
                            currentUser = result;
                            currentView = Recalibrate.KEY.TYPE_VIEW_S;
                            changeCurrentView();
                        } else {
                            currentView = Recalibrate.KEY.TYPE_VIEW_C;
                            changeCurrentView();
                        }
                    }

                    @Override
                    public void onError(int error) {
                        currentView = Recalibrate.KEY.TYPE_VIEW_C;
                        changeCurrentView();
                    }
                });
            }else{
                String temp = Recalibrate.Preferences
                        .getStringPreferences(this, Recalibrate.KEY.USER_REGISTERED);
                if(temp.equals(Recalibrate.KEY.IS_REGISTERED))
                    currentView = Recalibrate.KEY.TYPE_VIEW_L;
                else currentView = Recalibrate.KEY.TYPE_VIEW_C;
                changeCurrentView();
            }
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        auth.addAuthStateListener(authListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (authListener != null) {
            auth.removeAuthStateListener(authListener);
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void hideIndicator(int position){
        for(FrameLayout intro : circleIndicator)
            intro.setBackground(getResources()
                    .getDrawable(R.drawable.circle_gray_full));

        circleIndicator[position].setBackground(getResources()
                .getDrawable(R.drawable.circle_white_full));
    }

    private void changeLogo(String logo){
        setLabel(logo);
    }

    private boolean changeInputData(){

        if(!TextUtils.isEmpty(personData[0].getText())) {
            currentUser.setAge(Integer.valueOf(personData[0].getText().toString()));
        } else {
            return showErrorMessage(Recalibrate.KEY.Error.AGE);
        }

        if(!TextUtils.isEmpty(personData[1].getText())) {
            userCheckIn.setWeight(Float.valueOf(personData[1].getText().toString()));
//            userCheckIn.setWeight(Float.valueOf(personData[0].getText().toString()));
        } else {
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_weight)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
            return showErrorMessage(Recalibrate.KEY.Error.WEIGHT);
        }

        if(!TextUtils.isEmpty(personData[2].getText())) {
//            userCheckIn.setHeight(Float.valueOf(personData[0].getText().toString()));
            currentUser.setHeight(Float.valueOf(personData[2].getText().toString()));
//            userCheckIn.setWeight(Float.valueOf(personData[0].getText().toString()));
        } else {
            return showErrorMessage(Recalibrate.KEY.Error.HEIGHT);
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_height)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
        }

        if(!TextUtils.isEmpty(personData[3].getText())) {
            userCheckIn.setNeck(Float.valueOf(personData[3].getText().toString()));
//            userCheckIn.setNeck(Float.valueOf(personData[1].getText().toString()));
        } else {
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_neck)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
            return showErrorMessage(Recalibrate.KEY.Error.NECK);
        }

        if(!TextUtils.isEmpty(personData[4].getText())) {
            userCheckIn.setChest(Float.valueOf(personData[4].getText().toString()));
//            userCheckIn.setChest(Float.valueOf(personData[2].getText().toString()));
        } else {
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_chest)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
            return showErrorMessage(Recalibrate.KEY.Error.CHEST);
        }

        if(!TextUtils.isEmpty(personData[5].getText())) {
            userCheckIn.setWaist(Float.valueOf(personData[5].getText().toString()));
//            userCheckIn.setWaist(Float.valueOf(personData[3].getText().toString()));
        } else {
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_waist)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
            return showErrorMessage(Recalibrate.KEY.Error.WAIST);
        }

        if(!TextUtils.isEmpty(personData[6].getText())) {
            userCheckIn.setHips(Float.valueOf(personData[6].getText().toString()));
//            userCheckIn.setHips(Float.valueOf(personData[4].getText().toString()));
        } else {
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_hips)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
            return showErrorMessage(Recalibrate.KEY.Error.HIP);
        }

        if(!TextUtils.isEmpty(personData[7].getText())) {
            userCheckIn.setThighs(Float.valueOf(personData[7].getText().toString()));
//            userCheckIn.setThighs(Float.valueOf(personData[5].getText().toString()));
        } else {
//            Toast.makeText(getApplicationContext()
//                    , getString(R.string.intro_error_things)
//                    , Toast.LENGTH_SHORT).show();
//            return false;
            return showErrorMessage(Recalibrate.KEY.Error.THIGH);
        }

        if(goalPosition > 0) {
            currentUser.setGoal(goalValue[goalPosition]);
        } else {
            Toast.makeText(getApplicationContext()
                    , getString(R.string.intro_error_goal)
                    , Toast.LENGTH_SHORT).show();
            return false;
        }

        if(levelPosition > 0) {
            currentUser.setLevel(levelValue[levelPosition]);
        } else {
            Toast.makeText(getApplicationContext()
                    , getString(R.string.intro_error_level)
                    , Toast.LENGTH_SHORT).show();
            return false;
        }

        if(workoutsList.size() > 0) {
//            for(Workout out : workoutsList)
//                out.setUserId(userCheckIn.getUserId());
//            DAO.saveWorkoutCollection(workoutsList);
        } else {
            Toast.makeText(getApplicationContext()
                    , getString(R.string.intro_error_workout)
                    , Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void initGoal(WheelView wheelView, String []data) {
        wheelView.setWheelViewBackgroundResource(android.R.color.transparent);
        wheelView.disableShadows();
        wheelView.setVisibleItems(3);
        final UserLoginAdapter goalAdapter = new UserLoginAdapter(this,
                data,
                goalPosition);
        wheelView.setViewAdapter(goalAdapter);
        wheelView.setCurrentItem(goalAdapter.getSelectedItem());
        goalAdapter.notifyDataChangedEvent();
        wheelView.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorGray_138));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_in_focus));
                    goalPosition = newValue;
                    goalAdapter.setSelectedItem(newValue);

                } catch (NullPointerException e) {
                    Log.e("GOAL", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void initLevel(WheelView wheelView, String []data) {
        wheelView.setWheelViewBackgroundResource(android.R.color.transparent);
        wheelView.disableShadows();
        wheelView.setVisibleItems(3);
        final UserLoginAdapter levelAdapter = new UserLoginAdapter(this,
                data,
                goalPosition);
        wheelView.setViewAdapter(levelAdapter);
        wheelView.setCurrentItem(levelAdapter.getSelectedItem());
        levelAdapter.notifyDataChangedEvent();
        wheelView.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorGray_138));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_in_focus));
                    levelPosition = newValue;
                    levelAdapter.setSelectedItem(newValue);

                } catch (NullPointerException e) {
                    Log.e("LEVEL", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void listenerStartInit() {
        btnGetStartHiScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                currentView = Recalibrate.KEY.TYPE_VIEW_L;
                changeCurrentView();
            }
        });
    }

    private void listenerRegistrationInit() {
        btnChangeRegistered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isRegistered=isRegistered?false:true;
//                currentView = currentView==Recalibrate.KEY.TYPE_VIEW_L
//                        ?Recalibrate.KEY.TYPE_VIEW_R
//                        :Recalibrate.KEY.TYPE_VIEW_L;
                changeLoginRegister();
            }
        });

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                public static final String EMAIL_VALIDATION_PATTERN = "^[-\\w.]+@([A-z0-9]+\\.)+[A-z]{2,}$";

                String EMAIL_VALIDATION_PATTERN = "^[-\\w.]+@([A-z0-9]+\\.)+[A-z]{2,}$";

                if(isRegistered) {

                    if (TextUtils.isEmpty(registrationInfo[3].getText().toString())
                            || TextUtils.equals(registrationInfo[3].getText().toString(), "")) {
                        Toast.makeText(Intro.this, getString(R.string.intro_reg_fir_name), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (TextUtils.isEmpty(registrationInfo[4].getText().toString())
                            || TextUtils.equals(registrationInfo[4].getText().toString(), "")) {
                        Toast.makeText(Intro.this, getString(R.string.intro_reg_sec_name), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!String.valueOf(genderSelection[0].getCurrentTextColor())
                            .equals(String.valueOf(getResources().getColor(R.color.colorWhite)))) {
                        if (!String.valueOf(genderSelection[1].getCurrentTextColor())
                                .equals(String.valueOf(getResources().getColor(R.color.colorWhite)))) {
                            Toast.makeText(Intro.this, getString(R.string.intro_reg_gen_type), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }

                }

                if(TextUtils.isEmpty(registrationInfo[0].getText().toString())
                        || TextUtils.equals(registrationInfo[0].getText().toString(), "")){
                    Toast.makeText(Intro.this, getString(R.string.intro_reg_err_mail), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!registrationInfo[0].getText().toString().trim().matches(EMAIL_VALIDATION_PATTERN)){
                    Toast.makeText(Intro.this, getString(R.string.intro_reg_err_mail), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(TextUtils.isEmpty(registrationInfo[1].getText().toString())
                        ||TextUtils.equals(registrationInfo[1].getText().toString(), "")){
                    Toast.makeText(Intro.this, getString(R.string.intro_reg_pas_data), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(registrationInfo[1].getText().length() < 6){
                    Toast.makeText(Intro.this, getString(R.string.intro_reg_pas_count), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(isRegistered) {
                    if (TextUtils.isEmpty(registrationInfo[2].getText().toString())
                            ||TextUtils.equals(registrationInfo[2].getText().toString(), "")) {
                        Toast.makeText(Intro.this, getString(R.string.intro_reg_conf_data), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (!TextUtils.equals(registrationInfo[1].getText().toString(),
                            registrationInfo[2].getText().toString())) {
                        Toast.makeText(Intro.this, getString(R.string.intro_reg_pas_no_match), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
//                startAddUserInfo();

                if(isRegistered){
                    Recalibrate.Helper.hideKeyboard(Intro.this);
                    currentUser.setStatus(getString(R.string.intro_view_reg_status_process));
                    showProgressBar();
                    auth.createUserWithEmailAndPassword(
                            registrationInfo[0].getText().toString()
                            , registrationInfo[1].getText().toString())
                            .addOnCompleteListener(Intro.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Recalibrate.LOG(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                                    if (!task.isSuccessful()) {
                                        hideProgressBar();
                                        Recalibrate.LOG(TAG, "createUserWithEmail:failed" + " - " + task.getException());
                                        Toast.makeText(Intro.this, "Register ERROR!",
                                                Toast.LENGTH_SHORT).show();
                                    }else{
                                        isRegistered();
                                        hideProgressBar();
                                        currentView = Recalibrate.KEY.TYPE_VIEW_S;
                                        changeCurrentView();
                                        userData();

                                    }
                                }
                            });

                }else{
                    Recalibrate.Helper.hideKeyboard(Intro.this);
                    showProgressBar();
                    auth.signInWithEmailAndPassword(
                            registrationInfo[0].getText().toString()
                            , registrationInfo[1].getText().toString())
                            .addOnCompleteListener(Intro.this, new OnCompleteListener<AuthResult>() {
                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    Recalibrate.LOG(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                                    // If sign in fails, display a message to the user. If sign in succeeds
                                    // the auth state listener will be notified and logic to handle the
                                    // signed in user can be handled in the listener.
                                    if (!task.isSuccessful()) {
                                        hideProgressBar();
                                        Recalibrate.LOG(TAG, "signInWithEmail:failed" + " - " + task.getException());
                                        Toast.makeText(Intro.this, "Login Error",
                                                Toast.LENGTH_SHORT).show();
                                    }else{
//                                        startAddUserInfo();
//                                        currentUser.setGoal();
                                        isRegistered();
                                        hideProgressBar();
//                                        Main.show(Intro.this);
                                        MainTable.show(Intro.this, Recalibrate.KEY.ROTATE_L);
                                    }
                                }
                            });
                }
            }
        });
    }

    private void isRegistered(){
        Recalibrate.Preferences.setStringPreferences(getApplicationContext()
                , Recalibrate.KEY.USER_REGISTERED
                , Recalibrate.KEY.IS_REGISTERED);
    }

    private void changeCurrentView(){
        Recalibrate.Helper.hideKeyboard(this);
        switch (currentView){
            case Recalibrate.KEY.TYPE_VIEW_C:
                hiScreen.setVisibility(View.VISIBLE);
                break;
//            case Recalibrate.KEY.TYPE_VIEW_R:
//                break;
            case Recalibrate.KEY.TYPE_VIEW_L:
                Animation animHiScreen = AnimationUtils.loadAnimation(getApplicationContext()
                        , R.anim._hide_view_is_down_to_up);
                hiScreen.startAnimation(animHiScreen);
                hiScreen.setVisibility(View.GONE);
                Animation animSettings = AnimationUtils.loadAnimation(getApplicationContext()
                        , R.anim._show_view_is_down_to_up);
//                loginScreen.startAnimation(animSettings);
//                loginScreen.setVisibility(View.VISIBLE);
                loginSection.startAnimation(animSettings);
                loginSection.setVisibility(View.VISIBLE);
                setLabel(R.string.intro_button_login);
                break;
//            case Recalibrate.KEY.TYPE_VIEW_D:
//                break;
            case Recalibrate.KEY.TYPE_VIEW_S:
                startAddUserInfo();
                break;
        }
    }

    private void listenerRegistrationGender(){
        genderSelection[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deselectGender();
                genderSelection[0].setTextColor(getResources().getColor(R.color.colorWhite));
                genderSelection[0].setBackground(getResources().getDrawable(R.drawable.background_rect_red_full));
            }
        });

        genderSelection[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deselectGender();
                genderSelection[1].setTextColor(getResources().getColor(R.color.colorWhite));
                genderSelection[1].setBackground(getResources().getDrawable(R.drawable.background_rect_red_full));
            }
        });
    }

    private void deselectGender(){
        for(TextView item : genderSelection){
            item.setTextColor(getResources().getColor(R.color.colorGray_068));
            item.setBackground(getResources().getDrawable(R.drawable.background_rect_red_frame));
        }
    }

    private void changeLoginRegister(){
        Recalibrate.Helper.hideKeyboard(this);
        if(isRegistered) {
            btnChangeRegistered.setText(getString(R.string.intro_button_login));
            btnRegistration.setText(getString(R.string.intro_button_registration));
            firstNameView.setVisibility(View.VISIBLE);
            lastNameView.setVisibility(View.VISIBLE);
            selectedGenderView.setVisibility(View.VISIBLE);
            confirmationView.setVisibility(View.VISIBLE);
            setLabel(R.string.intro_button_registration);
        }else{
            btnChangeRegistered.setText(getString(R.string.intro_button_registration));
            btnRegistration.setText(getString(R.string.intro_button_login));
            firstNameView.setVisibility(View.GONE);
            lastNameView.setVisibility(View.GONE);
            selectedGenderView.setVisibility(View.GONE);
            confirmationView.setVisibility(View.GONE);
            setLabel(R.string.intro_button_login);
        }
    }

    private void startAddUserInfo(){
        changeLogo(getString(R.string.intro_label_next));
        Animation animHiScreen = AnimationUtils.loadAnimation(getApplicationContext()
                , R.anim._hide_view_is_down_to_up);
//        loginScreen.startAnimation(animHiScreen);
//        loginScreen.setVisibility(View.GONE);
        loginSection.startAnimation(animHiScreen);
        loginSection.setVisibility(View.GONE);

        Animation animSettings = AnimationUtils.loadAnimation(getApplicationContext()
                , R.anim._show_view_is_down_to_up);
        settingsScreen.startAnimation(animSettings);
        settingsScreen.setVisibility(View.VISIBLE);
        DialogStart.show(Intro.this);
    }

    private void updateViewWorkout(){
        if(workoutsList.size() > workoutViewCollection.size()){
            LinearLayout work = (LinearLayout) getLayoutInflater()
                    .inflate(R.layout.item_workout_view, null, false);
            TextView name = (TextView)work.findViewById(R.id.workoutNamePosition);
            name.setText(String.valueOf(workoutsList.size()));
            ((TextView)work.findViewById(R.id.workoutDurationView))
                    .setText(workoutsList.get(workoutsList.size()-1).getDuration());
            ((TextView)work.findViewById(R.id.workoutIntensityView))
                    .setText(workoutsList.get(workoutsList.size()-1).getIntensity());
            ((TextView)work.findViewById(R.id.workoutFrequencyView))
                    .setText(workoutsList.get(workoutsList.size()-1).getFrequency());
            workoutViewCollection.add(work);
            workoutContainer.addView(work);
        }
    }

    private void userData(){
        currentUser.setCreated(System.currentTimeMillis());
        currentUser.setCurrentUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());
        currentUser.setFirstName(registrationInfo[3].getText().toString());
        currentUser.setLastName(registrationInfo[4].getText().toString());
        currentUser.setGender(genderSelectedResult());

        Backend.saveCurrentUser(currentUser);

//        dataRef = database.getReference(Recalibrate.KEY.USER_CURRENT);
//        dataRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid())
//                .setValue(currentUser)
//                .addOnCompleteListener(Intro.this, new OnCompleteListener<Void>() {
//                    @Override
//                    public void onComplete(@NonNull Task<Void> task) {
//                        Recalibrate.LOG(TAG, "USER created success");
//                    }
//                });
    }

    private String genderSelectedResult(){
        String result;
        if(genderSelection[0].getCurrentTextColor() == getResources().getColor(R.color.colorWhite)){
            result = getString(R.string.intro_view_reg_male);
        }else if(genderSelection[1].getCurrentTextColor() == getResources().getColor(R.color.colorWhite))
            result = getString(R.string.intro_view_reg_female);
        else result = getString(R.string.intro_view_reg_no_select);
        return result;
    }

    @Override
    public void onDialogWorkoutSaveListener(UserTraining objects) {
        objects.setUserId(FirebaseAuth.getInstance().getCurrentUser().getUid());
        workoutsList.add(objects);
        Backend.saveWorkoutObject(workoutsList);
        updateViewWorkout();
    }

    @Override
    public String onStringDataListener(int type) {
        String result = "";
//        if(information != null){
        switch (type){
            case Recalibrate.KEY.CHECK:
                if (information!= null) result = information.getCheckIn();
                else result = Recalibrate.Preferences.
                        getStringPreferences(getApplicationContext(),
                                Recalibrate.Preferences.TEXT_CHECK_IN);
                break;
            case Recalibrate.KEY.REVIEW:
                if (information!= null) result = information.getReviewPerformance();
                else result = Recalibrate.Preferences.
                        getStringPreferences(getApplicationContext(),
                                Recalibrate.Preferences.TEXT_PREVIEW_PERFORMANCE);
                break;
            case Recalibrate.KEY.TRACK:
                if (information!= null) result = information.getTrackProgress();
                else result = Recalibrate.Preferences.
                        getStringPreferences(getApplicationContext(),
                                Recalibrate.Preferences.TEXT_TRACK_PROGRESS);
                break;
//            }
        }
        return result;
    }
}
