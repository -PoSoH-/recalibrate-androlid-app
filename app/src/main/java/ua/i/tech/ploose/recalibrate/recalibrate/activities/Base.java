package ua.i.tech.ploose.recalibrate.recalibrate.activities;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.ShowToastMessageListener;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

public class Base extends AppCompatActivity implements ShowToastMessageListener {

    private TextView mTextToolBarView;
    private ImageView mLeftButtonMenuBack;
//    private SVGImageView mLeftButtonMenuBack;
    private ImageView mLeftButtonMenuClosed;
    private ImageView mLeftButtonMenuOk;
    private ImageView mRightButtonMenuAdd;
    private ImageView mRightButtonMenuSettings;
    private RelativeLayout mContainerProgress;
//    private ImageView mPhotoChangeButtonMenu;
//    private ImageView mSearchButtonMenu;
//    private ImageView mIconAppLogo;
//    private ImageView mBackgroundImg;

    RelativeLayout fullLayout;
    RelativeLayout toolbar;

    private SVG svgIconParser = null;
    private Bitmap bitmapCreated; // = Bitmap.createBitmap(44, 44, Bitmap.Config.ARGB_8888);
    private Canvas  canvasCreated; // = new Canvas(newBM);

    @Override
    public void setContentView(int layoutResID) {
        setContentView(getLayoutInflater().inflate(layoutResID, null));
    }

    @Override
    public void setContentView(View view) {
        fullLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.a_base, null);
        FrameLayout contentLayout = (FrameLayout) fullLayout.findViewById(R.id.baseContainer);
        contentLayout.addView(view);
        initializeControls(fullLayout);
        super.setContentView(fullLayout);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

//    public RelativeLayout getRelativeLayout(){return fullLayout;}

    private View.OnClickListener backButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onBackPressedWithAnimation();
        }
    };

    private void initializeControls(View view){
        toolbar = (RelativeLayout) view.findViewById(R.id.includeToolbarView);
        mLeftButtonMenuBack = (ImageView) view.findViewById(R.id.toolbar_btn_left_back);
        addedImageResourceToView(mLeftButtonMenuBack, R.raw.btn_back_red, 44, 44);

        mLeftButtonMenuClosed = (ImageView) view.findViewById(R.id.toolbar_btn_left_closed);
        addedImageResourceToView(mLeftButtonMenuClosed, R.raw.btn_closed_red, 38, 38);

        mRightButtonMenuSettings = (ImageView) view.findViewById(R.id.toolbar_btn_right_setting);
        addedImageResourceToView(mRightButtonMenuSettings, R.raw.btn_settings, 512, 512);

        mLeftButtonMenuOk = (ImageView) view.findViewById(R.id.toolbar_btn_left_ok);
        addedImageResourceToView(mLeftButtonMenuOk, R.raw.btn_frame_is_ok_red, 512, 512);

        mRightButtonMenuAdd = (ImageView) view.findViewById(R.id.toolbar_btn_right_add);
        addedImageResourceToView(mRightButtonMenuAdd, R.raw.btn_frame_is_add_red, 512, 512);

        mTextToolBarView = (TextView) view.findViewById(R.id.toolbar_label);
        mContainerProgress = (RelativeLayout) view.findViewById(R.id.baseProgressBarContainer);
        try {
            String label = getString(getPackageManager().getActivityInfo(getComponentName(), 0).
                    labelRes);
            if (!TextUtils.isEmpty(label)){
                mTextToolBarView.setVisibility(View.VISIBLE);
                mTextToolBarView.setText(label);
            }
        } catch (Exception ignored) {}
    }

    public void addedImageResourceToView(ImageView view, int resource, int x_size, int y_size){
        try {
            svgIconParser = SVG.getFromResource(this, resource);
            bitmapCreated = Bitmap.createBitmap(x_size, y_size, Bitmap.Config.ARGB_8888);
            canvasCreated = new Canvas(bitmapCreated);
            svgIconParser.renderToCanvas(canvasCreated);
            view.setImageBitmap(bitmapCreated);
        } catch (SVGParseException e) {
            e.printStackTrace();
        }
    }

    public RelativeLayout getToolbarView(){return toolbar;}

    public void showToolBar(){toolbar.setVisibility(View.VISIBLE);}
    public void hideToolBar(){toolbar.setVisibility(View.GONE);}

    public void showButtonMenuBack(){
        mLeftButtonMenuBack.setVisibility(View.VISIBLE);
    }
    public void hideButtonMenuBack(){
        mLeftButtonMenuBack.setVisibility(View.INVISIBLE);
    }
    public ImageView getButtonMenuBack(){return mLeftButtonMenuBack;}
    public void showButtonMenuClosed(){
        mLeftButtonMenuClosed.setVisibility(View.VISIBLE);
    }
    public void hideButtonMenuClosed(){
        mLeftButtonMenuClosed.setVisibility(View.INVISIBLE);
    }
    public ImageView getButtonMenuClosed(){return mLeftButtonMenuClosed;}
    public void showButtonMenuOk(){
        mLeftButtonMenuOk.setVisibility(View.VISIBLE);
    }
    public void hideButtonMenuOk(){
        mLeftButtonMenuOk.setVisibility(View.INVISIBLE);
    }
    public ImageView getButtonMenuOk(){return mLeftButtonMenuOk;}
    public void showButtonMenuAdd(){
        mRightButtonMenuAdd.setVisibility(View.VISIBLE);
    }
    public void hideButtonMenuAdd(){
        mRightButtonMenuAdd.setVisibility(View.INVISIBLE);
    }
    public ImageView getButtonMenuAdd(){return mRightButtonMenuAdd;}
    public void showButtonMenuSettings(){
        mRightButtonMenuSettings.setVisibility(View.VISIBLE);
    }
    public void hideButtonMenuSettings(){
        mRightButtonMenuSettings.setVisibility(View.INVISIBLE);
    }
    public ImageView getButtonMenuSettings(){return mRightButtonMenuSettings;}

    public void showProgressBar(){
        mContainerProgress.setVisibility(View.VISIBLE);
    }
    public void hideProgressBar(){
        mContainerProgress.setVisibility(View.INVISIBLE);
    }
    public RelativeLayout getProgressBarContainer(){return mContainerProgress;}

    public void setLabel(int resId){
        setLabel(getString(resId));
    }

    public void setLabel(String label){
        if(mTextToolBarView.getVisibility() == View.GONE){
            mTextToolBarView.setVisibility(View.VISIBLE);
//            mIconAppLogo.setVisibility(View.GONE);
        }
        mTextToolBarView.setText(label);
    }

    public void setFragment(Fragment fragment, int layoutId){
        FragmentManager fragmentManager = getSupportFragmentManager();
        String tag = fragment.getClass().getSimpleName();
        if (fragmentManager.findFragmentByTag(tag) == null)
            fragmentManager.beginTransaction().replace(layoutId, fragment, tag).commit();
    }

    public void removeFragment(int layoutId){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(layoutId, new Fragment(), null).commit();
    }

    public static final int RIGHT_ANIMATION = -1; // animation is right to left
    public static final int LEFT_ANIMATION = -2; // animation is left to right
    public static final int DOWN_ANIMATION = -3; // animation is down to up
    public static final int UP_ANIMATION = -4; // animation is up to down

    public void setFragmentAnimation(FragmentManager fragmentManager, Fragment fragment,
                                            int layoutResIs, boolean addToBackStack, int typeAnimation){
        String tag = fragment.getClass().getSimpleName();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        switch (typeAnimation) {
            case RIGHT_ANIMATION:
                fragmentTransaction.setCustomAnimations(
                        R.anim.activity_start_enter_right_side
                        , R.anim.activity_start_exit_right_side
                );
                break;
            case LEFT_ANIMATION:
                fragmentTransaction.setCustomAnimations(
                        R.anim.activity_start_enter_left_side
                        , R.anim.activity_start_exit_left_side
                );
                break;
            case UP_ANIMATION:
                fragmentTransaction.setCustomAnimations (
                        R.anim.activity_start_enter_up_side
                        , R.anim.activity_start_exit_up_side
                );
                break;
            case DOWN_ANIMATION:
                fragmentTransaction.setCustomAnimations (
                        R.anim.activity_start_enter_down_side
                        , R.anim.activity_start_exit_down_side
                );
                break;
        }
        fragmentTransaction.replace(layoutResIs, fragment, tag);
        if (addToBackStack) fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public Fragment getFragment(Class cl){
        return getSupportFragmentManager().findFragmentByTag(cl.getSimpleName());
    }

//    public static Base get(Activity activity){
//        return (Base) activity;
//    }

    public void onBackPressedWithAnimation(){
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_start_enter_left_side, R.anim.activity_start_exit_left_side);
    }

//    public static void animationRightToLeft(Activity activity){
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side,
//                R.anim.activity_start_exit_right_side);
//    }

//    public static void setRightAnimation(Activity activity){
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side,
//                R.anim.activity_start_exit_right_side);
//    }

    protected void saveCheckInCollection(final List<UserCheckIn> collection
            , final User user){
        user.printLogValueField();

        if(collection.size() == 1){

            Backend.loadUserTraining(new Backend.OnRecalibrateTrainingListener() {
                @Override
                public void onSuccess(List result) {
                    Recalibrate.Calc.calcScore(getApplicationContext()
                            , result
                            , user);
                    Recalibrate.Calc.calcCheckInProgress(getApplicationContext()
                            , null
                            , collection.get(0)
                            , user);
                    Recalibrate.Calc.calcDeficitSurplus(getApplicationContext()
                            , collection.get(0)
                            , user);
                    Recalibrate.Calc.calcBMR(getApplicationContext()
                            , collection.get(0)
                            , user);
                }

                @Override
                public void onError(int error) {

                }
            });
        }else {
            collection.get(collection.size() - 1).setRecords(Recalibrate.Helper.createRecord());
            collection.get(collection.size() - 1).setCreated(Recalibrate.Helper.createCorrectDate());
            Recalibrate.Calc.calcCheckInProgress(getApplicationContext()
                    , collection.get(collection.size() - 2)
                    , collection.get(collection.size() - 1)
                    , user);
            Recalibrate.Calc.calcWeekEnergyRequirements(getApplicationContext()
                    , collection.get(collection.size() - 2)
                    , collection.get(collection.size() - 1)
                    , user);
        }
        collection.get(collection.size()-1).printfieldValueChekIn();

        Backend.saveCheckInObject(collection, new Backend.OnRecalibrateSaveOnSuccess() {
            @Override
            public void onSuccess() {
                Recalibrate.LOG("BASE", "CheckIn update success...");
            }

            @Override
            public void onError() {
                Recalibrate.LOG("BASE", "CheckIn update error...");
            }
        });
    }

    protected boolean showErrorMessage(int code){
        String message = null;
        switch (code){
            case Recalibrate.KEY.Error.WEIGHT:
                message = getString(R.string.intro_error_weight);
                break;
            case Recalibrate.KEY.Error.NECK:
                message = getString(R.string.intro_error_neck);
                break;
            case Recalibrate.KEY.Error.CHEST:
                message = getString(R.string.intro_error_chest);
                break;
            case Recalibrate.KEY.Error.WAIST:
                message = getString(R.string.intro_error_waist);
                break;
            case Recalibrate.KEY.Error.HIP:
                message = getString(R.string.intro_error_hips);
                break;
            case Recalibrate.KEY.Error.THIGH:
                message = getString(R.string.intro_error_things);
                break;
        }
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public boolean showMessage(int typeMessage) {
        return showErrorMessage(typeMessage);
    }
}
