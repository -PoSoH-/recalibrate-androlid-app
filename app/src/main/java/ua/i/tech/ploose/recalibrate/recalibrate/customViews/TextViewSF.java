package ua.i.tech.ploose.recalibrate.recalibrate.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;

/**
 * Created by ADMIN on 31.03.2016.
 */
public class TextViewSF extends TextView {
    public TextViewSF(Context context) {
        super(context);
    }

    public TextViewSF(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public TextViewSF(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.TextViewSF);
        String customFont = a.getString(R.styleable.TextViewSF_TextViewSF);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf;
        try {
//            tf = Typeface.createFromAsset(ctx.getAssets(),"fonts/brandon/brandon_regular.otf");
            tf = Typeface.createFromAsset(ctx.getAssets(),"fonts/sf/sf-ui-display-regular.ttf");
            setTypeface(tf);
        } catch (Exception e) {
            Recalibrate.LOG("New font","BrandonRegular" + " - " + "Could not get typeface: " + e.getMessage());
            return false;
        }
        return true;
    }
}
