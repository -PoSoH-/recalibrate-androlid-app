package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.ProgramInfo;

/**
 * Created by Developer on 26.12.2016.
 */

public class MealPlans extends Fragment {

    public static Fragment createFragment(){
        return new MealPlans();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_menu_settings_h, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final WebView textMacros = (WebView) view.findViewById(R.id.mainSettingsMenuWebView);
        Backend.loadProgramInfo(new Backend.OnRecalibrateProgramInfo() {
            @Override
            public void onSuccess(ProgramInfo information) {
                textMacros.loadData(createHTMLDocument(information.getMealPlans()), "text/html; charset=utf-8", "utf-8");
            }

            @Override
            public void onError(int error) {
                textMacros.loadData(createHTMLDocument("<body><h3>ERROR</h3><p>Error load data with server</p></body>"), "text/html; charset=utf-8", "utf-8");
            }
        });
    }

    private String createHTMLDocument(String bodyBlock){
        StringBuilder text = new StringBuilder();
        text.append("<html>");
        text.append("<head>");
        text.append("<style type=\"text/css\">");
        text.append("font-face {"
                + "font-family: brandon;"
                + "src: url(\"file:///android_asset/fonts//brandon/brandon_regular.otf\")}");
        text.append("body {"
                + "font-family: brandon;"
                + "font-size: medium;"
                + "text-align: justify;}");
        text.append("h3 {text-align: center;}");
        text.append("</style>");
        text.append("</head>");
        text.append(bodyBlock);
        text.append("</html>");

//        Recalibrate.LOG("TEXT", text.toString());
        return text.toString();
    }
}
