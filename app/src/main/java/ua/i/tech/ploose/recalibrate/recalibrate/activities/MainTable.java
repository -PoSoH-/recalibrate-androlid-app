package ua.i.tech.ploose.recalibrate.recalibrate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Calories;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

public class MainTable extends Base {

    private final String TAG = MainTable.class.getSimpleName() + " : ";

    private ImageView indicatorsEnable[];

    private TextView []trainingDay = null;
    private TextView []noTrainingDay = null;
    private List<UserCheckIn> checkIns = new LinkedList<>();

    public static void show(AppCompatActivity activity, final int rotate){
        Intent intent = new Intent(activity, MainTable.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side
//                , R.anim.activity_start_exit_right_side);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRotationAnimation();
        setContentView(R.layout.a_main_table);

        hideToolBar();

        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {
            @Override
            public void onSuccess(List<UserCheckIn> result) {
                checkIns = result;
                updateUi();
            }

            @Override
            public void onError(int error) {

            }
        });

        indicatorsEnable = new ImageView[]{
                (ImageView) findViewById(R.id.mainIndicatorCupEnable),
                (ImageView) findViewById(R.id.mainIndicatorGraphEnable),
                (ImageView) findViewById(R.id.mainIndicatorHumanEnable)
        };

        FrameLayout[] buttons = new FrameLayout[]{
                (FrameLayout) findViewById(R.id.mainButtonCup),
                (FrameLayout) findViewById(R.id.mainButtonGraph),
                (FrameLayout) findViewById(R.id.mainButtonHuman)
        };

        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        buttons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainPref.show(MainTable.this, Recalibrate.KEY.ROTATE_R);
            }
        });
        buttons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainProf.show(MainTable.this, Recalibrate.KEY.ROTATE_R);
            }
        });

        trainingDay = new TextView[]{
                (TextView) findViewById(R.id.trainingDayProtein),      // training day protein
                (TextView) findViewById(R.id.trainingDayCarbohydrate), // training day carbohydrate
                (TextView) findViewById(R.id.trainingDayFat),          // training day fat
                (TextView) findViewById(R.id.trainingDayCalories),     // training day calories
        };

        noTrainingDay = new TextView[]{
                (TextView) findViewById(R.id.noTrainingDayProtein),      // training day protein
                (TextView) findViewById(R.id.noTrainingDayCarbohydrate), // training day carbohydrate
                (TextView) findViewById(R.id.noTrainingDayFat),          // training day fat
                (TextView) findViewById(R.id.noTrainingDayCalories),     // training day calories
        };

        initializeImageIndicator();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setRotationAnimation() {
        int rotationAnimation = WindowManager.LayoutParams.ROTATION_ANIMATION_CROSSFADE;
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        winParams.rotationAnimation = rotationAnimation;
        win.setAttributes(winParams);
    }

    private void initializeImageIndicator(){
        addedImageResourceToView(indicatorsEnable[0],R.raw.btn_cup_red, 512,512);
        addedImageResourceToView(indicatorsEnable[1],R.raw.btn_graph_black, 512,512);
        addedImageResourceToView(indicatorsEnable[2],R.raw.btn_man_black, 512,512);
        deselectedButton();
    }

    private void deselectedButton(){
        indicatorsEnable[0].setVisibility(View.VISIBLE);
        indicatorsEnable[1].setVisibility(View.VISIBLE);
        indicatorsEnable[2].setVisibility(View.VISIBLE);
    }

    private void updateUi(){
        Calories calories = checkIns.get(checkIns.size()-1)
                .getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING);
        trainingDay[0].setText(String.valueOf(calories.getProtein())
                + "g ("
                + String.valueOf((int)(calories.getProteinPercent()*100.0F))
                + "%)");
        trainingDay[1].setText(String.valueOf(calories.getCarbohydrate())
                + "g ("
                + String.valueOf((int)(calories.getCarbohydratePercent()*100.0F))
                + "%)");
        trainingDay[2].setText(String.valueOf(calories.getFat())
                + "g ("
                + String.valueOf((int)(calories.getFatPercent()*100.0F))
                + "%)");
        trainingDay[3].setText(String.valueOf( (int) calories.getCalories()));
        calories = checkIns.get(checkIns.size()-1)
                .getCalories().get(Recalibrate.Calc.KEY_N_TRAINING);
        noTrainingDay[0].setText(String.valueOf(calories.getProtein())
                + "g ("
                + String.valueOf((int)(calories.getProteinPercent() * 100.0F))
                + "%)");
        noTrainingDay[1].setText(String.valueOf(calories.getCarbohydrate())
                + "g ("
                + String.valueOf((int)(calories.getCarbohydratePercent()*100.0F))
                + "%)");
        noTrainingDay[2].setText(String.valueOf(calories.getFat())
                + "g ("
                + String.valueOf((int)(calories.getFatPercent()*100.0F))
                + "%)");
        noTrainingDay[3].setText(String.valueOf( (int) calories.getCalories()));
    }
}

