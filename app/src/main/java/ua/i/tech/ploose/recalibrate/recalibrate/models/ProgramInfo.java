package ua.i.tech.ploose.recalibrate.recalibrate.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

/**
 * Created by Developer on 11.01.2017.
 */

@IgnoreExtraProperties
public class ProgramInfo implements Serializable{

    private String checkIn;
    private String about;
    private String reviewPerformance;
    private String trackProgress;
    private String termsConditions;
    private String countMacros;
    private String mealPlans;

    public ProgramInfo(){}

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(String checkIn) {
        this.checkIn = checkIn;
    }

    public String getReviewPerformance() {
        return reviewPerformance;
    }

    public void setReviewPerformance(String reviewPerformance) {
        this.reviewPerformance = reviewPerformance;
    }

    public String getTrackProgress() {
        return trackProgress;
    }

    public void setTrackProgress(String trackProgress) {
        this.trackProgress = trackProgress;
    }

    public String getTermsConditions() {
        return termsConditions;
    }

    public void setTermsConditions(String termsConditions) {
        this.termsConditions = termsConditions;
    }

    public String getCountMacros() {
        return countMacros;
    }

    public void setCountMacros(String countMacros) {
        this.countMacros = countMacros;
    }

    public String getMealPlans() {
        return mealPlans;
    }

    public void setMealPlans(String mealPlans) {
        this.mealPlans = mealPlans;
    }
}
