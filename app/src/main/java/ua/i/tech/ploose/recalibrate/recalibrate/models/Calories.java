package ua.i.tech.ploose.recalibrate.recalibrate.models;

/**
 * Created by root on 20.02.17.
 */

public class Calories {
    private float calories = 0.0F;
    private int protein = 0;
    private float proteinPercent = 0;
    private int carbohydrate = 0;
    private float carbohydratePercent = 0;
    private int fat = 0;
    private float fatPercent = 0;

    public float getCalories() {
        return calories;
    }

    public void setCalories(float calories) {
        this.calories = calories;
    }

    public int getProtein() {
        return protein;
    }

    public void setProtein(int protein) {
        this.protein = protein;
    }

    public float getProteinPercent() {
        return proteinPercent;
    }

    public void setProteinPercent(float proteinPercent) {
        this.proteinPercent = proteinPercent;
    }

    public int getCarbohydrate() {
        return carbohydrate;
    }

    public void setCarbohydrate(int carbohydrate) {
        this.carbohydrate = carbohydrate;
    }

    public float getCarbohydratePercent() {
        return carbohydratePercent;
    }

    public void setCarbohydratePercent(float carbohydratePercent) {
        this.carbohydratePercent = carbohydratePercent;
    }

    public int getFat() {
        return fat;
    }

    public void setFat(int fat) {
        this.fat = fat;
    }

    public float getFatPercent() {
        return fatPercent;
    }

    public void setFatPercent(float fatPercent) {
        this.fatPercent = fatPercent;
    }
}
