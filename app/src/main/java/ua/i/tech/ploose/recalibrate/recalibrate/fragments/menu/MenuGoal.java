package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.wheelAdapters.SettingAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

/**
 * Created by Developer on 26.12.2016.
 */

public class MenuGoal extends Fragment {

    private OnClickButtonListener listener = null;

    private WheelView wheel;
    private SettingAdapter goalAdapter;
    private View buttonSave;
    private int selectedGoal;
    private List<UserCheckIn> userCheckIn = null;
    String dataGoals [] = null;
    String dataValue [] = null;


    public static Fragment createFragment(){
        return new MenuGoal();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnClickButtonListener)
            listener = (OnClickButtonListener) context;
        else
            throw new ClassCastException("ERROR! "
                    + "No implements interface - "
                    + OnClickButtonListener.class.getSimpleName());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_include_goal_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {
            @Override
            public void onSuccess(List<UserCheckIn> result) {
                userCheckIn = result;
                updateGoalsToSelected();
            }

            @Override
            public void onError(int error) {

            }
        });

        wheel = (WheelView) view.findViewById(R.id.mainAboutMenuWheelGoal);
        wheel.disableShadows();
        dataGoals = getResources().getStringArray(R.array.intro_settings_collection_goal);
        dataValue = getResources().getStringArray(R.array.intro_settings_collection_goal_value);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        createFilter(wheel, dataGoals);

        buttonSave = view.findViewById(R.id.mainAboutMenuBntGoalSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedGoal > 0) {
                    userCheckIn.get(userCheckIn.size() - 1).setGoal(dataValue[selectedGoal]);
                    listener.onClickListenerSave(userCheckIn);
                }else
                    Toast.makeText(getContext(), getString(R.string.intro_error_goal), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void updateGoalsToSelected(){

        if (userCheckIn != null){
            for(int i = 0; i < dataValue.length; i++){
                if(TextUtils.equals(dataValue[i], userCheckIn.get(userCheckIn.size()-1).getGoal())){
                    selectedGoal = i;
                    wheel.setCurrentItem(selectedGoal);
                    goalAdapter.setSelectedItem(selectedGoal);
                    goalAdapter.notifyDataChangedEvent();
                    return;
                }
            }
        }
    }

    private void createFilter(WheelView wheelView, String []data) {
        wheelView.setWheelViewBackgroundResource(android.R.color.transparent);
        wheelView.disableShadows();
        wheelView.setVisibleItems(3);
        goalAdapter = new SettingAdapter(getContext(),
                data,
                selectedGoal);
        wheelView.setViewAdapter(goalAdapter);
        wheelView.setCurrentItem(goalAdapter.getSelectedItem());
        goalAdapter.notifyDataChangedEvent();
        wheelView.setCurrentItem(selectedGoal);
        goalAdapter.setSelectedItem(selectedGoal);

        wheelView.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorGray_138));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources().getDimension(R.dimen.filter_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources().getDimension(R.dimen.filter_text_size_in_focus));
                    selectedGoal = newValue;
                    goalAdapter.setSelectedItem(newValue);

                } catch (NullPointerException e) {
                    Log.e("FILTER", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
}
