package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.MenuTrainingPlan;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogTrainingSetup;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

/**
 * Created by Developer on 26.12.2016.
 */

public class MenuPlan extends Fragment {

    public static Fragment createFragment(){
        return new MenuPlan();
    }

    private MenuTrainingPlan adapter;
    private OnUserTrainingListListener listener;
    private List<UserTraining> trainings;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnUserTrainingListListener)
            listener = (OnUserTrainingListListener) context;
        else
            throw new ClassCastException("Error implements interface - OnUserTrainingListListener");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_include_plan_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ListView planList = (ListView) view.findViewById(R.id.mainMenuTrainingPlan);
        adapter = new MenuTrainingPlan(getContext(), trainings = new LinkedList<>());
        planList.setAdapter(adapter);
        LinearLayout footer = (LinearLayout) LayoutInflater.from(getContext())
                .inflate(R.layout.item_workout_add_view, null, false);
        footer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DialogSettings.show(getActivity());
                DialogTrainingSetup.show(getActivity());
            }
        });
        planList.addFooterView(footer);
        assert listener != null;
        trainings = listener.updateUserTrainingList();
        updateAllWorkout(trainings);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    public void updateAllWorkout(List<UserTraining> userTrainings){
        trainings = userTrainings;
        adapter.updateAllWorkout(userTrainings);
    }

    public interface OnUserTrainingListListener{
        List<UserTraining> updateUserTrainingList();
    }
}
