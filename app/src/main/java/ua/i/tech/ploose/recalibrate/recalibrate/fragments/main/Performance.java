package ua.i.tech.ploose.recalibrate.recalibrate.fragments.main;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;

/**
 * Created by Developer on 26.12.2016.
 */

public class Performance extends Fragment {

    public static Fragment createFragment(){
        return new Performance();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_table_performance_view, container, false);
//        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LineChart charts[] = new LineChart[]{
                (LineChart) view.findViewById(R.id.mainPerformanceChart_fir),
                (LineChart) view.findViewById(R.id.mainPerformanceChart_sec),
                (LineChart) view.findViewById(R.id.mainPerformanceChart_thr),
                (LineChart) view.findViewById(R.id.mainPerformanceChart_fou)
        };

        setDataOnLineChart(charts[0],"Protein", generateMatrix(40), generateMatrix(40));
        setDataOnLineChart(charts[1],"Carbs", generateMatrix(40), generateMatrix(40));
        setDataOnLineChart(charts[2],"Fat", generateMatrix(40), generateMatrix(40));
        setDataOnLineChart(charts[3],"Calories", generateMatrix(40), generateMatrix(40));

//        for(int pos=0; pos<charts.length;pos++){
////            addDataToGraph(charts[pos], pos);
//            setDataOnLineChart(charts[pos],"Fat");
//        }

//        for(LineChart item : charts)
//            addDataToGraph(item);

    }

    private void addDataToGraph(LineChart chart, int position){
        List<Entry> entries1 = new ArrayList<>();
        entries1.add(new Entry(1, 20));
        entries1.add(new Entry(2, 10));
        entries1.add(new Entry(3, -7));
        entries1.add(new Entry(4, 0));
        entries1.add(new Entry(5, -10));
        entries1.add(new Entry(6, 10));
        entries1.add(new Entry(7, 0));
        entries1.add(new Entry(8, 20));
        entries1.add(new Entry(9, -5));

        List<Entry> entries2 = new ArrayList<>();
        entries2.add(new Entry(1, 5));
        entries2.add(new Entry(2, 10));
        entries2.add(new Entry(3, 0));
        entries2.add(new Entry(4, 5));
        entries2.add(new Entry(5, 15));
        entries2.add(new Entry(6, 25));
        entries2.add(new Entry(7, 5));
        entries2.add(new Entry(8, 10));
        entries2.add(new Entry(9, 5));

        LineDataSet dataSet1 = null;
        LineDataSet dataSet2 = null;

        switch (position) {
            case 0:
                dataSet1 = new LineDataSet(entries1, "Protein real"); // add entries to dataset
                dataSet2 = new LineDataSet(entries2, "Protein plan"); // add entries to dataset
                break;
            case 1:
                dataSet1 = new LineDataSet(entries1, "Carbohydrates real"); // add entries to dataset
                dataSet2 = new LineDataSet(entries2, "Carbohydrates plan"); // add entries to dataset
                break;
            case 2:
                dataSet1 = new LineDataSet(entries1, "Fat real"); // add entries to dataset
                dataSet2 = new LineDataSet(entries2, "Fat plan"); // add entries to dataset
                break;
            case 3:
                dataSet1 = new LineDataSet(entries1, "Calories real"); // add entries to dataset
                dataSet2 = new LineDataSet(entries2, "Calories plan"); // add entries to dataset
                break;
        }

        if(dataSet1 == null || dataSet2 == null) return;

        dataSet1.setColor(getResources().getColor(R.color.colorRed_B));
        dataSet1.setValueTextColor(getResources().getColor(R.color.colorBlack));
        dataSet2.setColor(getResources().getColor(R.color.colorBlue));
        dataSet2.setValueTextColor(getResources().getColor(R.color.colorBlack));

        LineData lineData = new LineData();
        lineData.addDataSet(dataSet1);
        lineData.addDataSet(dataSet2);
        chart.setData(lineData);
        chart.invalidate();
    }

    //  addDataToGraph(charts[pos], pos);

    public void setDataOnLineChart(LineChart lineChart
            , String labelLegend
            , float [] target
            , float [] actual){
        List<Entry> entries = new ArrayList<Entry>();//y
        List<String> xAxes = new ArrayList<>();//x
        List<Entry> testEntriesPieces = new ArrayList<>();//y

        String dataAxes[] = getResources().getStringArray(R.array.main_graph_x_axis_name);

//        for (int i=0; i<dataAxes.length;i++) {
//            entries.add(new Entry(i, i+((int) (100*Math.random()))));//x, y
//            xAxes.add(dataAxes[i]);
//        }
//        for (int i=0; i<dataAxes.length;i++) {
//            testEntriesPieces.add(new Entry(i, i+((int) (100*Math.random()))));//x, y
//            xAxes.add(dataAxes[i]);
//        }
//        testEntriesPieces.add(new Entry(0,((int) (100*Math.random()+100))));
//        // testEntriesPieces.add(new Entry(1,((int) (200*Math.random()+100))));
//        testEntriesPieces.add(new Entry(2,((int) (100*Math.random()+100))));
//        testEntriesPieces.add(new Entry(3,((int) (100*Math.random()+100))));
//        // testEntriesPieces.add(new Entry(4,((int) (200*Math.random()+100))));
//        testEntriesPieces.add(new Entry(5,((int) (100*Math.random()+100))));

        for (int i=0; i<target.length;i++) {
            entries.add(new Entry(i, target[i]));//x, y
            testEntriesPieces.add(new Entry(i,actual[i]));
            if(i<9){
                xAxes.add(i, 0+""+(i+1)+getOrdinalFor(i+1));
            }
            else{
                xAxes.add(i, (i+1)+getOrdinalFor(i+1));
            }
        }

        final String[] xaxes = new String[xAxes.size()];
        for(int i=0; i<xAxes.size();i++){
            xaxes[i] = xAxes.get(i).toString();
        }

        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

        LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset target
        // setLine( dataSet,lineDataSets,  entries, "#ed5353");//чеврона лінія чогось не робить
        dataSet.setColor(Color.parseColor("#ed5353"));//чеврона лінія
        dataSet.setDrawCircles(false);// не малює кільця на графіку
        dataSet.setDrawValues(false);//не малює значення на графіку
        dataSet.setHighlightEnabled(false);// не малює перехресні лінії при натискуванні на графіку
        lineDataSets.add(dataSet);//червона

        if(testEntriesPieces.size()==1){
            LineDataSet dataSet1 = new LineDataSet(testEntriesPieces, ""); // add entries to dataset
            setPoint(dataSet1, lineDataSets, testEntriesPieces,"#3844e2");
        }
        else if(testEntriesPieces.size()!=0){
            List<Entry> bufferEntryList = new ArrayList<>();
            LineDataSet dataSet1 = new LineDataSet(bufferEntryList, ""); // add entries to dataset
            boolean flag = false;
            for (int i = 1; i < testEntriesPieces.size(); i++) {//тут проблеми через цю 1 проблеми з точками!!!
                if (testEntriesPieces.get(i).getX()-testEntriesPieces.get(i-1).getX()==1){
                    bufferEntryList.add(testEntriesPieces.get(i-1));
                    bufferEntryList.add(testEntriesPieces.get(i));
                    flag = true;
                    if((testEntriesPieces.size()-1)==i){
                        setLine(dataSet1,lineDataSets, bufferEntryList, "#3844e2");
                    }
                }
                else if(flag){

                    setLine( dataSet1,lineDataSets,  bufferEntryList, "#3844e2");
                    flag = false;
                    bufferEntryList = new ArrayList<>();
                    if((testEntriesPieces.size()-1)==i){
                        bufferEntryList.add(testEntriesPieces.get(i));
                        setPoint(dataSet1,lineDataSets, bufferEntryList,"#3844e2");
                        bufferEntryList = new ArrayList<>();
                    }

                }
                else {
                    if(i==(testEntriesPieces.size()-1)){
                        bufferEntryList.add(testEntriesPieces.get(i-1));
                        setPoint(dataSet, lineDataSets, bufferEntryList,"#3844e2");

                        bufferEntryList = new ArrayList<>();
                        bufferEntryList.add(testEntriesPieces.get(i));
                        setPoint(dataSet, lineDataSets, bufferEntryList,"#3844e2");
                        bufferEntryList = new ArrayList<>();
                    }
                    else {
                        bufferEntryList.add(testEntriesPieces.get(i-1));
                        setPoint(dataSet, lineDataSets, bufferEntryList,"#3844e2");
                        bufferEntryList = new ArrayList<>();
                    }

                }
            }
        }

        IAxisValueFormatter formatter = new IAxisValueFormatter() {//показуємо вісь х з рядковими величинами
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xaxes[(int) value];
            }

        };


        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//вісь х (опис) знизу
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1
        xAxis.setValueFormatter(formatter);

        lineDataSets.add(dataSet);//червона
        lineChart.setData(new LineData(lineDataSets));

        Legend legend = lineChart.getLegend();//тут граємось із легендами
//        legend.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);//тут ставимо легенду за графіком!!!
//        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        List<LegendEntry> legendEntries = new ArrayList<>();
//        LegendEntry legendEntry1 = new LegendEntry(labelLegend.toLowerCase()+" target",Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#ed5353"));//тут описуэмо одиницю легенди червона лныя
//        LegendEntry legendEntry2 = new LegendEntry("actual "+labelLegend.toLowerCase(),Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#3844e2"));// синя лінія довжина і ширина
//        legendEntries.add(legendEntry1);
//        legendEntries.add(legendEntry2);
//        legend.setCustom(legendEntries);

        if (labelLegend.length()<=3){
            LegendEntry legendEntry1 = new LegendEntry("actual "+labelLegend.toLowerCase(),Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#3844e2"));// синя лінія довжина і ширина
            LegendEntry legendEntry2 = new LegendEntry(labelLegend.toLowerCase()+" target",Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#ed5353"));//тут описуэмо одиницю легенди червона лныя
            legendEntries.add(legendEntry1);
            legendEntries.add(legendEntry2);
            legend.setCustom(legendEntries);
        }
        else{
            LegendEntry legendEntry1 = new LegendEntry("actual",Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#3844e2"));// синя лінія довжина і ширина
            LegendEntry legendEntry2 = new LegendEntry(labelLegend.toLowerCase(),Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#00000000"));// синя лінія довжина і ширина
            LegendEntry legendEntry3 = new LegendEntry(labelLegend.toLowerCase(),Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#ed5353"));// синя лінія довжина і ширина
            LegendEntry legendEntry4 = new LegendEntry("target",Legend.LegendForm.LINE,30f,1f,null,Color.parseColor("#00000000"));//тут описуэмо одиницю легенди червона лныя
            legendEntries.add(legendEntry1);
            legendEntries.add(legendEntry2);
            legendEntries.add(legendEntry3);
            legendEntries.add(legendEntry4);
            legend.setCustom(legendEntries);
        }

        lineChart.getXAxis().setDrawGridLines(false);//ховає вертикальні лінії сітки
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setXOffset(1f);
        rightAxis.setAxisLineColor(Color.parseColor("#00000000"));
        rightAxis.setTextColor(Color.parseColor("#00000000"));
        // rightAxis.setEnabled(false);// ховає вісь У
        Description description = new Description();
        description.setText("");
        lineChart.setDescription(description);// ховає опис графіка
        lineChart.setScaleEnabled(false);//не зумиця
        lineChart.getAxisLeft().setDrawAxisLine(false);//ховає вісь У та що зліва

        lineChart.setVisibleXRangeMaximum(4.1f);

        lineChart.moveViewToX(target[target.length-1]);

        lineChart.invalidate();

    }

    private void setPoint( LineDataSet dataSet, ArrayList<ILineDataSet> lineDataSets, List<Entry> bufferEntryList, String color){
        dataSet = new LineDataSet(bufferEntryList, "");
        dataSet.setDrawCircles(true);// не малює кільця на графіку
        dataSet.setCircleColor(Color.parseColor(color));//колір кружка #3844e2
        dataSet.setCircleSize(2);//розмір кружка
        dataSet.setDrawValues(false);// не малює значення на графіку
        dataSet.setHighlightEnabled(false);// не малює перехресні лінії при натискуванні на графіку
        lineDataSets.add(dataSet);
    }
    private void setLine( LineDataSet dataSet, ArrayList<ILineDataSet> lineDataSets, List<Entry> bufferEntryList, String color){
        dataSet = new LineDataSet(bufferEntryList, "");
        dataSet.setColor(Color.parseColor(color));//"#3844e2"
        dataSet.setDrawCircles(false);// не малює кільця на графіку
        dataSet.setDrawValues(false);// не малює значення на графіку
        dataSet.setHighlightEnabled(false);
        lineDataSets.add(dataSet);
    }

    public static String getOrdinalFor(int value) {//генерує числові суфікси
        int hundredRemainder = value % 100;
        int tenRemainder = value % 10;
        if(hundredRemainder - tenRemainder == 10) {
            return "th";
        }

        switch (tenRemainder) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private float [] generateMatrix(int amount){
        float [] genMatrix = new float[amount];
        for(int i=0; i< genMatrix.length; i++){
            genMatrix[i]=((int) (200*Math.random()));
        }

        return  genMatrix;
    }

}
