package ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs;

import android.app.Dialog;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import ua.i.tech.ploose.recalibrate.recalibrate.R;

public class DialogCheckInLimit extends DialogFragment {

    public static void show(FragmentActivity fragmentActivity){ //}, final String userID, final String urlPhoto, final String userName) {
        DialogCheckInLimit dialog = new DialogCheckInLimit();
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, Dialog.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if(context instanceof onClickButtonListener)
//            listener = (onClickButtonListener) context;
//        else
//            throw new ClassCastException("No implements interface onClickButtonListener");
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_check_in_limit_view,
                null);

        view.findViewById(R.id.checkBntOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Point point = new Point();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_full_rect_transparent);
        dialog.getWindow().getWindowManager().getDefaultDisplay().getSize(point);
        dialog.setContentView(view);
        RelativeLayout base = (RelativeLayout) view.findViewById(R.id.dialogStart);
        base.getLayoutParams().width = point.x;
        base.getLayoutParams().height = point.y;
        return dialog;
    }
}
