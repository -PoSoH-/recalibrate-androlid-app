package ua.i.tech.ploose.recalibrate.recalibrate;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

//import com.activeandroid.app.Application;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import ua.i.tech.ploose.recalibrate.recalibrate.exeption.MinusValueException;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Calories;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Record;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

/**
 * Created by Developer on 26.12.2016.
 */

/**
 *   While CheckIn created -> To create all records of the object RECORD in the specified time period
 *   їх можна редагувати доки не відбувся запис...
 *   Після запису їїх можна тільки переглядатти - при виборі відповідної дати
 *   Масив для дат з кінця (Сьогодні вчора і т.д.)
 *   Коли перегляд кнопка додати новий запис не доступна....
 *   add record to array list in to UserCheckIn
 *   created graph build information:
 *   add graph information in to array list in ti UserCheckIn
 *
 *
 *
 */
public class Recalibrate extends Application {

    public static boolean isDebug = BuildConfig.DEBUG;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

//        LOG("FEMALE RESULT :", " " + Calc.calcFemaleProgress(174.0F, 40.0F, 81.0F, 99.0F));
//        LOG("MALE   RESULT :", " " + Calc.calcMaleProgress(174.0F, 40.0F, 81.0F));

//        new TestCalcalate();

    }

    public static void LOG(final String TAG, final String INFO){
        if(isDebug)
            Log.w(TAG, INFO);
    }

    public static class Helper{
        private static final int countId = 25;
        public static String createObjectId(){
            char []symbols = {
                    'a','b','c','d','e','f','g','h','i','j',  //10 symbols
                    'k','k','m','n','o','p','q','r','s','t',  //10 symbols
                    'u','v','w','x','y','z',                  //6  symbols
                    'A','B','C','D','E','F','G','H','I','J',  //10 symbols
                    'K','L','M','N','O','P','Q','R','S','T',  //10 symbols
                    'U','V','W','X','Y','Z',                  //6  symbols
                    '1','2','3','4','5','6','7','8','9','0',  //10 symbols
                    '!','%','&','?','=','+','-','*'};         //8  symbols
            StringBuilder result = new StringBuilder();
            for(int pos=0; pos<countId;pos++){
                result.append(symbols[new Random().nextInt(symbols.length)]);
            }
            return result.toString();
        }

        public static void hideKeyboard(AppCompatActivity activity) {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            View currentFocus = activity.getCurrentFocus();
            if (currentFocus != null) {
                IBinder iBinder = currentFocus.getWindowToken();
                if (iBinder != null) inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
            }
        }

        public static List<Record> createAllRecords(){
            List<Record> records = new LinkedList<>();
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.HOUR_OF_DAY, 3);

            int day = 1000*60*60*24;
            for(int i = 0; i<7; i++) {
                Record item = new Record();
                if(i == 0) cal.setTimeInMillis(cal.getTimeInMillis());
                else cal.setTimeInMillis(cal.getTimeInMillis() + day);
                item.setCurrentDate(cal.getTimeInMillis());
                Recalibrate.LOG(UserCheckIn.class.getSimpleName() + " : "
                        , new SimpleDateFormat("dd/MM/yy - hh:mm").format(cal.getTime()));
                records.add(item);
            }
            return records;
        }

        public static List<Record> createRecord() {
            List<Record> records = new LinkedList<>();
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.HOUR_OF_DAY, 3);
            Record item = new Record();
            item.setCurrentDate(cal.getTimeInMillis());
            records.add(item);
            return records;
        }

        public static String[] getAllDays(List<Record> records){
            int position = records.size()-1;
            String data[] = new String[records.size()];
            Calendar cal = Calendar.getInstance();
            for(int i = 0; i < records.size(); i++){
                cal.setTimeInMillis(records.get(position).getCurrentDate());
                data[i] = new SimpleDateFormat("dd MMMM").format(cal.getTime());
                Recalibrate.LOG(UserCheckIn.class.getSimpleName() + " : "
                        , data[i]);
                position--;
            }
            return data;
        }

        public static long createCorrectDate(){
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.HOUR_OF_DAY, 3);
            return cal.getTimeInMillis();
        }

    }

    public static class KEY{
        public static final String APP_INFO = "PROGRAM_INFO";
        public static final String APP_FAQ =  "PROGRAM_FAQ";
        public static final String FAQ_Q = "QUESTION";
        public static final String FAQ_A = "ANSWER";
        public static final String USER_CURRENT =  "USER_CURRENT";
        public static final String USER_CHECK_IN = "USER_CHECK_IN";
        public static final String USER_TRAINING = "USER_TRAINING";
        public static final String USER_RECORDS = "USER_CALORIES";
        public static final String USER_CALORIES = "USER_CALORIES";
        public static final String USER_MACROS = "USER_MACROS";
        public static final String USER_REGISTERED = "USER.APP.REGISTERED";
        public static final String IS_REGISTERED = "REGISTERED";

        public static final int CHECK = 21;
        public static final int REVIEW = 23;
        public static final int TRACK = 25;

        public static final int TYPE_VIEW_C = 0;   // start screen view visibility on user is not register
        public static final int TYPE_VIEW_L = 4;   // if user is registered to view login -> start main view
        public static final int TYPE_VIEW_S = 8;   // this view visible is registration user

        public static final int ROTATE_R = 0;
        public static final int ROTATE_L = 1;

        private static long _S_ = 1000L;
        private static long _M_ = _S_ * 60;
        public static long _HOUR = _M_ * 60;
        public static long _DAY = _HOUR * 24;
        public static long _WEEK_ = _DAY * 7;

        public static class Error{
            public static final int AGE = 7;
            public static final int WEIGHT = 0;
            public static final int HEIGHT = 1;
            public static final int NECK = 2;
            public static final int CHEST = 3;
            public static final int WAIST = 4;
            public static final int HIP = 5;
            public static final int THIGH = 6;
        }
    }

    public static class Preferences{
        private static final String PREFERENCES = "ua.i.tech.ploose.recalibrate.PREFERENCES";
        public static final String TEXT_CHECK_IN = "PROGRAM.INFO.CHECK.IN";
        public static final String TEXT_TRACK_PROGRESS = "PROGRAM.TRACK.PROGRESS";
        public static final String TEXT_PREVIEW_PERFORMANCE = "PROGRAM.PREVIEW.PERFORMANCE";
        public static final String TEXT_TERMS_CONDITIONS = "PROGRAM.TERMS.CONDITIONS";
        public static final String TEXT_ABOUT = "PROGRAM.INFO.TEXT_ABOUT";

        private static SharedPreferences getPreferences(final Context context){
            return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE);
        }

        public static void setStringPreferences(Context context, String value, final String KEY){
            if(getPreferences(context).edit().putString(KEY, value).commit()) {
                Log.d("work", "MainPref : save is ok" + value);
            } else{
                Log.d("work", "MainPref : save is error");
            }
        }

        public static String getStringPreferences(Context context, final String KEY){
            return getPreferences(context).getString(KEY, "");
        }
    }

    public static class Calc {

        private static final String TAG = Calc.class.getSimpleName() + " : ";

        public static void calcScore(Context context
                , final List<UserTraining> workout
                , final User user) {

            Recalibrate.LOG(TAG, "Score calc");

            final float LIGHT = 0.03F;
            final float MEDIUM = 0.05F;
            final float HIGH = 0.07F;

            final float ACTIVE = 1.4F;
            final float MODERATE = 1.3F;
            final float NO_ACTIVE = 1.2F;

            float score = 0.0F;
            final String[] intensity = context.getResources().getStringArray(R.array.intro_settings_collection_intensity);
            final String[] levelVal = context.getResources().getStringArray(R.array.intro_settings_collection_level_value);
            for (int i=0; i<workout.size(); i++) {
                float value = 0.0F;
                if (TextUtils.equals(workout.get(i).getIntensity(), intensity[1])) {
                    Recalibrate.LOG(TAG, "intensity : " + workout.get(i).getIntensity());
                    value = LIGHT;
                } else if (TextUtils.equals(workout.get(i).getIntensity(), intensity[2])) {
                    Recalibrate.LOG(TAG, "intensity : " + workout.get(i).getIntensity());
                    value = MEDIUM;
                } else if (TextUtils.equals(workout.get(i).getIntensity(), intensity[3])) {
                    Recalibrate.LOG(TAG, "intensity : " + workout.get(i).getIntensity());
                    value = HIGH;
                }
                float temp = (value * (Float.valueOf(workout.get(i).getDuration()) / 60F));
                Recalibrate.LOG(TAG, "value intensity : " + value);
                Recalibrate.LOG(TAG, "iteration : " + i + " val : " + temp);
                score = score + temp;
            }
            float level = 0.0F;
            if (TextUtils.equals(user.getLevel(), levelVal[1])) {
                level = ACTIVE;
            } else if (TextUtils.equals(user.getLevel(), levelVal[2])) {
                level = MODERATE;
            } else if (TextUtils.equals(user.getLevel(), levelVal[3])) {
                level = NO_ACTIVE;
            }
            score = score + level;
            user.setTotalActiveScore(score);
        }

        public static void calcCheckInProgress(Context context
                , UserCheckIn oldValue
                , UserCheckIn newValue
                , final User user) {

            Recalibrate.LOG(TAG, "CheckIn progress calc");

            if (oldValue != null) {
                newValue.setProgressWeight((-1 * (1 - (oldValue.getWeight() / newValue.getWeight()))) * 100);
                newValue.setProgressNeck(oldValue.getNeck() / newValue.getNeck());
                newValue.setProgressChest(oldValue.getChest() / newValue.getChest());
                newValue.setProgressWaist(oldValue.getWaist() / newValue.getWaist());
                newValue.setProgressHips(oldValue.getHips() / newValue.getHips());
                newValue.setProgressThighs(oldValue.getThighs() / newValue.getThighs());
            }
            if (TextUtils.equals(user.getGender(), context.getString(R.string.intro_view_reg_male))) {
                newValue.setBodyFat(calcMaleProgress(user.getHeight()
                        , newValue.getNeck()
                        , newValue.getWaist()));
            } else if (TextUtils.equals(user.getGender(), context.getString(R.string.intro_view_reg_female))) {
                newValue.setBodyFat(calcFemaleProgress(user.getHeight()
                        , newValue.getNeck()
                        , newValue.getWaist()
                        , newValue.getHips()));
            } else {
                throw new MinusValueException("Error value");
            }
            if (oldValue != null) {
                newValue.setProgressBodyFat(newValue.getBodyFat() - oldValue.getBodyFat());
            }
            newValue.setLBM((100 - newValue.getBodyFat()) * newValue.getWeight() / 100);
            if (oldValue != null) {
                newValue.setProgressLBM(newValue.getLBM() - oldValue.getLBM());
            }
        }

        private static float calcFemaleProgress(float height, float neck, float waist, float hip) {
            LOG(TAG, "CALCULATE PROGRESS FEMALE SECTION");
            float kf1 = 1.29579F;
            float kf2 = 0.35004F;
            float kf3 = 0.22100F;
            return (float) (495 / (kf1 - kf2 * Math.log10(waist + hip - neck) + kf3 * Math.log10(height)) - 450);
        }

        private static float calcMaleProgress(float height, float neck, float waist) {
            LOG(TAG, "CALCULATE PROGRESS MALE SECTION");
            float kf1 = 1.03240F;
            float kf2 = 0.19077F;
            float kf3 = 0.15456F;
            return (float) (495 / (kf1 - kf2 * Math.log10(waist - neck) + kf3 * Math.log10(height)) - 450);
        }

        public static void calcDeficitSurplus(Context context
                , UserCheckIn checkIn
                , User user) {

            Recalibrate.LOG(TAG, "Deficit Surplus calc");

            float kfgL = 0.90F;
            float kfgM = 1.00F;
            float kfgG = 1.05F;
            float valueGoals = 0.0F;
            String[] goals = context.getResources().getStringArray(R.array.intro_settings_collection_goal_value);
            if (TextUtils.equals(checkIn.getGoal(), goals[1])) {
                valueGoals = kfgL;
            } else if (TextUtils.equals(checkIn.getGoal(), goals[2])) {
                valueGoals = kfgM;
            } else if (TextUtils.equals(checkIn.getGoal(), goals[3])) {
                valueGoals = kfgG;
            }

            float valueGender = 0.0F;

            if (TextUtils.equals(user.getGender(), context.getString(R.string.intro_view_reg_male))) {
                if (checkIn.getBodyFat() > 20) {
                    valueGender = 0.5F;
                }
            } else if (TextUtils.equals(user.getGender(), context.getString(R.string.intro_view_reg_female))) {
                if (checkIn.getBodyFat() > 25) {
                    valueGender = 0.05F;
                }
            }
            user.setTotalDefSur(valueGoals - valueGender);
        }

        public static  final String KEY_Y_TRAINING = "Y";
        public static  final String KEY_N_TRAINING = "N";

        public static void calcBMR(Context context
                , UserCheckIn checkIn
                , User user) {

            Recalibrate.LOG(TAG, "BMR calc");

            if (TextUtils.equals(user.getGender(), context.getString(R.string.intro_view_reg_male))) {
                user.setTotalBMR(calcMaleCalories(user.getHeight(), checkIn.getWeight(), user.getAge()));
            } else if (TextUtils.equals(user.getGender(), context.getString(R.string.intro_view_reg_female))) {
                user.setTotalBMR(calcFemaleCalories(user.getHeight(), checkIn.getWeight(), user.getAge()));
            }

            Calories training = new Calories();
            Calories noTraining = new Calories();
            training.setCalories(user.getTotalActiveScore() * user.getTotalBMR() * user.getTotalDefSur());
            noTraining.setCalories(user.getTotalActiveScore() * user.getTotalBMR() * (user.getTotalDefSur() - 0.1F));

            HashMap<String, Calories> blockData = new HashMap<>();
            blockData.put(KEY_Y_TRAINING,  training);
            blockData.put(KEY_N_TRAINING, noTraining);
            checkIn.setCalories(blockData);

            calcEnergyRequirements(context, checkIn);
        }

        public static void calcWeekEnergyRequirements(Context context
                , UserCheckIn oldCheckIn
                , UserCheckIn newcheckIn
                , User user) {

            Recalibrate.LOG(TAG, "Energy Week Requirement calc");

            final String []goalsValue = context.getResources()
                    .getStringArray(R.array.intro_settings_collection_goal_value);

            if(TextUtils.equals(newcheckIn.getGoal(), goalsValue[1])){
                calcFatLossVariant(newcheckIn);
            }else if(TextUtils.equals(newcheckIn.getGoal(), goalsValue[2])){
                calcMaintenanceVariant(newcheckIn);
            }else if(TextUtils.equals(newcheckIn.getGoal(), goalsValue[3])){
                calcMassGainVariant(newcheckIn);
            }else return;

            Calories training = new Calories();
            Calories noTraining = new Calories();

            training.setCalories(oldCheckIn.getCalories().get(KEY_Y_TRAINING).getCalories()
                    + ( oldCheckIn.getCalories().get(KEY_Y_TRAINING).getCalories()
                    * newcheckIn.getCalorieAdjustmen() ));
            noTraining.setCalories(oldCheckIn.getCalories().get(KEY_N_TRAINING).getCalories()
                    + ( oldCheckIn.getCalories().get(KEY_N_TRAINING).getCalories()
                    * newcheckIn.getCalorieAdjustmen() ));

            HashMap<String, Calories> blockData = new HashMap<>();
            blockData.put(KEY_Y_TRAINING, training);
            blockData.put(KEY_N_TRAINING, noTraining);
            newcheckIn.setCalories(blockData);

            calcEnergyRequirements(context, newcheckIn);
        }

        private static int calcMaleCalories(float height, float weight, float age) {
            LOG(TAG, "CALCULATE CALORIES MALE SECTION");
            return (int) (10 * weight + 6.25 * height - 5 * age + 5);
        }

        private static int calcFemaleCalories(float height, float weight, float age) {
            LOG(TAG, "CALCULATE CALORIES FEMALE SECTION");
            return (int) (10 * weight + 6.25 * height - 5 * age - 161);
        }

        private static final int CHANGE_BF_OK    = 10;
        private static final int CHANGE_BW_OK    = 11;
        private static final int CHANGE_BF_HIGH = 12;
        private static final int CHANGE_BF_LOW = 13;
        private static final int CHANGE_BW_LOW = 14;
        private static final int CALC_ERROR = -1;

        private static /*int*/ void calcFatLossVariant(UserCheckIn checkIn){

            Recalibrate.LOG(TAG, "Fat loss variant calc");

            float knt_wp_ok = -0.005F;
            float knt_fp_low = -0.500F;
            float knt_fp_high = 1.500F;
            if(checkIn.getProgressWeight()>=knt_wp_ok&&checkIn.getProgressBodyFat()<=knt_fp_low){
                checkIn.setProcessAcceptable(CHANGE_BF_OK);
                checkIn.setCalorieAdjustmen(0.0F);
            }else if (checkIn.getProgressWeight()<=knt_wp_ok&&checkIn.getProgressBodyFat()>=knt_fp_low){
                checkIn.setProcessAcceptable(CHANGE_BW_OK);
                checkIn.setCalorieAdjustmen(0.0F);
            }else if (checkIn.getProgressBodyFat()>=knt_fp_high){
                checkIn.setProcessAcceptable(CHANGE_BF_HIGH);
                checkIn.setCalorieAdjustmen(0.1F);
            }else if (checkIn.getProgressBodyFat()<=knt_fp_low){
                checkIn.setProcessAcceptable(CHANGE_BF_LOW);
                checkIn.setCalorieAdjustmen(-0.05F);
            }else if (checkIn.getProgressWeight()>=knt_wp_ok){
                checkIn.setProcessAcceptable(CHANGE_BW_LOW);
                checkIn.setCalorieAdjustmen(-0.05F);
            }else {
                checkIn.setProcessAcceptable(CALC_ERROR);
                checkIn.setCalorieAdjustmen(0.0F);
            }
            Recalibrate.LOG(TAG, "Fat Loss - " + checkIn.getProcessAcceptable()
                    + " - " + checkIn.getCalorieAdjustmen());
        }

        private static final int INCREASING_BW_MUCH = 15;
        private static final int DECREASING_BW_MUCH = 16;
        private static final int INCREASING_BF_MUCH = 17;
        private static final int DECREASING_BF_MUCH = 18;
        private static final int PROGRESS_OKAY = 0;

        private static /*int*/ void calcMaintenanceVariant(UserCheckIn checkIn){

            Recalibrate.LOG(TAG, "Maintenance variant calc");

            float knt_m_bw_increasing = 0.005F;
            float knt_m_bw_decreasing = -0.500F;
            float knt_m_bf_increasing = 0.500F;
            float knt_m_bf_decreasing = -0.500F;

            if(checkIn.getProgressWeight()>=knt_m_bw_increasing){
                checkIn.setProcessAcceptable(INCREASING_BW_MUCH);
                checkIn.setCalorieAdjustmen(-0.05F);
            }else if(checkIn.getProgressWeight()<=knt_m_bw_decreasing){
                checkIn.setProcessAcceptable(DECREASING_BW_MUCH);
                checkIn.setCalorieAdjustmen(0.05F);
            }else if(checkIn.getProgressBodyFat()>=knt_m_bf_increasing){
                checkIn.setProcessAcceptable(INCREASING_BF_MUCH);
                checkIn.setCalorieAdjustmen(-0.05F);
            }else if(checkIn.getProgressBodyFat()<=knt_m_bf_decreasing){
                checkIn.setProcessAcceptable(DECREASING_BF_MUCH);
                checkIn.setCalorieAdjustmen(0.05F);
            }else{
                checkIn.setProcessAcceptable(PROGRESS_OKAY);
                checkIn.setCalorieAdjustmen(0.0F);
            }
            Recalibrate.LOG(TAG, "Fat Loss - " + checkIn.getProcessAcceptable()
                    + " - " + checkIn.getCalorieAdjustmen());
        }

        private static final int BW_LOSING = 19;
        private static final int BF_HIGH = 20;
        private static final int BW_LOW = 21;


        private static /*int*/ void calcMassGainVariant(UserCheckIn checkIn){

            Recalibrate.LOG(TAG, "Mass Gain variant calc");

            float knt_lmg_bw_losing = -0.0001F;
            float knt_lmg_bf_high = 1.00F;
            float knt_lmg_bf_low  = 0.0015F;

            if(checkIn.getProgressWeight()<=knt_lmg_bw_losing){
                checkIn.setProcessAcceptable(BW_LOSING);
                checkIn.setCalorieAdjustmen(0.1F);
            }else if(checkIn.getProgressBodyFat()>=knt_lmg_bf_high){
                checkIn.setProcessAcceptable(BF_HIGH);
                checkIn.setCalorieAdjustmen(-0.05F);
            }else if(checkIn.getProgressBodyFat()<=knt_lmg_bf_low){
                checkIn.setProcessAcceptable(BW_LOW);
                checkIn.setCalorieAdjustmen(0.05F);
            }else{
                checkIn.setProcessAcceptable(PROGRESS_OKAY);
                checkIn.setCalorieAdjustmen(0.0F);
            }

            Recalibrate.LOG(TAG, "Fat Loss - " + checkIn.getProcessAcceptable()
                    + " - " + checkIn.getCalorieAdjustmen());
        }

        private static void calcEnergyRequirements(Context context
                , UserCheckIn checkIn) {

            Recalibrate.LOG(TAG, "Energy requirement calc");

            //protein calculate
            float kfgL = 3.50F;
            float kfgM = 2.50F;
            float kfgG = 3.00F;
            float valueGoals = 0.0F;
            String[] goals = context.getResources().getStringArray(R.array.intro_settings_collection_goal_value);
            if (TextUtils.equals(checkIn.getGoal(), goals[1])) {
                valueGoals = kfgL;
            } else if (TextUtils.equals(checkIn.getGoal(), goals[2])) {
                valueGoals = kfgM;
            } else if (TextUtils.equals(checkIn.getGoal(), goals[3])) {
                valueGoals = kfgG;
            }
            checkIn.getCalories().get(KEY_Y_TRAINING).setProtein((int) (checkIn.getLBM() * valueGoals));

            checkIn.getCalories().get(KEY_Y_TRAINING).setProteinPercent(
                    checkIn.getCalories().get(KEY_Y_TRAINING).getProtein()
                            * 4
                            / checkIn.getCalories().get(KEY_Y_TRAINING).getCalories());

            checkIn.getCalories().get(KEY_N_TRAINING).setProtein((int) (checkIn.getLBM() * valueGoals));
            checkIn.getCalories().get(KEY_N_TRAINING).setProteinPercent(checkIn.getCalories()
                    .get(KEY_N_TRAINING).getProtein() * 4 / checkIn.getCalories()
                    .get(KEY_N_TRAINING).getCalories());

            //fat calculate
            float kf1 = 0.20F;
            float kf2 = 0.25F;
            float kf3 = 0.30F;
            float kf4 = 0.35F;
            float valueFat = 0.0F;
            if (checkIn.getBodyFat() <= 10) {
                valueFat = kf1;
            } else if (checkIn.getBodyFat() <= 20) {
                valueFat = kf2;
            } else if (checkIn.getBodyFat() <= 30) {
                valueFat = kf3;
            } else
                valueFat = kf4;

            float kf5 = 0.05F;
            float valueFatGoals = 0.0F;
            if (TextUtils.equals(checkIn.getGoal(), goals[2])) {
                valueFatGoals = kf5;
            } else if (TextUtils.equals(checkIn.getGoal(), goals[3])) {
                valueFatGoals = kf5;
            }

            checkIn.getCalories().get(KEY_Y_TRAINING)
                    .setFatPercent(valueFat + valueFatGoals);

            checkIn.getCalories().get(KEY_Y_TRAINING).setFat(
                    (int) (checkIn.getCalories().get(KEY_Y_TRAINING).getCalories()
                            * checkIn.getCalories().get(KEY_Y_TRAINING).getFatPercent() / 9));

            checkIn.getCalories().get(KEY_N_TRAINING)
                    .setFat(checkIn.getCalories().get(KEY_Y_TRAINING).getFat());
            checkIn.getCalories().get(KEY_N_TRAINING).setFatPercent(
                    checkIn.getCalories().get(KEY_N_TRAINING).getFat()
                            * 9
                            / checkIn.getCalories().get(KEY_N_TRAINING).getCalories());

            //carbohydrates calculate

            checkIn.getCalories().get(KEY_Y_TRAINING).setCarbohydrate((int)((checkIn.getCalories()
                    .get(KEY_Y_TRAINING).getCalories()-(checkIn.getCalories()
                    .get(KEY_Y_TRAINING).getProtein()*4)-(checkIn.getCalories()
                    .get(KEY_Y_TRAINING).getFat()*9))/4));

            checkIn.getCalories().get(KEY_Y_TRAINING).setCarbohydratePercent(
                    (checkIn.getCalories().get(KEY_Y_TRAINING).getCarbohydrate()
                            *4
                            /checkIn.getCalories().get(KEY_Y_TRAINING).getCalories()));

            checkIn.getCalories().get(KEY_N_TRAINING).setCarbohydrate((int)((checkIn.getCalories()
                    .get(KEY_N_TRAINING).getCalories()-(checkIn.getCalories()
                    .get(KEY_N_TRAINING).getProtein()*4)-(checkIn.getCalories()
                    .get(KEY_N_TRAINING).getFat()*9))/4));

            checkIn.getCalories().get(KEY_N_TRAINING).setCarbohydratePercent(
                    (checkIn.getCalories().get(KEY_N_TRAINING).getCarbohydrate()
                            * 4
                            / checkIn.getCalories().get(KEY_N_TRAINING).getCalories()));
        }
    }

    private class TestCalcalate {

        User current = new User();
        UserCheckIn checkIn = new UserCheckIn();

        public TestCalcalate(){

            current.setId("test");
            current.setFirstName("Igor");
            current.setLastName("Morosov");
            current.setAge(26);
            current.setHeight(174);
            current.setGender("Male");
            current.setGoal("gain_muscle");
            current.setLevel("ACTIVE");
            current.setStatus("test");
            current.setCreated(System.currentTimeMillis());

            checkIn.setWeight(85.0F);
            checkIn.setNeck(40.0F);
            checkIn.setChest(105.0F);
            checkIn.setWaist(81.0F);
            checkIn.setHips(99.0F);
            checkIn.setThighs(59.0F);
            checkIn.setGoal(current.getGoal());
            checkIn.setCreated(System.currentTimeMillis());

            HashMap<String, Calories> temp = new HashMap<>();
            temp.put(Recalibrate.Calc.KEY_Y_TRAINING, new Calories());
            temp.put(Recalibrate.Calc.KEY_N_TRAINING, new Calories());
            checkIn.setCalories(temp);

            ArrayList<UserTraining> worckout = new ArrayList<>();
            UserTraining training = new UserTraining();
            training.setDuration("90");
            training.setFrequency("1 per week");
            training.setIntensity("high");
            training.setId("test");
            training.setUserId(current.getId());
            training.setCreated(System.currentTimeMillis());
            training.printFieldValueTraining();
            worckout.add(training);

            Recalibrate.Calc.calcScore(getApplicationContext(), worckout, current);
            Recalibrate.Calc.calcCheckInProgress(getApplicationContext(), null, checkIn, current);
            Recalibrate.Calc.calcDeficitSurplus(getApplicationContext(), checkIn, current);
            Recalibrate.Calc.calcBMR(getApplicationContext(), checkIn,current);
//            Recalibrate.Calc.cal

            current.printLogValueField();
            checkIn.printfieldValueChekIn();

            UserCheckIn nCheckIn = new UserCheckIn();
            nCheckIn.setWeight(85.0F);
            nCheckIn.setNeck(40.0F);
            nCheckIn.setChest(105.0F);
            nCheckIn.setWaist(81.0F);
            nCheckIn.setHips(99.0F);
            nCheckIn.setThighs(59.0F);
            nCheckIn.setGoal(current.getGoal());
//            nCheckIn.setRecords(Recalibrate.Helper.createAllRecords());
            nCheckIn.setCreated(System.currentTimeMillis());

            Recalibrate.Calc.calcCheckInProgress(getApplicationContext(), checkIn, nCheckIn, current);
            Recalibrate.Calc.calcDeficitSurplus(getApplicationContext(), nCheckIn, current);
            Recalibrate.Calc.calcBMR(getApplicationContext(), nCheckIn, current);

            nCheckIn.printfieldValueChekIn();
        }



    }

}
