package ua.i.tech.ploose.recalibrate.recalibrate.fragments.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomStringListener;

/**
 * Created by Developer on 26.12.2016.
 */

public class Check extends Fragment {

    private CustomStringListener listener = null;
    private String introText;

    public static Fragment createFragment(){
        return new Check();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CustomStringListener)
            listener = (CustomStringListener) context;
        else
            throw new ClassCastException("Error implements interface CustomStringListener...");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater
            , @Nullable ViewGroup container
            , @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_intro_pager_check, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView text = (TextView) view.findViewById(R.id.introFragmentCheckText);
        assert listener != null;
        text.setText(introText = listener.onStringDataListener(Recalibrate.KEY.CHECK));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
