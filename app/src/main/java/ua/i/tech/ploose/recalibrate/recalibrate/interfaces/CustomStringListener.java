package ua.i.tech.ploose.recalibrate.recalibrate.interfaces;

/**
 * Created by Developer on 30.12.2016.
 */

public interface CustomStringListener {
    String onStringDataListener(int type);
}
