package ua.i.tech.ploose.recalibrate.recalibrate.fragments.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ua.i.tech.ploose.recalibrate.recalibrate.R;

/**
 * Created by Developer on 26.12.2016.
 */

public class DataTable extends Fragment {

    public static Fragment createFragment(){
        return new DataTable();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_table_data_view, container, false);
    }
}
