package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuSettings;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;

/**
 * Created by Developer on 26.12.2016.
 */

public class About extends Fragment {

    public static Fragment createFragment(){
        return new About();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_menu_settings_a, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView textAbout = (TextView) view.findViewById(R.id.mainSettingsMenuAbout);
        textAbout.setText(Recalibrate.Preferences.
                getStringPreferences(getContext(), Recalibrate.Preferences.TEXT_ABOUT));

    }
}
