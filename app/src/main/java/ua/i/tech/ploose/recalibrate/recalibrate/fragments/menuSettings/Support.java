package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.activities.MainProf;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;

/**
 * Created by Developer on 26.12.2016.
 */

public class Support extends Fragment {

    private final String TAG = Support.class.getSimpleName() + " : ";

    private OnFAQUserListener listener;

    public static Fragment createFragment(){
        return new Support();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnFAQUserListener)
            listener = (OnFAQUserListener) context;
        else
            throw new ClassCastException("Error implements interface OnFAQUserListener!...");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_menu_settings_s, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final EditText question = (EditText) view.findViewById(R.id.FAQ_edit_text);
        TextView btnSend = (TextView) view.findViewById(R.id.mainSettingsMenuSupportSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkVerifyDataField(question)) {
                    sendMail(question, new MessageSendCompleted() {
                        @Override
                        public void completed() {
                            Recalibrate.Helper.hideKeyboard((MainProf)getActivity());
                            question.setText("");
                        }
                    });
                }
            }
        });

        View supportView = view.findViewById(R.id.main_menu_support_view);
        supportView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private boolean checkVerifyDataField(EditText text){
        Recalibrate.LOG(TAG, text.getText().toString());
        if(TextUtils.isEmpty(text.getText())
                || TextUtils.equals(text.getText(), "")
                || TextUtils.equals(text.getText(), getString(R.string.main_setting_faq_text_hint))){
            Toast.makeText(getContext(),
                    R.string.main_setting_error_text_letter,
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void sendMail(final EditText textMail, final MessageSendCompleted completed){
        Recalibrate.Helper.hideKeyboard((MainProf)getActivity());
        final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("plain/text");
        // Кому
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
                new String[] { getResources().getString(R.string.mail_support_department)});
        // Зачем
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                getResources().getString(R.string.main_setting_faq_theme_text));
        // О чём

        assert listener != null;
        User user = listener.onUserListener();
        String data = Html.fromHtml(
                "<p>"+ textMail.getText().toString() +"</p>" +
                        "<p>From: " + user.getFirstName() + " " + user.getLastName() + "</p>")
                .toString();

        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, data);
//                textMail.getText().toString());
//                    // С чем
//                    emailIntent.putExtra(
//                            android.content.Intent.EXTRA_STREAM,
//                            Uri.parse("file://"
//                                    + Environment.getExternalStorageDirectory()
//                                    + "/Клипы/SOTY_ATHD.mp4"));
//                    emailIntent.setType("text/video");
        // Поехали!
        getContext().startActivity(Intent.createChooser(emailIntent,
                getResources().getString(R.string.mail_support_department_send)));
        completed.completed();
    }

    private interface MessageSendCompleted {
        void completed();
    }

    public interface OnFAQUserListener{
        User onUserListener();
    }
}
