package ua.i.tech.ploose.recalibrate.recalibrate.exeption;

/**
 * Created by root on 16.02.17.
 */

public class MinusValueException extends RuntimeException {
    public MinusValueException() {super();}
    public MinusValueException(String message) {super(message);}
}
