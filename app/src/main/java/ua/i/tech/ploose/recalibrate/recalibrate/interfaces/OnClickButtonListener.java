package ua.i.tech.ploose.recalibrate.recalibrate.interfaces;

import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

/**
 * Created by Developer on 07.02.2017.
 */

public interface OnClickButtonListener {
    void onClickListenerSave(UserCheckIn newCheckIn);
    void onClickListenerSave(List<UserCheckIn> listCheckIn);
    void onChangeInfoListener();
}