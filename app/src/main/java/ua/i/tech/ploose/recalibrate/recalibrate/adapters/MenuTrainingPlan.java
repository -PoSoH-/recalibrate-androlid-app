package ua.i.tech.ploose.recalibrate.recalibrate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;
//import ua.i.tech.ploose.recalibrate.recalibrate.models.Workout;

//import static com.activeandroid.Cache.getContext;

/**
 * Created by Developer on 03.01.2017.
 */

public class MenuTrainingPlan extends BaseAdapter {

    private List<UserTraining> listWorkOut = new ArrayList<>();
    private Context context;

    public MenuTrainingPlan(Context context, List<UserTraining> objects) {
        this.context = context;
        this.listWorkOut = objects;
    }

    @Override
    public int getCount() {
        return listWorkOut.size();
    }

    @Override
    public Object getItem(int i) {
        return listWorkOut.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        UserTraining currentPlan = listWorkOut.get(position);

        if(convertView == null){
            convertView = LayoutInflater.from(this.context)
                    .inflate(R.layout.item_workout_view, parent, false);
        }
        TextView number = (TextView) convertView.findViewById(R.id.workoutNamePosition);
        number.setText(String.valueOf(position));
        TextView duration = (TextView)convertView.findViewById(R.id.workoutDurationView);
        duration.setText(currentPlan.getDuration());
        duration.setTextColor(0xFF555555);
        TextView intensity = (TextView)convertView.findViewById(R.id.workoutIntensityView);
        intensity.setText(currentPlan.getIntensity());
        intensity.setTextColor(0xFF555555);
        TextView frequency = (TextView)convertView.findViewById(R.id.workoutFrequencyView);
        frequency.setText(currentPlan.getFrequency());
        frequency.setTextColor(0xFF555555);
        return convertView;
    }

    public void updateAllWorkout(List<UserTraining> allWorkout){
        this.listWorkOut = allWorkout;
        notifyDataSetChanged();
    }
}
