package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.activities.Base;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.SettingFAQListAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;

/**
 * Created by Developer on 26.12.2016.
 */

public class FAQ extends Fragment {

    private ArrayList<Map> data = new ArrayList<>();

    public static Fragment createFragment(){
        return new FAQ();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_menu_settings_f, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((Base)getActivity()).showProgressBar();
        ListView faqList = (ListView) view.findViewById(R.id.mainSettingsMenuFAQ);
        final SettingFAQListAdapter adapter = new SettingFAQListAdapter(getContext(), data);
        faqList.setAdapter(adapter);
        Backend.loadFAQInstruction(new Backend.OnRecalibrateMapListener() {
            @Override
            public void onSuccess(List<Map> FAQInformation) {
                ((Base)getActivity()).hideProgressBar();
                adapter.updateFAQList(FAQInformation);
            }

            @Override
            public void onError(int error) {
                ((Base)getActivity()).hideProgressBar();
                Toast.makeText(getContext(), "Error load FAQ!...", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
