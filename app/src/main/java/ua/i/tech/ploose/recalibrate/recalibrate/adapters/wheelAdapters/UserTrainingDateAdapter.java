package ua.i.tech.ploose.recalibrate.recalibrate.adapters.wheelAdapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Record;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

public class UserTrainingDateAdapter extends AbstractWheelTextAdapter {

    private int selectedItem = 0;
    private String[]info;
    private View itemView[];

    public UserTrainingDateAdapter(Context context, List<Record> currentFilter, int selectedPosition ) {
        super(context, R.layout.item_user_setup_dialog_view, NO_RESOURCE);

        this.itemView = new View[currentFilter.size()];
        info = new String [currentFilter.size()];
        updateValues(currentFilter);
        this.selectedItem = selectedPosition;
        setSelectedItem(selectedPosition);
        getItemView(this.selectedItem);
        setItemTextResource(R.id.menuSettingViewValue);
    }

    @Override
    public View getItem(int index, View convertView, ViewGroup parent) {
        View view = super.getItem(index, convertView, parent);
        TextView timeWheelItem = (TextView) view.findViewById(R.id.menuSettingViewValue);
//        if()
        timeWheelItem.setText(info[index].toString());
        if (index == selectedItem){
            timeWheelItem.setTextColor(context.getResources().getColor(R.color.colorRed_B));
            timeWheelItem.setTextSize(context.getResources().getDimension(R.dimen.intro_text_size_in_focus));
        } else {
            timeWheelItem.setTextColor(context.getResources().getColor(R.color.colorGray_138));
            timeWheelItem.setTextSize(context.getResources().getDimension(R.dimen.intro_text_size_out_focus));
        }
        itemView[index] = view;
        return view;
    }

    @Override
    protected CharSequence getItemText(int index) {
        return info[index];
    }

    @Override
    public int getItemsCount() {
        return this.info.length;
    }

    @Override
    public View getItemView(int index){
        return itemView[index];
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }
    public int getSelectedItem() {return this.selectedItem;}

    public void updateValues(List<Record> allData) {
        Calendar currentDay = Calendar.getInstance();
        currentDay.set(Calendar.HOUR, 0);
        currentDay.set(Calendar.MINUTE, 0);
        currentDay.set(Calendar.SECOND, 0);
        currentDay.set(Calendar.HOUR_OF_DAY, 3);

        int position = allData.size() - 1;
        Calendar cal = Calendar.getInstance();
        for(int i = 0; i< allData.size(); i++){
            cal.setTimeInMillis(allData.get(position).getCurrentDate());

            Recalibrate.LOG("CREATE WHEEL", currentDay.getTimeInMillis()
                    + " <= "
                    + allData.get(position).getCurrentDate()
                    + " >  "
                    + (currentDay.getTimeInMillis() + Recalibrate.KEY._DAY));

            if(((currentDay.getTimeInMillis() - Recalibrate.KEY._DAY)/1000)
                    <= (allData.get(position).getCurrentDate()/1000)
                    && (allData.get(position).getCurrentDate()/1000)
                    < (currentDay.getTimeInMillis()/1000)) {
                info[i] = "Yesterday (" + new SimpleDateFormat("dd MMMM").format(cal.getTime()) + ")";
            }else if((currentDay.getTimeInMillis()/1000)
                    <= (allData.get(position).getCurrentDate()/1000)
                    && (allData.get(position).getCurrentDate()/1000)
                    < ((currentDay.getTimeInMillis() + Recalibrate.KEY._DAY)/1000)) {
                info[i] = "Today (" + new SimpleDateFormat("dd MMMM").format(cal.getTime()) + ")";
            }else{
                info[i] = new SimpleDateFormat("dd MMMM").format(cal.getTime());
            }
//            Recalibrate.LOG(UserCheckIn.class.getSimpleName() + " : "
//                    , info[i]);
            position--;
        }
        notifyDataChangedEvent();
    }
}
