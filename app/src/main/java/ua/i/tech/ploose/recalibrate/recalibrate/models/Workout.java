//package ua.i.tech.ploose.recalibrate.recalibrate.models;
//
//import com.activeandroid.Model;
//import com.activeandroid.annotation.Column;
//import com.activeandroid.annotation.Table;
//
//import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
//
///**
// * Created by Developer on 29.12.2016.
// */
//
//@Table(name = "workout")
//public class Workout extends Model {
//
//    @Column(name = "workout_id", unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
//    private String ID = Recalibrate.Helper.createObjectId();  // Масса
//    @Column(name = "workout_user_id")
//    private String UserId;
//    @Column(name = "workout_duration")
//    private String Duration;
//    @Column(name = "workout_intensity")
//    private String Intensity;
//    @Column(name = "workout_frequency")
//    private String Frequency;
//
//    public Workout(){super();}
//
//    public Workout(String UserId,
//                   String Duration,
//                   String Intensity,
//                   String Frequency){
//        super();
//        this.UserId = UserId;
//        this.Duration = Duration;
//        this.Intensity = Intensity;
//        this.Frequency = Frequency;
//    }
//
//    public String getID() {
//        return ID;
//    }
//
//    public void setID(String ID) {
//        this.ID = ID;
//    }
//
//    public String getUserId() {
//        return UserId;
//    }
//
//    public void setUserId(String userId) {
//        UserId = userId;
//    }
//
//    public String getDuration() {
//        return Duration;
//    }
//
//    public void setDuration(String duration) {
//        Duration = duration;
//    }
//
//    public String getIntensity() {
//        return Intensity;
//    }
//
//    public void setIntensity(String intensity) {
//        Intensity = intensity;
//    }
//
//    public String getFrequency() {
//        return Frequency;
//    }
//
//    public void setFrequency(String frequency) {
//        Frequency = frequency;
//    }
//}
