//package ua.i.tech.ploose.recalibrate.recalibrate.activities;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v4.view.ViewPager;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageView;
//import android.widget.Toast;
//
//import com.google.firebase.auth.FirebaseAuth;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogCheck;
//import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;
//import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
//import ua.i.tech.ploose.recalibrate.recalibrate.R;
//import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
//import ua.i.tech.ploose.recalibrate.recalibrate.adapters.MainPageAdapter;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogSuccessful;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuTracking;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuGoal;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuPlan;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuSettings;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuCheckIn;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.About;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.CountMacros;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.FAQ;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.MealPlans;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.Support;
//import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.Terms;
//import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;
//import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnDialogFragmentListener;
//import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
//import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
//import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;
//
//public class Main extends Base implements CustomAdapterListener,
//        OnDialogFragmentListener
//        , MenuPlan.OnUserTrainingListListener
//        , Support.OnFAQUserListener
//        , OnClickButtonListener {
//
//    private final String TAG = Main.class.getSimpleName() + " : ";
//
//    public static final int PAGER_TABLE = 100;
//    public static final int PAGER_PERFORMANCE = 110;
//    public static final int PAGER_PROFILE = 120;
//
//    public static final int VIEW_MENU_GONE = 200;     // menu not loaded
//    public static final int VIEW_MENU_CHECK = 210;    // menu check in data user
//    public static final int VIEW_MENU_TRACKING = 220; // menu tracking record
//    public static final int VIEW_MENU_SETTING = 230;  // menu settings
//
//    public static final int VIEW_MENU_SETTING_A = 0;//231;
//    public static final int VIEW_MENU_SETTING_H = 1;//232;
//    public static final int VIEW_MENU_SETTING_P = 2;//233;
//    public static final int VIEW_MENU_SETTING_S = 3;//234;
//    public static final int VIEW_MENU_SETTING_F = 4;//235;
//    public static final int VIEW_MENU_SETTING_C = 5;//236;
//
//    public static final int VIEW_MENU_ABOUT_P = 0;//231;
//    public static final int VIEW_MENU_ABOUT_G = 1;//232;
//    public static final int VIEW_MENU_ABOUT_T = 2;//233;
//    public static final int VIEW_MENU_ABOUT_L = 3;//234;
//
//    private int pagerState = PAGER_TABLE;
//    private int menuSate =  VIEW_MENU_GONE;
//    private boolean menuSettings = false;
//    private boolean menuAbout = false;
//
//    private ImageView indicatorsDisable[];
//    private ImageView indicatorsEnable[];
//    private FrameLayout buttons[];
//    private ImageView btnClosed;
//    private ImageView btnBack;
//    private ImageView btnAdd;
//    private ImageView btnOk;
//    private ImageView btnSettings;
//    private ViewPager pager;
//
//    private int menuContainerId;
//    private FrameLayout menuContainer;
//
//    private User currentUser = null;
//    private UserCheckIn userCheckIn = null;
//    private ArrayList<UserTraining> plainTraining = new ArrayList<>();
//
//    private long _S_ = 1000L;
//    private long _M_ = _S_ * 60;
//    private long _M_10 = _S_ * 60 * 10;
//    private long _H_ = _M_ * 60;
//    private long _D_ = _H_ * 24;
//    private long _W_ = _D_ * 7;
//    private long _C_ = 0L;
//
//    public static void show(AppCompatActivity activity){
//        Intent intent = new Intent(activity, Main.class);
//        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side
//                , R.anim.activity_start_exit_right_side);
//        activity.finish();
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.a_main);
//
//        hideToolBar();
//
//        menuContainerId = R.id.mainMenuItemsViewContainer;
//        menuContainer = (FrameLayout) findViewById(R.id.mainMenuItemsViewContainer);
//
//        indicatorsDisable = new ImageView[]{
//                (ImageView) findViewById(R.id.mainIndicatorCupDisable),
//                (ImageView) findViewById(R.id.mainIndicatorGraphDisable),
//                (ImageView) findViewById(R.id.mainIndicatorHumanDisable)
//        };
//        indicatorsEnable = new ImageView[]{
//                (ImageView) findViewById(R.id.mainIndicatorCupEnable),
//                (ImageView) findViewById(R.id.mainIndicatorGraphEnable),
//                (ImageView) findViewById(R.id.mainIndicatorHumanEnable)
//        };
//
//        pager = (ViewPager) findViewById(R.id.mainViewPager);
//        MainPageAdapter adapterPager = new MainPageAdapter(getSupportFragmentManager());
//        pager.setAdapter(adapterPager);
//        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//            @Override
//            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
//            }
//
//            @Override
//            public void onPageSelected(int position) {
////                activityScreenOrientation(position);
//                changeScreenMode(position);
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });
//
//        buttons = new FrameLayout[]{
//                (FrameLayout) findViewById(R.id.mainButtonCup),
//                (FrameLayout) findViewById(R.id.mainButtonGraph),
//                (FrameLayout) findViewById(R.id.mainButtonHuman)
//        };
//
//        buttons[0].setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pager.setCurrentItem(0);
//            }
//        });
//        buttons[1].setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pager.setCurrentItem(1);
//            }
//        });
//        buttons[2].setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                pager.setCurrentItem(2);
//            }
//        });
//
//        initializeImageIndicator();
//        initToolbarButtons();
//        loadInfoIsDataBase();
//    }
//
//    @Override
//    public void onBackPressed() {
//        if(menuAbout || menuSettings){
//            closeMenuView();
//        }else {
//            if (menuSate == VIEW_MENU_GONE) {
//                if (pagerState == PAGER_TABLE)
//                    super.onBackPressed();
//                pager.setCurrentItem(0);
//            } else
//                closeMenuView();
//        }
//    }
//
//    private void initializeImageIndicator(){
//        addedImageResourceToView(indicatorsDisable[0],R.raw.btn_cup_black, 512,512);
//        addedImageResourceToView(indicatorsDisable[1],R.raw.btn_graph_black, 512,512);
//        addedImageResourceToView(indicatorsDisable[2],R.raw.btn_man_black, 512,512);
//        addedImageResourceToView(indicatorsEnable[0],R.raw.btn_cup_red, 512,512);
//        addedImageResourceToView(indicatorsEnable[1],R.raw.btn_graph_red, 512,512);
//        addedImageResourceToView(indicatorsEnable[2],R.raw.btn_man_red, 512,512);
//
//        deselectedButton();
//        selectedButton(0);
//    }
//
//    private void changeScreenMode(int position){
//        deselectedButton();
//        hideAllToolbarButtons();
//        switch (position) {
//            case 0:
//                pagerState = PAGER_TABLE;
//                hideToolBar();
//                selectedButton(position);
//                break;
//            case 1:
//                pagerState = PAGER_PERFORMANCE;
//                showToolBar();
//                selectedButton(position);
//                break;
//            case 2:
//                pagerState = PAGER_PROFILE;
//                showToolBar();
//                selectedButton(position);
//                break;
//        }
//        showToolbarButton();
//    }
//
////    private void activityScreenOrientation(final int position){
////        if(position == 0)
////            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
////        else
////            setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
////
////    }
//
//    private void hideAllToolbarButtons(){
//        hideButtonMenuAdd();
//        hideButtonMenuOk();
//        hideButtonMenuSettings();
//        hideButtonMenuBack();
//        hideButtonMenuClosed();
//    }
//
//    private void showToolbarButton(){
//        if(menuSate == VIEW_MENU_GONE){
//            if(pagerState == PAGER_PROFILE){
//                setLabel(R.string.main_profile);
//                showButtonMenuSettings();
//            }else if(pagerState == PAGER_PERFORMANCE){
//                setLabel(R.string.main_performance);
//                showButtonMenuOk();
//                showButtonMenuAdd();
//            }
//        }
//    }
//
//    private void deselectedButton(){
//        indicatorsDisable[0].setVisibility(View.VISIBLE);
//        indicatorsDisable[1].setVisibility(View.VISIBLE);
//        indicatorsDisable[2].setVisibility(View.VISIBLE);
//        indicatorsEnable[0].setVisibility(View.GONE);
//        indicatorsEnable[1].setVisibility(View.GONE);
//        indicatorsEnable[2].setVisibility(View.GONE);
//    }
//
//    private void selectedButton(final int position){
//        indicatorsDisable[position].setVisibility(View.GONE);
//        indicatorsEnable[position].setVisibility(View.VISIBLE);
//    }
//
//    private void initToolbarButtons(){
//        btnClosed = getButtonMenuClosed();
//        btnClosed.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(menuSate == VIEW_MENU_CHECK)
//                    closeMenuView();
//                else if(menuSate == VIEW_MENU_TRACKING)
//                    closeMenuView();
//            }
//        });
//
//        btnBack = getButtonMenuBack();
//        btnBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                closeMenuView();
//            }
//        });
//
//        btnAdd = getButtonMenuAdd();
//        btnAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showTrackingView();
//            }
//        });
//        btnOk = getButtonMenuOk();
//        btnOk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showCheckInView();
//            }
//        });
//        btnSettings = getButtonMenuSettings();
//        btnSettings.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//               showSettingsView();
//            }
//        });
//    }
//
//    private void showCheckInView(){
//        menuSate = VIEW_MENU_CHECK;
//        hideAllToolbarButtons();
//        showButtonMenuClosed();
//        setFragmentAnimation(getSupportFragmentManager(),
//                MenuCheckIn.createFragment(),
//                menuContainerId,
//                false,
//                UP_ANIMATION);
//        menuContainer.setVisibility(View.VISIBLE);
//        setLabel(R.string.main_add_label_text);
//    }
//    private void showTrackingView(){
//        if(System.currentTimeMillis() - _C_ < _M_) {
//            DialogCheck.show(this);
//        }else {
//            menuSate = VIEW_MENU_TRACKING;
//            hideAllToolbarButtons();
//            showButtonMenuClosed();
//            setFragmentAnimation(getSupportFragmentManager(),
//                    MenuTracking.createFragment(),
//                    menuContainerId,
//                    false,
//                    UP_ANIMATION);
//            menuContainer.setVisibility(View.VISIBLE);
//            setLabel(R.string.main_ok_label_text);
//        }
//    }
//    private void showSettingsView(){
//        menuSate = VIEW_MENU_SETTING;
//        hideAllToolbarButtons();
//        showButtonMenuBack();
//        setFragmentAnimation(getSupportFragmentManager(),
//                MenuSettings.createFragment(),
//                menuContainerId,
//                false,
//                RIGHT_ANIMATION);
//        menuContainer.setVisibility(View.VISIBLE);
//        setLabel(R.string.main_settings_label_text);
//    }
//
//    private void closeMenuView(){
//        if(menuSate == VIEW_MENU_SETTING){
//            if(menuSettings) {
//                setFragmentAnimation(getSupportFragmentManager(),
//                        MenuSettings.createFragment(),
//                        menuContainerId,
//                        false,
//                        LEFT_ANIMATION);
//                menuSettings = false;
//                setLabel(R.string.main_settings_label_text);
//                return;
//            }else{
//                setEmptyFragment(LEFT_ANIMATION);
//            }
//        }else{
//            if(menuAbout){
//                setEmptyFragment(LEFT_ANIMATION);
//                menuAbout = false;
//                setLabel(R.string.main_settings_menu_about_label);
//            }else {
//                setEmptyFragment(DOWN_ANIMATION);
//            }
//        }
//        menuSate = VIEW_MENU_GONE;
//        hideAllToolbarButtons();
//        showToolbarButton();
//    }
//
//    private void setEmptyFragment(int typeAnimation){
//        setFragmentAnimation(getSupportFragmentManager(),
//                new Fragment(),
//                menuContainerId,
//                false,
//                typeAnimation);
//    }
//
//    private void loadInfoIsDataBase(){
//        Backend.loadUserData(new Backend.OnRecalibrateUserListener() {
//            @Override
//            public void onSuccess(User result) {
//                currentUser = result;
//            }
//
//            @Override
//            public void onError(int error) {
//                String errorView = "";
//                switch (error) {
//                    case Backend.LOAD_ERROR:
//                        errorView = "Load Error";
//                        break;
//                }
//                Toast.makeText(getApplicationContext(), errorView, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        Backend.loadUserTraining(new Backend.OnRecalibrateTrainingListener() {
//            @Override
//            public void onSuccess(List result) {
//                plainTraining = (ArrayList<UserTraining>) result;
//            }
//
//            @Override
//            public void onError(int error) {
//                String errorView = "";
//                switch (error) {
//                    case Backend.LOAD_ERROR:
//                        errorView = "Load Error";
//                        break;
//                }
//                Toast.makeText(getApplicationContext(), errorView, Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {
//            @Override
//            public void onSuccess(UserCheckIn result) {
//                userCheckIn = result;
//            }
//
//            @Override
//            public void onError(int error) {
//
//            }
//        });
//    }
//
//    @Override
//    public void onPositionSelected(int position) {
//        Fragment fragment = new Fragment();
//        if(menuSate == VIEW_MENU_SETTING){
//            Recalibrate.LOG(TAG, "Menu Settings position " + position);
//            switch (position){
//                case VIEW_MENU_SETTING_A:
//                    setLabel(R.string.main_settings_menu_about_label);
//                    fragment = About.createFragment();
//                    break;
//                case VIEW_MENU_SETTING_H:
//                    setLabel(R.string.main_settings_menu_macros_label);
//                    fragment = CountMacros.createFragment();
//                    break;
//                case VIEW_MENU_SETTING_P:
//                    setLabel(R.string.main_settings_menu_plans_label);
//                    fragment = MealPlans.createFragment();
//                    break;
//                case VIEW_MENU_SETTING_S:
//                    setLabel(R.string.main_settings_menu_support_label);
//                    fragment = Support.createFragment();
//                    break;
//                case VIEW_MENU_SETTING_F:
//                    setLabel(R.string.main_settings_menu_faq_label);
//                    fragment = FAQ.createFragment();
//                    break;
//                case VIEW_MENU_SETTING_C:
//                    setLabel(R.string.main_settings_menu_terms_label);
//                    fragment = Terms.createFragment();
//                    break;
//            }
//            menuSettings = true;
//        }else if (pagerState == PAGER_PROFILE) {
//            Recalibrate.LOG(TAG, "Menu profile position " + position);
//            switch (position){
//                case VIEW_MENU_ABOUT_P:
//                    fragment = MenuCheckIn.createFragment();
//                    setLabel(R.string.main_about_menu_check_label);
//                    break;
//                case VIEW_MENU_ABOUT_G:
//                    fragment = MenuGoal.createFragment();
//                    setLabel(R.string.main_about_menu_goals_label);
//                    break;
//                case VIEW_MENU_ABOUT_T:
//                    fragment = MenuPlan.createFragment();
//                    setLabel(R.string.main_about_menu_plan_label);
//                    break;
//                case VIEW_MENU_ABOUT_L:
//                    FirebaseAuth.getInstance().signOut();
//                    Intro.show(Main.this, null);
//                    return;
//
//            }
//            hideAllToolbarButtons();
//            showButtonMenuBack();
//            menuAbout = true;
//        }
//        setFragmentAnimation(getSupportFragmentManager(),
//                fragment,
//                menuContainerId,
//                false,
//                RIGHT_ANIMATION);
//    }
//
//    @Override
//    public void onDialogWorkoutSaveListener(UserTraining objects) {
//        plainTraining.add(objects);// = (ArrayList<Workout>) DAO.getUserPlains();
//        Backend.saveWorkoutObject(plainTraining);
//        Fragment fragment = getSupportFragmentManager().findFragmentById(menuContainerId);
//        if(fragment!=null && fragment instanceof MenuPlan){
//            ((MenuPlan)fragment).updateAllWorkout(plainTraining);
//        }
//        DialogSuccessful.show(this, null);
//    }
//
//    @Override
//    public ArrayList<UserTraining> updateUserTrainingList() {
//        return plainTraining;
//    }
//
//    @Override
//    public User onUserListener() {
//        return currentUser;
//    }
//
//    @Override
//    public void onClickListenerSave() {
//        if(menuSate == VIEW_MENU_TRACKING) {
//
//            _C_ = System.currentTimeMillis() + _M_;
//
//            DialogSuccessful.show(this, new DialogSuccessful.OnDisableListenerDialog() {
//                @Override
//                public void onSuccess() {
//                    DialogCheck.show(Main.this);
//                }
//            });
//        }
//        if(menuSate == VIEW_MENU_CHECK || menuAbout) {
//            DialogSuccessful.show(this, null);
//        }
//        closeMenuView();
//    }
//
//    @Override
//    public void onChangeInfoListener() {
//        Recalibrate.LOG(TAG, "Dialog Progress View");
//    }
//}
//
