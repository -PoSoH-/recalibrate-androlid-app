package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.MenuListAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;

/**
 * Created by Developer on 26.12.2016.
 */

public class MenuSettings extends Fragment {

    private CustomAdapterListener listener;
    private MenuListAdapter adapterSettingMenu;

    public static Fragment createFragment(){
        return new MenuSettings();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CustomAdapterListener)
            listener = (CustomAdapterListener) context;
        else
            throw new ClassCastException("Error implements interface - CustomAdapterListener...");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.f_main_include_settings_menu, container, false);
        String data[] = getResources().getStringArray(R.array.main_settings_list);
        List info = new LinkedList();
        Collections.addAll(info, data);
        adapterSettingMenu = new MenuListAdapter(getContext(), info);
        ListView settingsMenu = (ListView) view.findViewById(R.id.mainSettingsMenu);
        settingsMenu.setAdapter(adapterSettingMenu);
        settingsMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                assert listener != null;
                listener.onPositionSelected(i);
            }
        });
//        return inflater.inflate(R.layout.f_main_include_settings_menu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
