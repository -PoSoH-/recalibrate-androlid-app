package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.ShowToastMessageListener;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

/**
 * Created by Developer on 26.12.2016.
 */

public class MenuCheckIn extends Fragment {

    public static Fragment createFragment(){
        return new MenuCheckIn();
    }

    private OnClickButtonListener listener;
    private ShowToastMessageListener listenerShow;

    private EditText weight; // = (EditText) view.findViewById(R.id.introSettingsWeight);
    private EditText neck; // = (EditText) view.findViewById(R.id.introSettingsNeck);
    private EditText chest; // = (EditText) view.findViewById(R.id.introSettingsChest);
    private EditText waist; // = (EditText) view.findViewById(R.id.introSettingsWaist);
    private EditText hips; // = (EditText) view.findViewById(R.id.introSettingsHips);
    private EditText things; // = (EditText) view.findViewById(R.id.introSettingsThighs);

    private TextView btnSave;

    private final String TAG = MenuCheckIn.class.getSimpleName() + " : ";
    private List<UserCheckIn> checkIns = null;
    private UserCheckIn newCheckIn = new UserCheckIn();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnClickButtonListener)
            listener = (OnClickButtonListener) context;
        else
            throw new ClassCastException("No implements interface OnClickButtonListener");
        if(context instanceof ShowToastMessageListener)
            listenerShow = (ShowToastMessageListener) context;
        else
            throw new ClassCastException("No implements interface ShowToastMessageListener");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_include_check_in_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        weight = (EditText) view.findViewById(R.id.introSettingsWeight);
        neck = (EditText) view.findViewById(R.id.introSettingsNeck);
        chest = (EditText) view.findViewById(R.id.introSettingsChest);
        waist = (EditText) view.findViewById(R.id.introSettingsWaist);
        hips = (EditText) view.findViewById(R.id.introSettingsHips);
        things = (EditText) view.findViewById(R.id.introSettingsThighs);

        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {
            @Override
            public void onSuccess(List<UserCheckIn> result) {
                checkIns = result;
                updateDataInfo();
            }

            @Override
            public void onError(int error) {

            }
        });

        btnSave = (TextView) view.findViewById(R.id.introBntSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                assert listener != null;
                if(updateUserCheckIn()){
                    if(((checkIns.get(checkIns.size()-1).getCreated() + Recalibrate.KEY._WEEK_)/1000)
                            < (Recalibrate.Helper.createCorrectDate()/1000)){
                        checkIns.add(newCheckIn);
                        listener.onClickListenerSave(checkIns);
                    } else{
                        listener.onClickListenerSave(new LinkedList<UserCheckIn>());
                    }
                }
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void updateDataInfo(){
        int lenght = checkIns.size()-1;
        weight.setText(String.valueOf(checkIns.get(lenght).getWeight()));
        neck.setText(String.valueOf(checkIns.get(lenght).getNeck()));
        chest.setText(String.valueOf(checkIns.get(lenght).getChest()));
        waist.setText(String.valueOf(checkIns.get(lenght).getWaist()));
        hips.setText(String.valueOf(checkIns.get(lenght).getHips()));
        things.setText(String.valueOf(checkIns.get(lenght).getThighs()));
    }

    private boolean updateUserCheckIn(){
        if(!TextUtils.isEmpty(weight.getText())&&!TextUtils.equals(weight.getText(), ""))
            newCheckIn.setWeight(Float.valueOf(weight.getText().toString()));
        else return listenerShow.showMessage(Recalibrate.KEY.Error.WEIGHT);

        if(!TextUtils.isEmpty(neck.getText())&&!TextUtils.equals(neck.getText(), ""))
            newCheckIn.setNeck(Float.valueOf(neck.getText().toString()));
        else return listenerShow.showMessage(Recalibrate.KEY.Error.NECK);

        if(!TextUtils.isEmpty(chest.getText())&&!TextUtils.equals(chest.getText(), ""))
            newCheckIn.setChest(Float.valueOf(chest.getText().toString()));
        else return listenerShow.showMessage(Recalibrate.KEY.Error.CHEST);

        if(!TextUtils.isEmpty(waist.getText())&&!TextUtils.equals(waist.getText(), ""))
            newCheckIn.setWaist(Float.valueOf(waist.getText().toString()));
        else return listenerShow.showMessage(Recalibrate.KEY.Error.WAIST);

        if(!TextUtils.isEmpty(hips.getText())&&!TextUtils.equals(hips.getText(), ""))
            newCheckIn.setHips(Float.valueOf(hips.getText().toString()));
        else return listenerShow.showMessage(Recalibrate.KEY.Error.HIP);

        if(!TextUtils.isEmpty(things.getText())&&!TextUtils.equals(things.getText(), ""))
            newCheckIn.setThighs(Float.valueOf(things.getText().toString()));
        else return listenerShow.showMessage(Recalibrate.KEY.Error.THIGH);
        return true;
    }


}
