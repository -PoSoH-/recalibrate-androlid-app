package ua.i.tech.ploose.recalibrate.recalibrate.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ua.i.tech.ploose.recalibrate.recalibrate.fragments.intro.Check;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.intro.Review;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.intro.Track;

/**
 * Created by Developer on 27.12.2016.
 */

public class IntroPageAdapter extends FragmentStatePagerAdapter {

    public IntroPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return Check.createFragment();
            case 1: return Track.createFragment();
            case 2: return Review.createFragment();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
