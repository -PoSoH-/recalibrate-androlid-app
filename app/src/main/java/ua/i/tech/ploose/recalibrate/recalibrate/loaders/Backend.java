package ua.i.tech.ploose.recalibrate.recalibrate.loaders;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.models.ProgramInfo;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

/**
 * Created by Developer on 13.01.2017.
 */

public class Backend {

    public static final int LOAD_ERROR = -1001;

    private static DatabaseReference getDataBaseReference(final String childLoads){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        return database.getReference(childLoads);
    }

    public static String getUserId(){
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public static void loadUserTraining (final OnRecalibrateTrainingListener infoResult){
        getDataBaseReference(Recalibrate.KEY.USER_TRAINING).child(getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null){
                    List<UserTraining> userTrainings = new ArrayList<>();
                    for(DataSnapshot item : dataSnapshot.getChildren()) {
                        userTrainings.add(item.getValue(UserTraining.class));
                    }
                    infoResult.onSuccess(userTrainings);
                }else{
                    infoResult.onError(LOAD_ERROR);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                infoResult.onError(databaseError.getCode());
            }
        });
    }

    public static void loadUserData (final OnRecalibrateUserListener infoResult){
        getDataBaseReference(Recalibrate.KEY.USER_CURRENT).child(getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot != null){
                            User user = dataSnapshot.getValue(User.class);
                            infoResult.onSuccess(user);
                        }else{
                            infoResult.onError(LOAD_ERROR);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        infoResult.onError(databaseError.getCode());
                    }
                });
    }

    public static void loadUserCheckIn (final OnRecalibrateCheckInListener infoResult){
        getDataBaseReference(Recalibrate.KEY.USER_CHECK_IN).child(getUserId())
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null){
                    List<UserCheckIn> data = new LinkedList<>();
                    for(DataSnapshot item : dataSnapshot.getChildren()) {
                        data.add(item.getValue(UserCheckIn.class));
                    }
                    infoResult.onSuccess(data);
                }else{
                    infoResult.onError(LOAD_ERROR);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                infoResult.onError(databaseError.getCode());
            }
        });
    }

    public static void saveCurrentUser(User currentUser){
        getDataBaseReference(Recalibrate.KEY.USER_CURRENT)
                .child(getUserId())
                .setValue(currentUser);
    }

    public static void saveCheckInObject (List<UserCheckIn> userCheckIn
            , final OnRecalibrateSaveOnSuccess listener){
        getDataBaseReference(Recalibrate.KEY.USER_CHECK_IN)
                .child(getUserId())
                .setValue(userCheckIn)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isComplete()){
                    listener.onSuccess();
                }else{
                    listener.onError();
                }
            }
        });
    }

    public static void saveWorkoutObject (List<UserTraining> userTraining){
        getDataBaseReference(Recalibrate.KEY.USER_TRAINING)
                .child(getUserId())
                .setValue(userTraining);
    }

    public static void loadTermsValues (final OnRecalibrateProgramInfo programInfo){
        getDataBaseReference(Recalibrate.KEY.APP_INFO)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(dataSnapshot != null){
                            ProgramInfo temp = dataSnapshot.getValue(ProgramInfo.class) ;
                            programInfo.onSuccess(temp);
                        }else{
                            programInfo.onError(LOAD_ERROR);
                        }
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        programInfo.onError(databaseError.getCode());
                    }
                });
    }

    public static void loadFAQInstruction (final OnRecalibrateMapListener faq){
        getDataBaseReference(Recalibrate.KEY.APP_FAQ)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Map> result = new ArrayList<>();
                        if(dataSnapshot != null){
                            for(DataSnapshot item : dataSnapshot.getChildren()) {
                                HashMap<String, String> pos = (HashMap<String, String>) item.getValue();
                                result.add(pos);
                            }
                            faq.onSuccess(result);
                        }else{
                            faq.onError(LOAD_ERROR);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        faq.onError(databaseError.getCode());
                    }
                });
    }

    public static void loadProgramInfo(final OnRecalibrateProgramInfo response){
        getDataBaseReference(Recalibrate.KEY.APP_INFO)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot != null){
                    ProgramInfo info = dataSnapshot.getValue(ProgramInfo.class);
                    if(info!=null){
                        response.onSuccess(info);
                    }else{
                        response.onError(100);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Recalibrate.LOG("Backend", "Error data base "
                        + databaseError.getCode()
                        + " - "
                        + databaseError.getMessage());
            }
        });
    }

    public interface OnRecalibrateUserListener {
        void onSuccess (User result);
        void onError(int error);
    }
    public interface OnRecalibrateCheckInListener {
        void onSuccess (List<UserCheckIn> result);
        void onError(int error);
    }
    public interface OnRecalibrateTrainingListener {
        void onSuccess (List result);
        void onError(int error);
    }
    public interface OnRecalibrateSaveOnSuccess {
        void onSuccess();
        void onError();
    }
    public interface OnRecalibrateStringOnSuccess {
        void onSuccess(String information);
        void onError(String error);
    }
    public interface OnRecalibrateProgramInfo {
        void onSuccess(ProgramInfo information);
        void onError(int error);
    }
    public interface OnRecalibrateMapListener {
        void onSuccess(List<Map> FAQInformation);
        void onError(int error);
    }
}
