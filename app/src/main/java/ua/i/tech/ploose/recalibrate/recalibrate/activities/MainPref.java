package ua.i.tech.ploose.recalibrate.recalibrate.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogCheck;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogCheckInLimit;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogSuccessful;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuPlan;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuTracking;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.Support;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnDialogFragmentListener;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Calories;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Record;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

public class MainPref extends Base implements OnDialogFragmentListener
        , MenuPlan.OnUserTrainingListListener
        , Support.OnFAQUserListener
        , OnClickButtonListener {

    private final String TAG = MainPref.class.getSimpleName() + " : ";

    public static final int VIEW_MENU_GONE = 200;     // menu not loaded
    public static final int VIEW_MENU_CHECK = 210;    // menu check in data user
    public static final int VIEW_MENU_TRACKING = 220; // menu tracking record

    private int menuSate =  VIEW_MENU_GONE;

    private ImageView indicatorsEnable[];
    private FrameLayout buttons[];
    private ImageView btnAdd;
    private ImageView btnOk;
    private ImageView btnClosed;

    private int menuContainerId;
    private FrameLayout menuContainer;

    private User currentUser = null;
    private List<UserCheckIn> userCheckIn = new LinkedList<>();
    private List<UserTraining> plainTraining = new LinkedList<>();
    private LineChart charts[] = null;
    private TextView txtChartsActual[] = null;
    private TextView txtChartsTarget[] = null;

    private final String _CARB = "CARBOHYDRATES";
    private final String _PROT = "PROTEIN";
    private final String _FAT_ = "FAT";
    private final String _CALR = "CALORIES";
    private Map<String, Map> graphInfo = new HashMap<>();
/**
 * map is next type <String, float[]>
 *     String value next constant A - actual and T - target
 * */

    private long _S_ = 1000L;
    private long _M_ = _S_ * 60;
    private long _M_10 = _S_ * 60 * 10;
    private long _H_ = _M_ * 60;
    private long _D_ = _H_ * 24;
    private long _W_ = _D_ * 7;
    private long _C_ = 0L;

    public static void show(AppCompatActivity activity, final int rotate){
        Intent intent = new Intent(activity, MainPref.class);
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side
//                , R.anim.activity_start_exit_right_side);
        if(rotate == Recalibrate.KEY.ROTATE_L)
            activity.overridePendingTransition(R.anim.act_enter_l_rotate
                    , R.anim.act_enter_l_rotate);
        else
            activity.overridePendingTransition(R.anim.act_enter_r_rotate
                    , R.anim.act_enter_r_rotate);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main_pref);

        sampleGraphData();

        menuContainerId = R.id.mainMenuItemsViewContainer;
        menuContainer = (FrameLayout) findViewById(R.id.mainMenuItemsViewContainer);

        indicatorsEnable = new ImageView[]{
                (ImageView) findViewById(R.id.mainIndicatorCupEnable),
                (ImageView) findViewById(R.id.mainIndicatorGraphEnable),
                (ImageView) findViewById(R.id.mainIndicatorHumanEnable)
        };

        buttons = new FrameLayout[]{
                (FrameLayout) findViewById(R.id.mainButtonCup),
                (FrameLayout) findViewById(R.id.mainButtonGraph),
                (FrameLayout) findViewById(R.id.mainButtonHuman)
        };

        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainTable.show(MainPref.this, Recalibrate.KEY.ROTATE_R);
            }
        });
        buttons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        buttons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainProf.show(MainPref.this , Recalibrate.KEY.ROTATE_L);
            }
        });

        initializeImageIndicator();
        initToolbarButtons();
        loadInfoIsDataBase();
        showToolbarButton();

        charts = new LineChart[]{
                (LineChart) findViewById(R.id.mainPerformanceChart_fir),
                (LineChart) findViewById(R.id.mainPerformanceChart_sec),
                (LineChart) findViewById(R.id.mainPerformanceChart_thr),
                (LineChart) findViewById(R.id.mainPerformanceChart_fou)
        };
        txtChartsActual = new TextView[]{
                (TextView) findViewById(R.id.idProteinActual)
                ,(TextView) findViewById(R.id.idCarbohydratesActual)
                ,(TextView) findViewById(R.id.idFatActual)
                ,(TextView) findViewById(R.id.idCaloriesActual)
        };
        txtChartsTarget = new TextView[]{
                (TextView) findViewById(R.id.idProteinTarget)
                ,(TextView) findViewById(R.id.idCarbohydratesTarget)
                ,(TextView) findViewById(R.id.idFatTarget)
                ,(TextView) findViewById(R.id.idCaloriesTarget)
        };
    }

    @Override
    public void onBackPressed() {
        if (menuSate == VIEW_MENU_GONE) {
            MainTable.show(MainPref.this, Recalibrate.KEY.ROTATE_L);
        } else
            closeMenuView();
    }

    private void initializeImageIndicator(){
        addedImageResourceToView(indicatorsEnable[0],R.raw.btn_cup_black, 512,512);
        addedImageResourceToView(indicatorsEnable[1],R.raw.btn_graph_red, 512,512);
        addedImageResourceToView(indicatorsEnable[2],R.raw.btn_man_black, 512,512);
        deselectedButton();
    }

    private void hideAllToolbarButtons(){
        hideButtonMenuAdd();
        hideButtonMenuOk();
        hideButtonMenuSettings();
        hideButtonMenuBack();
        hideButtonMenuClosed();
    }

    private void showToolbarButton(){
        setLabel(R.string.main_performance);
        showButtonMenuOk();
        showButtonMenuAdd();
    }

    private void deselectedButton(){
        indicatorsEnable[0].setVisibility(View.VISIBLE);
        indicatorsEnable[1].setVisibility(View.VISIBLE);
        indicatorsEnable[2].setVisibility(View.VISIBLE);
    }

    private void initToolbarButtons(){
        btnClosed = getButtonMenuClosed();
        btnClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menuSate == VIEW_MENU_CHECK)
                    closeMenuView();
                else if(menuSate == VIEW_MENU_TRACKING)
                    closeMenuView();
            }
        });
        btnAdd = getButtonMenuAdd();
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTrackingView();
            }
        });
        btnOk = getButtonMenuOk();
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCheckInView();
            }
        });
    }

    private void showCheckInView(){
//        long current = System.currentTimeMillis();
////        Recalibrate.LOG(TAG, userCheckIn.get(userCheckIn.size()-1).getCreated() + " OR " + current);
////        Recalibrate.LOG(TAG, userCheckIn.get(userCheckIn.size()-1).getCreated() + _M_ + " OR " + current);
////        Recalibrate.LOG(TAG, userCheckIn.get(userCheckIn.size()-1).getCreated() + _H_ + " OR " + current);
//        Recalibrate.LOG(TAG, userCheckIn.get(userCheckIn.size()-1).getCreated() + _D_ + " OR " + current);
//        if((userCheckIn.get(userCheckIn.size()-1).getCreated() + _D_) > current){
//            DialogCheck.show(MainPref.this);
//            return;
//        }
        menuSate = VIEW_MENU_CHECK;
        hideAllToolbarButtons();
        showButtonMenuClosed();
        setFragmentAnimation(getSupportFragmentManager(),
                MenuCheckIn.createFragment(),
                menuContainerId,
                false,
                UP_ANIMATION);
        menuContainer.setVisibility(View.VISIBLE);
        setLabel(R.string.main_add_label_text);
    }
    private void showTrackingView(){
        if(System.currentTimeMillis() - _C_ < _M_) {
            DialogCheck.show(this);
        }else {
            menuSate = VIEW_MENU_TRACKING;
            hideAllToolbarButtons();
            showButtonMenuClosed();
            setFragmentAnimation(getSupportFragmentManager(),
                    MenuTracking.createFragment(),
                    menuContainerId,
                    false,
                    UP_ANIMATION);
            menuContainer.setVisibility(View.VISIBLE);
            setLabel(R.string.main_ok_label_text);
        }
    }

    private void setEmptyFragment(int typeAnimation){
        setFragmentAnimation(getSupportFragmentManager(),
                new Fragment(),
                menuContainerId,
                false,
                typeAnimation);
    }

    private void closeMenuView(){
//        if(menuSate == VIEW_MENU_SETTING){
//            if(menuSettings) {
//                setFragmentAnimation(getSupportFragmentManager(),
//                        MenuSettings.createFragment(),
//                        menuContainerId,
//                        false,
//                        LEFT_ANIMATION);
//                menuSettings = false;
//                setLabel(R.string.main_settings_label_text);
//                return;
//            }else{
//                setEmptyFragment(LEFT_ANIMATION);
//            }
//        }else{
//            if(menuAbout){
//                setEmptyFragment(LEFT_ANIMATION);
//                menuAbout = false;
//                setLabel(R.string.main_settings_menu_about_label);
//            }else {
//                setEmptyFragment(DOWN_ANIMATION);
//            }
//        }
        setEmptyFragment(DOWN_ANIMATION);
        menuSate = VIEW_MENU_GONE;
        hideAllToolbarButtons();
        showToolbarButton();
    }

    private void loadInfoIsDataBase(){
        Backend.loadUserData(new Backend.OnRecalibrateUserListener() {
            @Override
            public void onSuccess(User result) {
                currentUser = result;
            }

            @Override
            public void onError(int error) {
                String errorView = "";
                switch (error) {
                    case Backend.LOAD_ERROR:
                        errorView = "Load Error";
                        break;
                }
                Toast.makeText(getApplicationContext(), errorView, Toast.LENGTH_SHORT).show();
            }
        });

        Backend.loadUserTraining(new Backend.OnRecalibrateTrainingListener() {
            @Override
            public void onSuccess(List result) {
                plainTraining = (ArrayList<UserTraining>) result;
            }

            @Override
            public void onError(int error) {
                String errorView = "";
                switch (error) {
                    case Backend.LOAD_ERROR:
                        errorView = "Load Error";
                        break;
                }
                Toast.makeText(getApplicationContext(), errorView, Toast.LENGTH_SHORT).show();
            }
        });

        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {
            @Override
            public void onSuccess(List<UserCheckIn> result) {
                userCheckIn = result;
                sampleGraphData();
            }

            @Override
            public void onError(int error) {

            }
        });
    }

    @Override
    public void onDialogWorkoutSaveListener(UserTraining objects) {
        plainTraining.add(objects);// = (ArrayList<Workout>) DAO.getUserPlains();
        Backend.saveWorkoutObject(plainTraining);
        Fragment fragment = getSupportFragmentManager().findFragmentById(menuContainerId);
        if(fragment!=null && fragment instanceof MenuPlan){
            ((MenuPlan)fragment).updateAllWorkout(plainTraining);
        }
        DialogSuccessful.show(this, null);
    }

    @Override
    public List<UserTraining> updateUserTrainingList() {
        return plainTraining;
    }

    @Override
    public User onUserListener() {
        return currentUser;
    }

    @Override
    public void onClickListenerSave(UserCheckIn newCheckIn) {
//        if(menuSate == VIEW_MENU_CHECK) {
//            if((newCheckIn.getCreated() + Recalibrate.KEY._WEEK_) > Recalibrate.Helper.createCorrectDate()){
//                DialogCheckInLimit.show(MainPref.this);
//            }else{
//                DialogSuccessful.show(this, null);
//                userCheckIn.add(newCheckIn);
//                userCheckIn.get(userCheckIn.size()-1).setGoal(currentUser.getGoal());
//                saveCheckInCollection(userCheckIn,currentUser);
//            }
//        }
        closeMenuView();
    }

    @Override
    public void onClickListenerSave(final List<UserCheckIn> listCheckIn) {
        if(menuSate == VIEW_MENU_TRACKING) {
            Backend.saveCheckInObject(listCheckIn, new Backend.OnRecalibrateSaveOnSuccess() {
                @Override
                public void onSuccess() {
                    Recalibrate.LOG("TRAINING", "RECORD SAVE SUCCESS");
                    userCheckIn = listCheckIn;
                    sampleGraphData();
                }

                @Override
                public void onError() {
                    Recalibrate.LOG("TRAINING", "RECORD SAVE ERROR");
                }
            });
            DialogSuccessful.show(this, new DialogSuccessful.OnDisableListenerDialog() {
                @Override
                public void onSuccess() {
                    if(((listCheckIn.get(listCheckIn.size()-1).getCreated() + Recalibrate.KEY._WEEK_)/1000)
                            < (Recalibrate.Helper.createCorrectDate()/1000)) {
                        DialogCheck.show(MainPref.this);
                    }
                }
            });
        }
        if(menuSate == VIEW_MENU_CHECK) {
            if(listCheckIn.size() == 0){
                DialogCheckInLimit.show(MainPref.this);
            }else{
                DialogSuccessful.show(this, null);
                userCheckIn.get(userCheckIn.size()-1).setGoal(currentUser.getGoal());
                saveCheckInCollection(userCheckIn,currentUser);
            }
        }
        closeMenuView();
    }

    @Override
    public void onChangeInfoListener() {
        Recalibrate.LOG(TAG, "Dialog Progress View");
    }

//    Graph example data

    private final String A = "ACTUAL";
    private final String T = "TARGET";

    private void sampleGraphData(){
        if(userCheckIn.size() == 0) return;
        graphInfo.put(_CARB, selectedCARB());
        graphInfo.put(_PROT, selectedPROT());
        graphInfo.put(_FAT_, selectedFAT());
        graphInfo.put(_CALR, selectedCALORIES());
        buildGraph();
    }

    private void buildGraph(){

        setDataOnLineChart(charts[0]
                , txtChartsActual[0]
                , txtChartsTarget[0]
                ,"Protein"
                , listToArray((List<Float>) graphInfo.get(_PROT).get(T))
                , listToArray((List<Float>) graphInfo.get(_PROT).get(A)));

        setDataOnLineChart(charts[1]
                , txtChartsActual[1]
                , txtChartsTarget[1]
                ,"Carbs"
                , listToArray((List<Float>) graphInfo.get(_CARB).get(T))
                , listToArray((List<Float>) graphInfo.get(_CARB).get(A)));

        setDataOnLineChart(charts[2]
                , txtChartsActual[2]
                , txtChartsTarget[2]
                ,"Fat"
                , listToArray((List<Float>) graphInfo.get(_FAT_).get(T))
                , listToArray((List<Float>) graphInfo.get(_FAT_).get(A)));

        setDataOnLineChart(charts[3]
                , txtChartsActual[3]
                , txtChartsTarget[3]
                ,"Calories"
                , listToArray((List<Float>) graphInfo.get(_CALR).get(T))
                , listToArray((List<Float>) graphInfo.get(_CALR).get(A)));

//        setDataOnLineChart(charts[0],"Protein", generateMatrix(40), generateMatrix(40));
//        setDataOnLineChart(charts[1],"Carbs", generateMatrix(40), generateMatrix(40));
//        setDataOnLineChart(charts[2],"Fat", generateMatrix(40), generateMatrix(40));
//        setDataOnLineChart(charts[3],"Calories", generateMatrix(40), generateMatrix(40));
    }

    private float[] listToArray(List<Float> data){
        int length = data.size();
        float result[] = new float[length];
        for (int i=0;i<length;i++)
            result[i] = data.get(i);
        return result;
    }

    private Map<String, List<Float>> selectedCARB(){
        Map<String, List<Float>> carb = new HashMap<>();
        List<Float> resultA = new LinkedList<>();
        List<Float> resultT = new LinkedList<>();
        for(UserCheckIn itemValue : userCheckIn){
            List<Record> records = null;
            Map<String, Calories> calories = null;
            records = itemValue.getRecords();
            calories = itemValue.getCalories();
            for(Record item : records){
                // target data
                if((item.getCurrentDate()/1000)
                        < ((Recalibrate.Helper.createCorrectDate() + Recalibrate.KEY._DAY)/1000)) {
                    if (item.getTraining() == 1) {
                        /// it is training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_Y_TRAINING).getCarbohydrate());
                    } else if (item.getTraining() == -1) {
                        /// it is no training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_N_TRAINING).getCarbohydrate());
                    } else {
                        /// no data values
                        resultT.add(0.0F);
                    }
                    // actual data
                    if (item.getTraining() == 1 || item.getTraining() == -1) {
                        /// it is training or no training day actual value
                        resultA.add(item.getCarbohydrates());
                    } else {
                        /// no data values
                        resultA.add(0.0F);
                    }
                }
            }
        }
        resultA.toArray();
        carb.put(A, resultA);
        carb.put(T, resultT);
        return carb;
    }

    private Map<String, List<Float>> selectedPROT(){
        Map<String, List<Float>> protein = new HashMap<>();
        List<Float> resultA = new LinkedList<>();
        List<Float> resultT = new LinkedList<>();
        for(UserCheckIn itemValue : userCheckIn){
            List<Record> records = null;
            Map<String, Calories> calories = null;
            records = itemValue.getRecords();
            calories = itemValue.getCalories();
            for(Record item : records){
                if((item.getCurrentDate()/1000)
                        < ((Recalibrate.Helper.createCorrectDate() + Recalibrate.KEY._DAY)/1000)) {
                    // target data
                    if (item.getTraining() == 1) {
                        /// it is training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_Y_TRAINING).getProtein());
                    } else if (item.getTraining() == -1) {
                        /// it is no training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_N_TRAINING).getProtein());
                    } else {
                        /// no data values
                        resultT.add(0.0F);
                    }
                    // actual data
                    if (item.getTraining() == 1 || item.getTraining() == -1) {
                        /// it is training or no training day actual value
                        resultA.add(item.getProtein());
                    } else {
                        /// no data values
                        resultA.add(0.0F);
                    }
                }
            }
        }
        resultA.toArray();
        protein.put(A, resultA);
        protein.put(T, resultT);
        return protein;
    }

    private Map<String, List<Float>> selectedFAT(){
        Map<String, List<Float>> fat = new HashMap<>();
        List<Float> resultA = new LinkedList<>();
        List<Float> resultT = new LinkedList<>();
        for(UserCheckIn itemValue : userCheckIn){
            List<Record> records = null;
            Map<String, Calories> calories = null;
            records = itemValue.getRecords();
            calories = itemValue.getCalories();
            for(Record item : records){
                if((item.getCurrentDate()/1000)
                        < ((Recalibrate.Helper.createCorrectDate() + Recalibrate.KEY._DAY)/1000)) {
                    // target data
                    if (item.getTraining() == 1) {
                        /// it is training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_Y_TRAINING).getFat());
                    } else if (item.getTraining() == -1) {
                        /// it is no training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_N_TRAINING).getFat());
                    } else {
                        /// no data values
                        resultT.add(0.0F);
                    }
                    // actual data
                    if (item.getTraining() == 1 || item.getTraining() == -1) {
                        /// it is training or no training day actual value
                        resultA.add(item.getFat());
                    } else {
                        /// no data values
                        resultA.add(0.0F);
                    }
                }
            }
        }
        resultA.toArray();
        fat.put(A, resultA);
        fat.put(T, resultT);
        return fat;
    }

    private Map<String, List<Float>> selectedCALORIES(){
        Map<String, List<Float>> calorie = new HashMap<>();
        List<Float> resultA = new LinkedList<>();
        List<Float> resultT = new LinkedList<>();
        for(UserCheckIn itemValue : userCheckIn){
            List<Record> records = null;
            Map<String, Calories> calories = null;
            records = itemValue.getRecords();
            calories = itemValue.getCalories();
            for(Record item : records){
                if((item.getCurrentDate()/1000)
                        < ((Recalibrate.Helper.createCorrectDate() + Recalibrate.KEY._DAY)/1000)) {
                    // target data
                    if (item.getTraining() == 1) {
                        /// it is training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_Y_TRAINING).getCalories());
                    } else if (item.getTraining() == -1) {
                        /// it is no training day
                        resultT.add((float) calories.get(Recalibrate.Calc.KEY_N_TRAINING).getCalories());
                    } else {
                        /// no data values
                        resultT.add(0.0F);
                    }
                    // actual data
                    if (item.getTraining() == 1 || item.getTraining() == -1) {
                        /// it is training or no training day actual value
                        resultA.add(item.getProtein()*4 + item.getFat()*9 + item.getCarbohydrates()*4);
                    } else {
                        /// no data values
                        resultA.add(0.0F);
                    }
                }
            }
        }
        resultA.toArray();
        calorie.put(A, resultA);
        calorie.put(T, resultT);
        return calorie;
    }

    public void setDataOnLineChart(LineChart lineChart
            , TextView legendActual
            , TextView legendTarget
            , String labelLegend
            , float [] target
            , float [] actual){

        final List<Entry> entries = new ArrayList<Entry>();//y
        List<String> xAxes = new ArrayList<>();//x
        List<Entry> testEntriesPieces = new ArrayList<>();//y

        legendActual.setText("actual " + labelLegend);
        legendTarget.setText("target " + labelLegend);

        if(target.length < 1 || actual.length < 1)
            return;
        else if(target.length == 1 && actual.length == 1){

            if(target[0] == 0.0 || actual[0] == 0.0) return;

            for (int i=0; i<target.length;i++) {
                entries.add(new Entry(i, target[i]));//x, y
                testEntriesPieces.add(new Entry(i, actual[i]));
            }
            xAxes.add("");
            xAxes.add(0+""+(1)+getOrdinalFor(1));
            xAxes.add("");
        }else if(target.length > 1 && actual.length > 1){
            for (int i=0; i<target.length;i++) {
                entries.add(new Entry(i, target[i]));//x, y
                testEntriesPieces.add(new Entry(i,actual[i]));
                if(i<9){
                    xAxes.add(i, 0+""+(i+1)+getOrdinalFor(i+1));
                }
                else{
                    xAxes.add(i, (i+1)+getOrdinalFor(i+1));
                }
            }
        }else{
            return;
        }

        final String[] xaxes = new String[xAxes.size()];
        for(int i=0; i<xAxes.size();i++){
            xaxes[i] = xAxes.get(i).toString();
        }

        IAxisValueFormatter formatter = new IAxisValueFormatter() {//показуємо вісь х з рядковими величинами
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                Recalibrate.LOG("CHART", "index - " + value);
                if(entries.size() == 1) {
                    if (((int) value) == -1)
                        return xaxes[0];
                    if (((int) value) == 0)
                        return xaxes[1];
                    if (((int) value) == 1)
                        return xaxes[2];
                    else return "";
                }else {
                    return xaxes[(int) value];
                }
            }

        };

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);//вісь х (опис) знизу
        xAxis.setGranularity(1f); // minimum axis-step (interval) is 1

        xAxis.setValueFormatter(formatter);

        Legend legend = lineChart.getLegend();//тут граємось із легендами
        List<LegendEntry> legendEntries = new ArrayList<>();

        LegendEntry legendEntry1 = new LegendEntry("actual "
                + labelLegend.toLowerCase()
                + "   "
                , Legend.LegendForm.LINE,30f,1f,null,getResources().getColor(R.color.colorBlue));// синя лінія довжина і ширина

        LegendEntry legendEntry2 = new LegendEntry("target "
                + labelLegend.toLowerCase()
                , Legend.LegendForm.LINE,30f,1f,null,getResources().getColor(R.color.colorRed_B));//тут описуэмо одиницю легенди червона лныя

        legendEntries.add(legendEntry1);
        legendEntries.add(legendEntry2);
        legend.resetCustom();
        legend.setEnabled(false);

        ArrayList<ILineDataSet> lineDataSets = new ArrayList<>();

        if(entries.size()==1){
            LineDataSet dataSet = new LineDataSet(entries, ""); // add entries to dataset
            setPoint(dataSet, lineDataSets, entries,"#ed5353");
        }
        else if(entries.size()!=0){
            List<Entry> bufferEntryList = new ArrayList<>();
            LineDataSet dataSet = new LineDataSet(bufferEntryList, ""); // add entries to dataset
            boolean flag = false;
            for (int i = 1; i < entries.size(); i++) {//��� �������� ����� �� 1 �������� � �������!!!
                if (entries.get(i).getX()-entries.get(i-1).getX()==1){
                    bufferEntryList.add(entries.get(i-1));
                    bufferEntryList.add(entries.get(i));
                    flag = true;
                    if((entries.size()-1)==i){
                        setLine(dataSet,lineDataSets, bufferEntryList, "#ed5353");
                    }
                }
                else if(flag){

                    setLine( dataSet,lineDataSets,  bufferEntryList, "#ed5353");
                    flag = false;
                    bufferEntryList = new ArrayList<>();
                    if((entries.size()-1)==i){
                        bufferEntryList.add(entries.get(i));
                        setPoint(dataSet,lineDataSets, bufferEntryList,"#ed5353");
                        bufferEntryList = new ArrayList<>();
                    }

                }
                else {
                    if(i==(entries.size()-1)){
                        bufferEntryList.add(entries.get(i-1));
                        setPoint(dataSet, lineDataSets, bufferEntryList,"#ed5353");

                        bufferEntryList = new ArrayList<>();
                        bufferEntryList.add(entries.get(i));
                        setPoint(dataSet, lineDataSets, bufferEntryList,"#ed5353");
                        bufferEntryList = new ArrayList<>();
                    }
                    else {
                        bufferEntryList.add(entries.get(i-1));
                        setPoint(dataSet, lineDataSets, bufferEntryList,"#ed5353");
                        bufferEntryList = new ArrayList<>();
                    }

                }
            }
        }


        if(testEntriesPieces.size()==1){
            LineDataSet dataSet1 = new LineDataSet(testEntriesPieces, ""); // add entries to dataset
            setPoint(dataSet1, lineDataSets, testEntriesPieces, "#3844e2");
        }
        else if(testEntriesPieces.size()!=0){
            List<Entry> bufferEntryList = new ArrayList<>();
            LineDataSet dataSet1 = new LineDataSet(bufferEntryList, ""); // add entries to dataset
            boolean flag = false;
            for (int i = 1; i < testEntriesPieces.size(); i++) {//тут проблеми через цю 1 проблеми з точками!!!
                if (testEntriesPieces.get(i).getX()-testEntriesPieces.get(i-1).getX()==1){
                    bufferEntryList.add(testEntriesPieces.get(i-1));
                    bufferEntryList.add(testEntriesPieces.get(i));
                    flag = true;
                    if((testEntriesPieces.size()-1)==i){
                        setLine(dataSet1,lineDataSets, bufferEntryList, "#3844e2");
                    }
                }
                else if(flag){
                    setLine( dataSet1,lineDataSets,  bufferEntryList, "#3844e2");
                    flag = false;
                    bufferEntryList = new ArrayList<>();
                    if((testEntriesPieces.size()-1)==i){
                        bufferEntryList.add(testEntriesPieces.get(i));
                        setPoint(dataSet1,lineDataSets, bufferEntryList,"#3844e2");
                        bufferEntryList = new ArrayList<>();
                    }
                }
                else {
                    if(i==(testEntriesPieces.size()-1)){
                        bufferEntryList.add(testEntriesPieces.get(i-1));
                        setPoint(dataSet1, lineDataSets, bufferEntryList,"#3844e2");

                        bufferEntryList = new ArrayList<>();
                        bufferEntryList.add(testEntriesPieces.get(i));
                        setPoint(dataSet1, lineDataSets, bufferEntryList,"#3844e2");
                        bufferEntryList = new ArrayList<>();
                    }
                    else {
                        bufferEntryList.add(testEntriesPieces.get(i-1));
                        setPoint(dataSet1, lineDataSets, bufferEntryList,"#3844e2");
                        bufferEntryList = new ArrayList<>();
                    }

                }
            }
        }


        lineChart.setData(new LineData(lineDataSets));

        lineChart.getXAxis().setDrawGridLines(false);//ховає вертикальні лінії сітки
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setXOffset(1f);
        rightAxis.setAxisLineColor(Color.parseColor("#00000000"));
        rightAxis.setTextColor(Color.parseColor("#00000000"));
        // rightAxis.setEnabled(false);// ховає вісь У
        Description description = new Description();
        description.setText("");
        lineChart.setDescription(description);// ховає опис графіка
        lineChart.setScaleEnabled(false);//не зумиця
        lineChart.getAxisLeft().setDrawAxisLine(false);//ховає вісь У та що зліва

        lineChart.setVisibleXRangeMaximum(4.1f);

        lineChart.moveViewToX(target[target.length-1]);

        lineChart.invalidate();

    }

    private void setPoint( LineDataSet dataSet, ArrayList<ILineDataSet> lineDataSets, List<Entry> bufferEntryList, String color){
        dataSet = new LineDataSet(bufferEntryList, "");
        dataSet.setDrawCircles(true);// не малює кільця на графіку
        dataSet.setCircleColor(Color.parseColor(color));//колір кружка #3844e2
        dataSet.setCircleSize(2);//розмір кружка
        dataSet.setDrawValues(false);// не малює значення на графіку
        dataSet.setHighlightEnabled(false);// не малює перехресні лінії при натискуванні на графіку
        lineDataSets.add(dataSet);
    }
    private void setLine( LineDataSet dataSet, ArrayList<ILineDataSet> lineDataSets, List<Entry> bufferEntryList, String color){
        dataSet = new LineDataSet(bufferEntryList, "");
        dataSet.setColor(Color.parseColor(color));//"#3844e2"
        dataSet.setDrawCircles(false);// не малює кільця на графіку
        dataSet.setDrawValues(false);// не малює значення на графіку
        dataSet.setHighlightEnabled(false);
        lineDataSets.add(dataSet);
    }

    public static String getOrdinalFor(int value) {//генерує числові суфікси
        int hundredRemainder = value % 100;
        int tenRemainder = value % 10;
        if(hundredRemainder - tenRemainder == 10) {
            return "th";
        }

        switch (tenRemainder) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }

    private float [] generateMatrix(int amount){
        float [] genMatrix = new float[amount];
        for(int i=0; i< genMatrix.length; i++){
            genMatrix[i]=((int) (200*Math.random()));
        }

        return  genMatrix;
    }


}

