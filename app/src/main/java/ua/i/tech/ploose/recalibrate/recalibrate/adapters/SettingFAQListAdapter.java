package ua.i.tech.ploose.recalibrate.recalibrate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;

/**
 * Created by Developer on 03.01.2017.
 */

public class SettingFAQListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Map> faqInformation = new ArrayList<>();

    public SettingFAQListAdapter(Context context, ArrayList<Map> faqInformation) {
        this.context = context;
        this.faqInformation = faqInformation;
    }

    @Override
    public int getCount() {
        return this.faqInformation.size();
    }

    @Override
    public Object getItem(int i) {
        return this.faqInformation.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewFAQHolder itemHelper;
        String a = ((HashMap)getItem(position)).get(Recalibrate.KEY.FAQ_A).toString();
        String q = ((HashMap)getItem(position)).get(Recalibrate.KEY.FAQ_Q).toString();
        if(convertView == null){
            convertView = LayoutInflater.from(this.context)
                    .inflate(R.layout.item_faq_list_view, parent, false);
            itemHelper = new ViewFAQHolder((TextView) convertView.findViewById(R.id.itemFAQAnswerView),
                    (TextView) convertView.findViewById(R.id.itemFAQQuestionView));
            convertView.setTag(itemHelper);
        } else {
            itemHelper = (ViewFAQHolder) convertView.getTag();
        }
        itemHelper.text_A.setText(a);
        itemHelper.text_Q.setText(q);
        return convertView;
    }

    private static class ViewFAQHolder {
        private TextView text_A;
        private TextView text_Q;

        public ViewFAQHolder(TextView text_A, TextView text_Q){
            this.text_A = text_A;
            this.text_Q = text_Q;
        }
    }

    public void updateFAQList(List<Map> data){
        this.faqInformation = (ArrayList<Map>) data;
        notifyDataSetChanged();
    }
}
