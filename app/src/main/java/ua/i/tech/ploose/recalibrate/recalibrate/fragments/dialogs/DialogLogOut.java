package ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;

public class DialogLogOut extends DialogFragment {

    private OnClickLogOutListener listener;

    public static void show(FragmentActivity fragmentActivity){ //}, final String userID, final String urlPhoto, final String userName) {
        DialogLogOut dialog = new DialogLogOut();
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, Dialog.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnClickButtonListener)
            listener = (OnClickLogOutListener) context;
        else
            throw new ClassCastException("Error implements interface OnClickLogOutListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_log_out_view,
                null);

        view.findViewById(R.id.logOutBntNo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        view.findViewById(R.id.logOutBntYes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert listener != null;
                listener.onClickLogOutSuccess();
            }
        });

        Point point = new Point();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_full_rect_transparent);
        dialog.getWindow().getWindowManager().getDefaultDisplay().getSize(point);
        dialog.setContentView(view);
        RelativeLayout base = (RelativeLayout) view.findViewById(R.id.dialogStart);
        base.getLayoutParams().width = point.x;
        base.getLayoutParams().height = point.y;
        return dialog;
    }

    public interface OnClickLogOutListener{
        void onClickLogOutSuccess();
    }
}
