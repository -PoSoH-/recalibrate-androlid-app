package ua.i.tech.ploose.recalibrate.recalibrate.adapters.wheelAdapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.R;

public class UserLoginAdapter extends AbstractWheelTextAdapter {

    private int selectedItem = 0;
    private String[]info;
    private View itemView[];

    public UserLoginAdapter(Context context, String []currentFilter, int selectedPosition ) {
        super(context, R.layout.item_settings_view, NO_RESOURCE);

        this.itemView = new View[currentFilter.length];
        this.info = currentFilter;
        this.selectedItem = selectedPosition;
//        setSelectedItem(i);
        getItemView(this.selectedItem);
        setItemTextResource(R.id.menuSettingViewValue);
    }

    @Override
    public View getItem(int index, View convertView, ViewGroup parent) {
        View view = super.getItem(index, convertView, parent);
        TextView timeWheelItem = (TextView) view.findViewById(R.id.menuSettingViewValue);
        timeWheelItem.setText(info[index].toString());
        if (index == selectedItem){
            timeWheelItem.setTextColor(context.getResources().getColor(R.color.colorRed_B));
            timeWheelItem.setTextSize(context.getResources().getDimension(R.dimen.intro_text_size_in_focus));
        } else {
            timeWheelItem.setTextColor(context.getResources().getColor(R.color.colorGray_138));
            timeWheelItem.setTextSize(context.getResources().getDimension(R.dimen.intro_text_size_out_focus));
        }
        itemView[index] = view;
        return view;
    }

    @Override
    protected CharSequence getItemText(int index) {
        return info[index];
    }

    @Override
    public int getItemsCount() {
        return this.info.length;
    }

    @Override
    public View getItemView(int index){
        return itemView[index];
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }
    public int getSelectedItem() {return this.selectedItem;}
}
