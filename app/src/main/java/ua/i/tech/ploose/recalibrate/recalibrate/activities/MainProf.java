package ua.i.tech.ploose.recalibrate.recalibrate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.MenuListAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogCheckInLimit;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogLogOut;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs.DialogSuccessful;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuGoal;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuPlan;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu.MenuSettings;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.About;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.CountMacros;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.FAQ;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.MealPlans;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.Support;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings.Terms;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnDialogFragmentListener;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

public class MainProf extends Base implements CustomAdapterListener,
        OnDialogFragmentListener
        , MenuPlan.OnUserTrainingListListener
        , Support.OnFAQUserListener
        , OnClickButtonListener
        , DialogLogOut.OnClickLogOutListener {

    private final String TAG = MainProf.class.getSimpleName() + " : ";

    public static final int VIEW_MENU_GONE = 200;     // menu not loaded
    public static final int VIEW_MENU_SETTING = 230;  // menu settings

    public static final int VIEW_MENU_SETTING_A = 0;//231;
    public static final int VIEW_MENU_SETTING_H = 1;//232;
    public static final int VIEW_MENU_SETTING_P = 2;//233;
    public static final int VIEW_MENU_SETTING_S = 3;//234;
    public static final int VIEW_MENU_SETTING_F = 4;//235;
    public static final int VIEW_MENU_SETTING_C = 5;//236;

    public static final int VIEW_MENU_ABOUT_P = 0;//231;
    public static final int VIEW_MENU_ABOUT_G = 1;//232;
    public static final int VIEW_MENU_ABOUT_T = 2;//233;
    public static final int VIEW_MENU_ABOUT_L = 3;//234;

    private int menuSate =  VIEW_MENU_GONE;
    private boolean menuSettings = false;
    private boolean menuAbout = false;

    private ImageView indicatorsEnable[];

    private int menuContainerId;
    private FrameLayout menuContainer;

    private User currentUser = null;
    private List<UserCheckIn> userCheckIn = null;
    private List<UserTraining> plainTraining = new LinkedList<>();

    public static void show(AppCompatActivity activity, final int rotate){
        Intent intent = new Intent(activity, MainProf.class);
        activity.startActivity(intent);
//        activity.overridePendingTransition(R.anim.activity_start_enter_right_side
//                , R.anim.activity_start_exit_right_side);
        if(rotate == Recalibrate.KEY.ROTATE_L)
            activity.overridePendingTransition(R.anim.act_enter_l_rotate
                    , R.anim.act_enter_l_rotate);
        else
            activity.overridePendingTransition(R.anim.act_enter_r_rotate
                    , R.anim.act_enter_r_rotate);
        activity.finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_main_prof);

        String data[] = getResources().getStringArray(R.array.main_profile_list);
        List info = new LinkedList();
        Collections.addAll(info, data);
        ListView myProfile = (ListView) findViewById(R.id.mainProfileView);
//        ArrayAdapter<String> profileAdapter = new ArrayAdapter<String>(getContext(),
//                R.layout.item_profile_view,
//                info);

        MenuListAdapter menuAdapter = new MenuListAdapter(this, info);
        myProfile.setAdapter(menuAdapter);
        myProfile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onPositionSelected(i);
            }
        });

        menuContainerId = R.id.mainMenuItemsViewContainer;
        menuContainer = (FrameLayout) findViewById(R.id.mainMenuItemsViewContainer);

        indicatorsEnable = new ImageView[]{
                (ImageView) findViewById(R.id.mainIndicatorCupEnable),
                (ImageView) findViewById(R.id.mainIndicatorGraphEnable),
                (ImageView) findViewById(R.id.mainIndicatorHumanEnable)
        };

        FrameLayout[] buttons = new FrameLayout[]{
                (FrameLayout) findViewById(R.id.mainButtonCup),
                (FrameLayout) findViewById(R.id.mainButtonGraph),
                (FrameLayout) findViewById(R.id.mainButtonHuman)
        };

        buttons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainTable.show(MainProf.this, Recalibrate.KEY.ROTATE_R);
            }
        });
        buttons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainPref.show(MainProf.this, Recalibrate.KEY.ROTATE_R);
            }
        });
        buttons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        initializeImageIndicator();
        initToolbarButtons();
        loadInfoIsDataBase();
        showToolbarButton();
    }

    @Override
    public void onBackPressed() {
        if(menuSate == VIEW_MENU_SETTING || menuAbout || menuSettings){
            closeMenuView();
        }else {
//            super.onBackPressed();
            MainTable.show(MainProf.this, Recalibrate.KEY.ROTATE_L);
        }
    }

    private void initializeImageIndicator(){
        addedImageResourceToView(indicatorsEnable[0],R.raw.btn_cup_black, 512,512);
        addedImageResourceToView(indicatorsEnable[1],R.raw.btn_graph_black, 512,512);
        addedImageResourceToView(indicatorsEnable[2],R.raw.btn_man_red, 512,512);

        deselectedButton();
        selectedButton(0);
    }

    private void hideAllToolbarButtons(){
        hideButtonMenuAdd();
        hideButtonMenuOk();
        hideButtonMenuSettings();
        hideButtonMenuBack();
        hideButtonMenuClosed();
    }

    private void showToolbarButton(){
        setLabel(R.string.main_profile);
        showButtonMenuSettings();
    }

    private void deselectedButton(){
        indicatorsEnable[0].setVisibility(View.VISIBLE);
        indicatorsEnable[1].setVisibility(View.VISIBLE);
        indicatorsEnable[2].setVisibility(View.VISIBLE);
    }

    private void selectedButton(final int position){
        indicatorsEnable[position].setVisibility(View.VISIBLE);
    }

    private void initToolbarButtons(){
        ImageView btnBack = getButtonMenuBack();
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeMenuView();
            }
        });
        ImageView btnSettings = getButtonMenuSettings();
        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showSettingsView();
            }
        });
    }

    private void showSettingsView(){
        menuSate = VIEW_MENU_SETTING;
        hideAllToolbarButtons();
        showButtonMenuBack();
        setFragmentAnimation(getSupportFragmentManager(),
                MenuSettings.createFragment(),
                menuContainerId,
                false,
                RIGHT_ANIMATION);
        menuContainer.setVisibility(View.VISIBLE);
        setLabel(R.string.main_settings_label_text);
    }

    private void closeMenuView(){
        if(menuSate == VIEW_MENU_SETTING){
            if(menuSettings) {
                setFragmentAnimation(getSupportFragmentManager(),
                        MenuSettings.createFragment(),
                        menuContainerId,
                        false,
                        LEFT_ANIMATION);
                menuSettings = false;
                setLabel(R.string.main_settings_label_text);
                return;
            }else{
                setEmptyFragment(LEFT_ANIMATION);
            }
        }else{
            if(menuAbout){
                setEmptyFragment(LEFT_ANIMATION);
                menuAbout = false;
                setLabel(R.string.main_settings_menu_about_label);
            }
        }
        menuSate = VIEW_MENU_GONE;
        hideAllToolbarButtons();
        showToolbarButton();
    }

    private void setEmptyFragment(int typeAnimation){
        setFragmentAnimation(getSupportFragmentManager(),
                new Fragment(),
                menuContainerId,
                false,
                typeAnimation);
    }

    private void loadInfoIsDataBase(){
        Backend.loadUserData(new Backend.OnRecalibrateUserListener() {
            @Override
            public void onSuccess(User result) {
                currentUser = result;
            }

            @Override
            public void onError(int error) {
                String errorView = "";
                switch (error) {
                    case Backend.LOAD_ERROR:
                        errorView = "Load Error";
                        break;
                }
                Toast.makeText(getApplicationContext(), errorView, Toast.LENGTH_SHORT).show();
            }
        });

        Backend.loadUserTraining(new Backend.OnRecalibrateTrainingListener() {
            @Override
            public void onSuccess(List result) {
                plainTraining = (ArrayList) result;
            }

            @Override
            public void onError(int error) {
                String errorView = "";
                switch (error) {
                    case Backend.LOAD_ERROR:
                        errorView = "Load Error";
                        break;
                }
                Toast.makeText(getApplicationContext(), errorView, Toast.LENGTH_SHORT).show();
            }
        });

        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {

            @Override
            public void onSuccess(List<UserCheckIn> result) {
                userCheckIn = result;
            }

            @Override
            public void onError(int error) {

            }
        });
    }

    @Override
    public void onPositionSelected(int position) {
        Fragment fragment = new Fragment();
        if(menuSate == VIEW_MENU_SETTING){
            Recalibrate.LOG(TAG, "Menu Settings position " + position);
            switch (position){
                case VIEW_MENU_SETTING_A:
                    setLabel(R.string.main_settings_menu_about_label);
                    fragment = About.createFragment();
                    break;
                case VIEW_MENU_SETTING_H:
                    setLabel(R.string.main_settings_menu_macros_label);
                    fragment = CountMacros.createFragment();
//                    fragment = About.createFragment();
                    break;
                case VIEW_MENU_SETTING_P:
                    setLabel(R.string.main_settings_menu_plans_label);
                    fragment = MealPlans.createFragment();
//                    fragment = About.createFragment();
                    break;
                case VIEW_MENU_SETTING_S:
                    setLabel(R.string.main_settings_menu_support_label);
                    fragment = Support.createFragment();
                    break;
                case VIEW_MENU_SETTING_F:
                    setLabel(R.string.main_settings_menu_faq_label);
                    fragment = FAQ.createFragment();
                    break;
                case VIEW_MENU_SETTING_C:
                    setLabel(R.string.main_settings_menu_terms_label);
                    fragment = Terms.createFragment();
                    break;
            }
            menuSettings = true;
        }else {
            Recalibrate.LOG(TAG, "Menu profile position " + position);
            switch (position) {
                case VIEW_MENU_ABOUT_P:
                    fragment = MenuCheckIn.createFragment();
                    setLabel(R.string.main_about_menu_check_label);
                    break;
                case VIEW_MENU_ABOUT_G:
                    fragment = MenuGoal.createFragment();
                    setLabel(R.string.main_about_menu_goals_label);
                    break;
                case VIEW_MENU_ABOUT_T:
                    fragment = MenuPlan.createFragment();
                    setLabel(R.string.main_about_menu_plan_label);
                    break;
                case VIEW_MENU_ABOUT_L:
                    DialogLogOut.show(MainProf.this);
                    break;
            }
            menuAbout = true;
        }
            hideAllToolbarButtons();
            showButtonMenuBack();
        setFragmentAnimation(getSupportFragmentManager(),
                fragment,
                menuContainerId,
                false,
                RIGHT_ANIMATION);
    }

    @Override
    public void onDialogWorkoutSaveListener(UserTraining objects) {
        plainTraining.add(objects);// = (ArrayList<Workout>) DAO.getUserPlains();
        Backend.saveWorkoutObject(plainTraining);
        Fragment fragment = getSupportFragmentManager().findFragmentById(menuContainerId);
        if(fragment!=null && fragment instanceof MenuPlan){
            ((MenuPlan)fragment).updateAllWorkout(plainTraining);
        }
        DialogSuccessful.show(this, null);
    }

    @Override
    public List<UserTraining> updateUserTrainingList() {
        return plainTraining;
    }

    @Override
    public User onUserListener() {
        return currentUser;
    }

    @Override
    public void onClickListenerSave(UserCheckIn newCheckIn) {
//        saveCheckInCollection(userCheckIn, newCheckIn, currentUser);
//        DialogSuccessful.show(this, null);
//        closeMenuView();
    }

    @Override
    public void onClickListenerSave(final List<UserCheckIn> listCheckIn) {
        if(listCheckIn.size() == 0){
            DialogCheckInLimit.show(this);
        }else {
            DialogSuccessful.show(this, null);
            userCheckIn.get(userCheckIn.size()-1).setGoal(currentUser.getGoal());
            saveCheckInCollection(listCheckIn, currentUser);
        }
        closeMenuView();
    }

    @Override
    public void onChangeInfoListener() {
        Recalibrate.LOG(TAG, "Dialog Progress View");
    }

    @Override
    public void onClickLogOutSuccess() {
        FirebaseAuth.getInstance().signOut();
        Intro.show(MainProf.this, null);
    }
}

