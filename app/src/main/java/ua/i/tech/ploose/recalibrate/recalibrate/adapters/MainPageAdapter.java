package ua.i.tech.ploose.recalibrate.recalibrate.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ua.i.tech.ploose.recalibrate.recalibrate.fragments.main.DataTable;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.main.MyProfile;
import ua.i.tech.ploose.recalibrate.recalibrate.fragments.main.Performance;

/**
 * Created by Developer on 27.12.2016.
 */

public class MainPageAdapter extends FragmentStatePagerAdapter {

    public MainPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return DataTable.createFragment();
            case 1: return Performance.createFragment();
            case 2: return MyProfile.createFragment();
            default: return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
}
