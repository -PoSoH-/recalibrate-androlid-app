package ua.i.tech.ploose.recalibrate.recalibrate.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.ProgramInfo;
import ua.i.tech.ploose.recalibrate.recalibrate.models.User;

public class Splash extends AppCompatActivity {

    private final static String TAG = Splash.class.getSimpleName() + " : ";

    private ImageView logo;
    private TextView textLogo;
    private ProgressBar bar;
    private AsyncTask<Void, Integer, Void> progress;
    private ProgramInfo info = null;
    private User currentUser = null;
    private FirebaseUser user = null;

    private boolean isUserLoaded = false;
    private boolean isCurrentLoaded = false;
    private boolean isInfoLoaded = false;
    private boolean isAnimationOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_splash);

        logo = (ImageView) findViewById(R.id.splashImage);
        textLogo = (TextView) findViewById(R.id.splashText);
        bar = (ProgressBar) findViewById(R.id.splashProgress);
        bar.setMax(100);
        user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null){
            isUserLoaded = true;
            Backend.loadUserData(new Backend.OnRecalibrateUserListener() {
                @Override
                public void onSuccess(User result) {
                    if(result!=null) {
                        currentUser = result;
                        isCurrentLoaded = true;
//                        Recalibrate.Preferences.setStringPreferences(Splash.this,
//                                getString(R.string.intro_view_reg_status_process),
//                                Recalibrate.Preferences.);
                        switchNextScreen();
                    }
                }

                @Override
                public void onError(int error) {
                    isCurrentLoaded = false;
                }
            });
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

        progress = new AsyncTask<Void, Integer, Void>() {

            Integer progress[] = new Integer[] {0};

            @Override
            protected Void doInBackground(Void... voids) {

                boolean isProgress = true;
                long oldMiliSecond = System.currentTimeMillis();
                long newMiliSecond = 0;

                while (isProgress){
                    newMiliSecond = System.currentTimeMillis();
                    if((newMiliSecond - oldMiliSecond) >= 5){
                        oldMiliSecond = newMiliSecond;
                        progress[0] += 1;
                        publishProgress(progress);
                        if(progress[0] == 100){
                            isProgress = false;
                        }
                    }
                }

                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                startProgress(progress);
                super.onProgressUpdate(values);
            }

        }.execute();

        Backend.loadProgramInfo(new Backend.OnRecalibrateProgramInfo() {
            @Override
            public void onSuccess(ProgramInfo information) {
                info = information;
                isInfoLoaded = true;
                switchNextScreen();
            }

            @Override
            public void onError(int error) {
                isInfoLoaded = false;
                switchNextScreen();
            }
        });

//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference dataRef = database.getReference(Recalibrate.KEY.APP_INFO);
//        dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                if(dataSnapshot != null){
//                    info = dataSnapshot.getValue(ProgramInfo.class);
//                    if(info!=null){
//
////                        if(user == null) {
////                            Intro.show(Splash.this, info);
////                            Recalibrate.LOG(TAG, info.getTrackProgress());
////                        }else{
//////                            Main.show(Splash.this);
////                            MainTable.show(Splash.this, Recalibrate.KEY.ROTATE_L);
////                        }
//                    }else{
//                        isInfoLoaded = false;
//                        switchNextScreen();
////                        if(user != null) {
//////                            Main.show(Splash.this);
////                            MainTable.show(Splash.this, Recalibrate.KEY.ROTATE_L);
////                        }
//                    }
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Recalibrate.LOG(TAG, "Error data base "
//                        + databaseError.getCode()
//                        + " - "
//                        + databaseError.getMessage());
//            }
//        });
    }

    private void startProgress(Integer[] progress){
        bar.setProgress( progress[0]);
        if(bar.getProgress() == 100){
            startNextStep();
        }
    }

    private void startNextStep(){

        final Animation animLogo = AnimationUtils.loadAnimation(getApplicationContext()
                , R.anim.rotate_left_animation);

        Animation animProgress = AnimationUtils.loadAnimation(getApplicationContext()
                , R.anim._hide_object_animation);
        bar.startAnimation(animProgress);
        bar.setVisibility(View.INVISIBLE);

        Animation animContainer = AnimationUtils.loadAnimation(getApplicationContext()
                , R.anim._show_object_animation);
        textLogo.startAnimation(animContainer);
        textLogo.setVisibility(View.VISIBLE);

        animContainer.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                logo.startAnimation(animLogo);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        animLogo.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isAnimationOk = true;
                switchNextScreen();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    private void switchNextScreen(){
        if(isAnimationOk) {
            if(isInfoLoaded) {
                if (isUserLoaded) {
                    if (isCurrentLoaded) {
                        if (TextUtils.equals(currentUser.getStatus(), getString(R.string.intro_view_reg_status_complete))) {
                            MainTable.show(Splash.this, Recalibrate.KEY.ROTATE_L);
                        } else {
                            Intro.show(Splash.this, info);
                        }
                    }
                }else{
                    Intro.show(Splash.this, info);
                }
            }
        }
    }

}
