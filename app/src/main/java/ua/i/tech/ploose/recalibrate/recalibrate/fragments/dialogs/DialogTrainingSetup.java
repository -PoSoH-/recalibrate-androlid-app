package ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.wheelAdapters.UserTrainingAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnDialogFragmentListener;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;

public class DialogTrainingSetup extends DialogFragment {

    private String dataDuration[];
    private String dataIntensity[];
    private String dataFrequency[];
    private int selectDuration = 0;
    private int selectIntensity = 0;
    private int selectFrequency = 0;
    private OnDialogFragmentListener dialogFragmentListener;
    private UserTraining workout = new UserTraining();

    public static void show(FragmentActivity fragmentActivity){ //}, final String userID, final String urlPhoto, final String userName) {
        DialogTrainingSetup dialog = new DialogTrainingSetup();
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, Dialog.class.getSimpleName());
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnDialogFragmentListener)
            dialogFragmentListener = (OnDialogFragmentListener) context;
        else
            throw new ClassCastException(getString(R.string.error_implements_interface)
                    + " OnDialogFragmentListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        listener = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater()
                .inflate(R.layout.dialog_training_setup_view, null);

        WheelView wheelDuration = (WheelView) view.findViewById(R.id.introWheelDuration);
        WheelView wheelIntensity = (WheelView) view.findViewById(R.id.introWheelIntensity);
        WheelView wheelFrequency = (WheelView) view.findViewById(R.id.introWheelFrequency);

        final int maxMin = 120;
        final int stepMin = 5;
        int step = 5;
        int position = 1;
        int count = maxMin / stepMin + 1;
        dataDuration = new String[count];

        while (step <= maxMin){
            dataDuration[position] = String.valueOf(step);
            step += stepMin;
            position ++;
        }

        dataDuration[0] = getString(R.string.intro_settings_workout_duration_list);
        dataIntensity = getResources().getStringArray(R.array.intro_settings_collection_intensity);
        dataFrequency = getResources().getStringArray(R.array.intro_settings_collection_frequency);

        createDuration(getContext(), wheelDuration, dataDuration);
        createIntensive(getContext(), wheelIntensity, dataIntensity);
        createFrequency(getContext(), wheelFrequency, dataFrequency);

        TextView save = (TextView) view.findViewById(R.id.introBntWorkoutSave);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(changeWorkout()) {
                    dialogDismiss();
                }
            }
        });

        Point point = new Point();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_full_rect_transparent);
        dialog.getWindow().getWindowManager().getDefaultDisplay().getSize(point);
        dialog.setContentView(view);
        RelativeLayout base = (RelativeLayout) view.findViewById(R.id.dialogSettingsWorkout);
        base.getLayoutParams().width = point.x;
        base.getLayoutParams().height = point.y;
        return dialog;
    }

    private void createDuration(Context context, WheelView duration, String []data){

        duration.setWheelViewBackgroundResource(android.R.color.transparent);
        duration.disableShadows();
        duration.setColorSeparatorLine(R.color.colorTransparent);
        duration.setVisibleItems(3);
        final UserTrainingAdapter durationAdapter = new UserTrainingAdapter(context,
                data,
                selectDuration);
        duration.setViewAdapter(durationAdapter);
        duration.setCurrentItem(durationAdapter.getSelectedItem());
        durationAdapter.notifyDataChangedEvent();
        duration.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorGray_212));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_in_focus));
                    selectDuration = newValue;
                    durationAdapter.setSelectedItem(newValue);

                } catch (NullPointerException e) {
                    Log.e("GOAL", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void createIntensive(Context context, WheelView intensive, String []data){

        intensive.setWheelViewBackgroundResource(android.R.color.transparent);
        intensive.disableShadows();
        intensive.setColorSeparatorLine(R.color.colorTransparent);
        intensive.setVisibleItems(3);
        final UserTrainingAdapter intensiveAdapter = new UserTrainingAdapter(context,
                data,
                selectIntensity);
        intensive.setViewAdapter(intensiveAdapter);
        intensive.setCurrentItem(intensiveAdapter.getSelectedItem());
        intensiveAdapter.notifyDataChangedEvent();
        intensive.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorGray_212));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_in_focus));
                    selectIntensity = newValue;
                    intensiveAdapter.setSelectedItem(newValue);

                } catch (NullPointerException e) {
                    Log.e("GOAL", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private void createFrequency(Context context, WheelView frequency, String []data){

        frequency.setWheelViewBackgroundResource(android.R.color.transparent);
        frequency.disableShadows();
        frequency.setColorSeparatorLine(R.color.colorTransparent);
        frequency.setVisibleItems(3);
        final UserTrainingAdapter frequencyAdapter = new UserTrainingAdapter(context,
                data,
                selectFrequency);
        frequency.setViewAdapter(frequencyAdapter);
        frequency.setCurrentItem(frequencyAdapter.getSelectedItem());
        frequencyAdapter.notifyDataChangedEvent();
        frequency.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorGray_212));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources().getDimension(R.dimen.intro_text_size_in_focus));
                    selectFrequency = newValue;
                    frequencyAdapter.setSelectedItem(newValue);
                } catch (NullPointerException e) {
                    Log.e("GOAL", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }


    private boolean changeWorkout(){

        if(selectDuration > 0){
            workout.setDuration(dataDuration[selectDuration]);
        }else{
            Toast.makeText(getContext(),
                    getString(R.string.intro_error_duration),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(selectIntensity > 0){
            workout.setIntensity(dataIntensity[selectIntensity]);
        }else{
            Toast.makeText(getContext(),
                    getString(R.string.intro_error_intensity),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(selectFrequency > 0){
            workout.setFrequency(dataFrequency[selectFrequency]);
        }else{
            Toast.makeText(getContext(),
                    getString(R.string.intro_error_frequency),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void dialogDismiss(){
        assert dialogFragmentListener != null;
        dialogFragmentListener.onDialogWorkoutSaveListener(workout);
        this.dismiss();
    }

}
