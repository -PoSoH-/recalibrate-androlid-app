package ua.i.tech.ploose.recalibrate.recalibrate.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.exeption.MinusValueException;

/**
 * Created by Developer on 12.01.2017.
 */
@IgnoreExtraProperties
public class User {

    private String id = "";
    private String currentUserId = "";
    private String firstName = "";
    private String lastName = "";
    private String gender = "";
    private String goal = "";
    private String level = "";
    private float height = 0.0F;
    private int age = 0;
    private String status = ""; //process  //registered  //blocked
    private long created= 0L;
    private float totalActiveScore = 0.0F;
    private int totalBMR = 0;
    private float totalDefSur = 0.0F;

    public User(){}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(String currentUserId) {
        this.currentUserId = currentUserId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        try {
            if(height>0)
                this.height = height;
            else
                throw new MinusValueException("Height not minus value");
        } catch (MinusValueException e) {
            e.printStackTrace();
        }
    }

    public float getAge() {
        return this.age;
    }

    public void setAge(int age) {
        try {
            if(age>0)
                this.age = age;
            else
                throw new MinusValueException("Height not minus value");
        } catch (MinusValueException e) {
            e.printStackTrace();
        }
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public float getTotalActiveScore() {
        return totalActiveScore;
    }

    public void setTotalActiveScore(float totalActiveScore) {
        this.totalActiveScore = totalActiveScore;
    }

    public int getTotalBMR() {
        return totalBMR;
    }

    public void setTotalBMR(int totalBMR) {
        this.totalBMR = totalBMR;
    }

    public float getTotalDefSur() {
        return totalDefSur;
    }

    public void setTotalDefSur(float totalDefSur) {
        this.totalDefSur = totalDefSur;
    }

    public void printLogValueField(){
        Recalibrate.LOG("USER CLASS : "
                , "01:id:" +  getId()
                        + "\n02:first   :" +  getFirstName()
                        + "\n03:last    :" +  getLastName()
                        + "\n04:Age     :" +  getAge()
                        + "\n05:height  :" +  getHeight()
                        + "\n06:gender  :" +  getGender()
                        + "\n07:goal    :" +  getGoal()
                        + "\n08:level   :" +  getLevel()
                        + "\n09:score   :" +  getTotalActiveScore()
                        + "\n10:BMR     :" +  getTotalBMR()
                        + "\n11:DEF/SUR :" +  getTotalDefSur()
        );
    }
}
