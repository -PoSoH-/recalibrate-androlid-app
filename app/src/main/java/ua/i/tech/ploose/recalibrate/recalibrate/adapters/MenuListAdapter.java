package ua.i.tech.ploose.recalibrate.recalibrate.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.activities.Base;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;

/**
 * Created by Developer on 03.01.2017.
 */

public class MenuListAdapter extends ArrayAdapter<String> {

//    private CustomAdapterListener listener;

    public MenuListAdapter(Context context, List<String> objects) {
        super(context, 0, objects);
//        if(context instanceof CustomAdapterListener) listener = (CustomAdapterListener) context;
//        else throw new ClassCastException("ERROR");
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewItemHelper itemHelper = null;
        String data = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_profile_view, parent, false);
            itemHelper = new ViewItemHelper((TextView) convertView.findViewById(R.id.itemTextView),
                    (ImageView) convertView.findViewById(R.id.itemImageView));
            convertView.setTag(itemHelper);
        } else {
            itemHelper = (ViewItemHelper) convertView.getTag();
        }

        itemHelper.textView.setText(data);
        itemHelper.addIconView(getContext());
//        convertView.findViewById(R.id.itemListView)
//                .setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                assert listener != null;
//                listener.onPositionSelected(position);
//            }
//        });
        return convertView; // super.getView(position, convertView, parent);
    }

    private static class ViewItemHelper {
        private TextView textView;
        private ImageView imageView;

        public ViewItemHelper(TextView textView, ImageView imageView){
            this.textView = textView;
            this.imageView = imageView;
        }

        public void addIconView(Context context){
            SVG svgIconParser = null;
            try {
                svgIconParser = SVG.getFromResource(context, R.raw.btn_arrow_go_red);
            } catch (SVGParseException e) {
                e.printStackTrace();
            }
            Bitmap bitmapCreated = Bitmap.createBitmap(512, 512, Bitmap.Config.ARGB_8888);
            Canvas canvasCreated = new Canvas(bitmapCreated);
            svgIconParser.renderToCanvas(canvasCreated);
            this.imageView.setImageBitmap(bitmapCreated);
        }
    }
}
