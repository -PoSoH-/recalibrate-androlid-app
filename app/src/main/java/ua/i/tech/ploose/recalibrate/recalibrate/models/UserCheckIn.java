package ua.i.tech.ploose.recalibrate.recalibrate.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;

/**
 * Created by Developer on 29.12.2016.
 */
@IgnoreExtraProperties
public class UserCheckIn {

    private float Weight = -1.0F;  // Масса
    private float Neck = -1.0F;    // Шия
    private float Chest = -1.0F;   // Грудина
    private float Waist = -1.0F;   // Талія
    private float Hips = -1.0F;    // Бедра
    private float Thighs = -1.0F;  // Ляжки
    private String goal = "";      // Мета сіє некст век (по замовчуванню береться із користувача)
    private float progressWeight = 0.0F;   // позитивна негативна зміна прогресу
    private float progressNeck = 0.0F;
    private float progressChest = 0.0F;
    private float progressWaist = 0.0F;
    private float progressHips = 0.0F;
    private float progressThighs = 0.0F;
    private float bodyFat = 0.0F;          // кількість жиру
    private float progressBodyFat = 0.0F;  // прогрес +/- жир
    private float LBM = 0.0F;              // м'язова маса (всьо остальне крім жиру................)
    private float progressLBM = 0.0F;      // пргрес +/- м'язової маси
    private int processAcceptable = -1;     /// type progress information
    private float calorieAdjustmen = 0.0F;  /// calorie adjustment
    private Map<String, Calories> calories = new HashMap<>();
    private long created;          // час створення
    private String id = Recalibrate.Helper.createObjectId();

    private List<Record> records = new LinkedList<>();

    public UserCheckIn(){}

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {

        this.created = created;
    }

    public float getWeight() {
        return Weight;
    }

    public void setWeight(float weight) {
        if(weight>0)
            this.Weight = weight;
    }

    public float getNeck() {
        return Neck;
    }

    public void setNeck(float neck) {
        if(neck>0)
            Neck = neck;
    }

    public float getChest() {
        return Chest;
    }

    public void setChest(float chest) {
        if(chest>0)
         Chest = chest;
    }

    public float getWaist() {
        return Waist;
    }

    public void setWaist(float waist) {
        if(waist>0)
            Waist = waist;
    }

    public float getHips() {
        return Hips;
    }

    public void setHips(float hips) {
        if(hips>0)
            Hips = hips;
    }

    public float getThighs() {
        return Thighs;
    }

    public void setThighs(float thighs) {
        if(thighs>0)
            Thighs = thighs;
    }

    public String getGoal() {
        return goal;
    }

    public void setGoal(String goal) {
        this.goal = goal;
    }

    public float getProgressWeight() {
        return progressWeight;
    }

    public void setProgressWeight(float progressWeight) {
        this.progressWeight = progressWeight;
    }

    public float getProgressNeck() {
        return progressNeck;
    }

    public void setProgressNeck(float progressNeck) {
        this.progressNeck = progressNeck;
    }

    public float getProgressChest() {
        return progressChest;
    }

    public void setProgressChest(float progressChest) {
        this.progressChest = progressChest;
    }

    public float getProgressWaist() {
        return progressWaist;
    }

    public void setProgressWaist(float progressWaist) {
        this.progressWaist = progressWaist;
    }

    public float getProgressHips() {
        return progressHips;
    }

    public void setProgressHips(float progressHips) {
        this.progressHips = progressHips;
    }

    public float getProgressThighs() {
        return progressThighs;
    }

    public void setProgressThighs(float progressThighs) {
        this.progressThighs = progressThighs;
    }

    public float getBodyFat() {
        return bodyFat;
    }

    public void setBodyFat(float bodyFat) {
        this.bodyFat = bodyFat;
    }

    public float getProgressBodyFat() {
        return progressBodyFat;
    }

    public void setProgressBodyFat(float progressBodyFat) {
        this.progressBodyFat = progressBodyFat;
    }

    public float getLBM() {
        return LBM;
    }

    public void setLBM(float LBM) {
        this.LBM = LBM;
    }

    public float getProgressLBM() {
        return progressLBM;
    }

    public void setProgressLBM(float progressLBM) {
        this.progressLBM = progressLBM;
    }

    public int getProcessAcceptable() {
        return processAcceptable;
    }

    public void setProcessAcceptable(int processAcceptable) {
        this.processAcceptable = processAcceptable;
    }

    public float getCalorieAdjustmen() {
        return calorieAdjustmen;
    }

    public void setCalorieAdjustmen(float calorieAdjustmen) {
        this.calorieAdjustmen = calorieAdjustmen;
    }

    public Map<String, Calories> getCalories() {
        return calories;
    }

    public void setCalories(Map<String, Calories> calories) {
        this.calories = calories;
    }

    public String getId() {
        return id;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public void printfieldValueChekIn(){
        Recalibrate.LOG("CHECK IN VALUE :",
                "\n01 : weight          : " + getWeight()
                        + "\n02 : neck            : " + getNeck()
                        + "\n03 : chest           : " + getChest()
                        + "\n04 : waist           : " + getWaist()
                        + "\n05 : hip             : " + getHips()
                        + "\n06 : thighs          : " + getThighs()
                        + "\n07 : goal            : " + getGoal()
                        + "\n08 : created         : " + getCreated()
                        + "\n09 : weight %        : " + getProgressWeight()
                        + "\n10 : chest %         : " + getProgressChest()
                        + "\n11 : waist %         : " + getProgressWaist()
                        + "\n12 : hip %           : " + getProgressHips()
                        + "\n13 : thighs %        : " + getProgressThighs()
                        + "\n14 : fat             : " + getBodyFat()
                        + "\n15 : fat %           : " + getProgressBodyFat()
                        + "\n16 : LBM             : " + getLBM()
                        + "\n17 : LBM %           : " + getProgressLBM()
                        + "\n18 : calories      Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getCalories()
                        + "\n19 : gram protein  Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getProtein()
                        + "\n20 : %   protein   Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getProteinPercent() + "%"
                        + "\n21 : gramm carb... Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getCarbohydrate()
                        + "\n22 : % carb...     Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getCarbohydratePercent() + "%"
                        + "\n23 : gram fat      Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getFat()
                        + "\n24 : % fat         Y : " + getCalories().get(Recalibrate.Calc.KEY_Y_TRAINING).getFatPercent() + "%"
                        + "\n25 : calories      N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getCalories()
                        + "\n26 : gram protein  N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getProtein()
                        + "\n27 : % protein     N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getProteinPercent() + "%"
                        + "\n28 : gram carb...  N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getCarbohydrate()
                        + "\n29 : % carb...     N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getCarbohydratePercent() + "%"
                        + "\n30 : gram fat      N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getFat()
                        + "\n31 : % fat         N : " + getCalories().get(Recalibrate.Calc.KEY_N_TRAINING).getFatPercent() + "%"
                        + "\n32 : acceptable      : " + getProcessAcceptable()
                        + "\n33 : adjustmen       : " + getCalorieAdjustmen()

                        + "\n34 : record count    : " + getRecords().size()
        );
    }
}
