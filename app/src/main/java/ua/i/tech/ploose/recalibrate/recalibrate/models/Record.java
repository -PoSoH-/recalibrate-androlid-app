package ua.i.tech.ploose.recalibrate.recalibrate.models;

import java.io.Serializable;

import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;

/**
 * Created by root on 27.02.17.
 */

public class Record implements Serializable{

    private final String TAG = Record.class.getSimpleName() + " : ";

    private float carbohydrates = 0.0F;
    private float protein = 0.0F;
    private float fat = 0.0F;
    private int isTraining = 0;
    private boolean isRecord = false;
    private long currentDate = 0L;

    public float getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(float carbohydrates) {
        if(isRecord())
            this.carbohydrates = carbohydrates;
        else
            showLogRecordStatus();
    }

    public float getProtein() {
        return protein;
    }

    public void setProtein(float protein) {
        if(isRecord())
            this.protein = protein;
        else
            showLogRecordStatus();
    }

    public float getFat() {
            return fat;
    }

    public void setFat(float fat) {
        if(isRecord())
            this.fat = fat;
        else
            showLogRecordStatus();
    }

    public int getTraining() {
        return isTraining;
    }

    public void setTraining(int training) {
        if(isRecord())
            isTraining = training;
        else
            showLogRecordStatus();
    }

    public long getCurrentDate() {
//        Recalibrate.LOG(TAG, String.valueOf(this.currentDate));
        return this.currentDate;
    }

    public void setCurrentDate(long currentDate) {
        if(isRecord()) {
            Recalibrate.LOG(TAG, "NEW CREATE DATE" + currentDate);
            this.currentDate = currentDate;
        }
    }

    public void checkRecord(){
        if(!this.isRecord){
            this.isRecord = true;
        }
    }

    public boolean isRecord(){
        return !this.isRecord;
    }

    public boolean getIsRecord(){
        return this.isRecord;
    }

    private void showLogRecordStatus(){
        Recalibrate.LOG(TAG, "Sorry the entry previously made");
    }
}
