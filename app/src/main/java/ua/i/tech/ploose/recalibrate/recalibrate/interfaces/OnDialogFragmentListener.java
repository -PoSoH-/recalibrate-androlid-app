package ua.i.tech.ploose.recalibrate.recalibrate.interfaces;

import java.util.Objects;

import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;
//import ua.i.tech.ploose.recalibrate.recalibrate.models.Workout;

/**
 * Created by Developer on 30.12.2016.
 */

public interface OnDialogFragmentListener {

    void onDialogWorkoutSaveListener(UserTraining objects);

}
