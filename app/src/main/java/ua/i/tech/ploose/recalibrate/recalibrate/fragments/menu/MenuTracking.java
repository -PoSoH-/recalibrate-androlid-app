package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menu;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.activities.MainPref;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.wheelAdapters.UserTrainingDateAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnClickButtonListener;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.Record;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

/**
 * Created by Developer on 26.12.2016.
 */

public class MenuTracking extends Fragment {

    private OnClickButtonListener listener;

    private List<UserCheckIn> checkIn = new LinkedList<>();
    private List<Record> records = null; //new LinkedList<>();
//    private Record currentRecord = null;

    private int selectDay = 0;
//    private int isTrainingDay = 0;
    private TextView btnTypeDay[];
    private TextView carbohydrates;
    private TextView protein;
    private TextView fat;
    private TextView addNewRecord;
    private WheelView wheelFrequency;

    private String txtCarb = null;
    private String txtProt = null;
    private String txtFat_ = null;
    private Integer tempSelected = null;

    private UserTrainingDateAdapter durationAdapter =  null;

    public static Fragment createFragment(){
        return new MenuTracking();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnClickButtonListener)
            listener = (OnClickButtonListener) context;
        else
            throw new ClassCastException("No implement interface "
                    + OnClickButtonListener.class.getSimpleName());

        Backend.loadUserCheckIn(new Backend.OnRecalibrateCheckInListener() {
            @Override
            public void onSuccess(List<UserCheckIn> result) {
                checkIn = result;
                records = checkIn.get(checkIn.size()-1).getRecords();
                if((records.get(records.size()-1).getCurrentDate()/1000)
                        < (Recalibrate.Helper.createCorrectDate()/1000)){
                    Record record = new Record();
                    record.setCurrentDate(Recalibrate.Helper.createCorrectDate());
                    records.add(record);
                }
                createdWheelView();
                wheelFrequency.setCurrentItem(0);
                durationAdapter.setSelectedItem(0);
                selectDay = records.size()-1;
                durationAdapter.notifyDataChangedEvent();
                updateUI();
            }

            @Override
            public void onError(int error) {

            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_include_add_menu, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btnTypeDay = new TextView[]{
                (TextView) view.findViewById(R.id.mainTrackingTrainingDay),
                (TextView) view.findViewById(R.id.mainTrackingTrainingDayNot)
        };

        btnTypeDay[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempSelected = 1;
//                Recalibrate.Helper.hideKeyboard((MainPref)getActivity());
//                records.get(selectDay).setTraining(1);
                updateUI();
            }
        });
        btnTypeDay[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tempSelected = -1;
//                Recalibrate.Helper.hideKeyboard((MainPref)getActivity());
//                records.get(selectDay).setTraining(-1);
                updateUI();
            }
        });

        carbohydrates = (TextView) view.findViewById(R.id.introTrackingCarbohydrates);
        protein = (TextView) view.findViewById(R.id.introTrackingProtein);
        fat = (TextView) view.findViewById(R.id.introTrackingFat);
        this.carbohydrates.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtCarb = s.toString();
                resetTempValue();
            }
        });
        this.protein.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtProt = s.toString();
                resetTempValue();
            }
        });
        this.fat.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                txtFat_ = s.toString();
                resetTempValue();
            }
        });

        addNewRecord = (TextView) view.findViewById(R.id.mainTrackingAddNewRecord);
        addNewRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkVerifyDataField()){
                    records.get(selectDay)
                            .setCarbohydrates(Float.valueOf(carbohydrates.getText().toString()));
                    records.get(selectDay)
                            .setProtein(Float.valueOf(protein.getText().toString()));
                    records.get(selectDay)
                            .setFat(Float.valueOf(fat.getText().toString()));
                    records.get(selectDay).setTraining(tempSelected);
                    records.get(selectDay).checkRecord();
//                    records.get(selectDay).setCurrentDate(Recalibrate.Helper.createCorrectDate());
                    listener.onClickListenerSave(checkIn);
                }
            }
        });

        wheelFrequency = (WheelView) view.findViewById(R.id.mainTrainingSelectedDay);
        createdWheelView();
        updateUI();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    private void updateUI(){

        if(records == null) return;

        if(records.get(selectDay).getTraining() == 0){
            if(tempSelected == null)
                disableAll();
            else{
                checkTrainingValue(tempSelected);
            }
        }else {
            checkTrainingValue(records.get(selectDay).getTraining());
        }

        if(records.get(selectDay).getCarbohydrates() != 0.0F)
            this.carbohydrates.setText(String.valueOf((int)records.get(selectDay).getCarbohydrates()));
        else {
            if(txtCarb == null)
                this.carbohydrates.setText("");
            else
                this.carbohydrates.setText(txtCarb);
        }
        if(records.get(selectDay).getProtein() != 0.0F)
            this.protein.setText(String.valueOf((int)records.get(selectDay).getProtein()));
        else {
            if(txtProt == null)
                this.protein.setText("");
            else
                this.protein.setText(txtProt);
        }
        if(records.get(selectDay).getFat() != 0.0F)
            this.fat.setText(String.valueOf((int)records.get(selectDay).getFat()));
        else {
            if(txtFat_ == null)
                this.fat.setText("");
            else
                this.fat.setText(txtFat_);
        }

        if(records.get(selectDay).getIsRecord()){
            addNewRecord.setVisibility(View.INVISIBLE);
        }else{
            addNewRecord.setVisibility(View.VISIBLE);
        }

    }

    private void createdWheelView(){
        if(checkIn.size() > 0) {
            createDuration(getContext()
                    , wheelFrequency
                    , Recalibrate.Helper.getAllDays(checkIn.get(checkIn.size() - 1).getRecords()));
            wheelFrequency.setCurrentItem(records.size() - 1);
        }
    }

    private void createDuration(Context context, WheelView duration, String []data){
        duration.setWheelViewBackgroundResource(android.R.color.transparent);
        duration.disableShadows();
        duration.setColorSeparatorLine(R.color.colorTransparent);
        duration.setVisibleItems(3);
        durationAdapter = new UserTrainingDateAdapter(context,
                checkIn.get(checkIn.size()-1).getRecords(),
                selectDay);
        duration.setViewAdapter(durationAdapter);
//        duration.setCurrentItem(durationAdapter.getSelectedItem());
        durationAdapter.notifyDataChangedEvent();
        duration.addChangingListener(new OnWheelChangedListener() {
            @Override
            public void onChanged(WheelView wheel, int oldValue, int newValue) {
                try {
                    Recalibrate.Helper.hideKeyboard((MainPref)getActivity());
                    TextView timeWheelOldItem = (TextView) wheel.getViewAdapter().
                            getItemView(oldValue).findViewById(R.id.menuSettingViewValue);
                    TextView timeWheelNewItem = (TextView) wheel.getViewAdapter().
                            getItemView(newValue).findViewById(R.id.menuSettingViewValue);
                    timeWheelOldItem.setTextColor(getResources().getColor(R.color.colorBlack));
                    timeWheelNewItem.setTextColor(getResources().getColor(R.color.colorRed_B));
                    timeWheelOldItem.setTextSize(getResources()
                            .getDimension(R.dimen.intro_text_size_out_focus));
                    timeWheelNewItem.setTextSize(getResources()
                            .getDimension(R.dimen.intro_text_size_in_focus));
                    selectDay = (records.size()-1) - newValue;
                    Recalibrate.LOG("UPDATE WHELL", "..." + selectDay);
                    updateUI();
                    durationAdapter.setSelectedItem(newValue);
                } catch (NullPointerException e) {
                    Log.e("GOAL", " ERROR " + e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean checkVerifyDataField(){
        if(TextUtils.isEmpty(carbohydrates.getText()) || TextUtils.equals(carbohydrates.getText(), "")){
            Toast.makeText(getContext(),
                    getString(R.string.main_training_error_carbohydrates),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(protein.getText()) || TextUtils.equals(protein.getText(), "")){
            Toast.makeText(getContext(),
                    getString(R.string.main_training_error_protein),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if(TextUtils.isEmpty(fat.getText()) || TextUtils.equals(fat.getText(), "")){
            Toast.makeText(getContext(),
                    getString(R.string.main_training_error_fat),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        if( tempSelected == 0){
            Toast.makeText(getContext(),
                    getString(R.string.main_training_error_day_selected),
                    Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void enableTrainingN(){
        btnTypeDay[1].setTextColor(getResources().getColor(R.color.colorWhite));
        btnTypeDay[1].setBackground(getResources().getDrawable(R.drawable.background_rect_red_right));
        btnTypeDay[0].setTextColor(getResources().getColor(R.color.colorRed_B));
        btnTypeDay[0].setBackgroundColor(getResources().getColor(R.color.colorTransparent));
    }
    private void enableTrainingY(){
        btnTypeDay[0].setTextColor(getResources().getColor(R.color.colorWhite));
        btnTypeDay[0].setBackground(getResources().getDrawable(R.drawable.background_rect_red_left));
        btnTypeDay[1].setTextColor(getResources().getColor(R.color.colorRed_B));
        btnTypeDay[1].setBackgroundColor(getResources().getColor(R.color.colorTransparent));
    }
    private void disableAll(){
        btnTypeDay[0].setTextColor(getResources().getColor(R.color.colorRed_B));
        btnTypeDay[0].setBackgroundColor(getResources().getColor(R.color.colorTransparent));
        btnTypeDay[1].setTextColor(getResources().getColor(R.color.colorRed_B));
        btnTypeDay[1].setBackgroundColor(getResources().getColor(R.color.colorTransparent));
    }
    private void checkTrainingValue(int value){
        if(value == 1)
            enableTrainingY();
        else
            enableTrainingN();
    }

    private void resetTempValue(){
        if(records.get(selectDay).getCarbohydrates() > 0
                ||records.get(selectDay).getProtein() > 0
                ||records.get(selectDay).getFat() > 0
                ||records.get(selectDay).getTraining() != 0){
            txtCarb = null;
            txtProt = null;
            txtFat_ = null;
            tempSelected = null;
        }
    }
}
