package ua.i.tech.ploose.recalibrate.recalibrate.models;

import com.google.firebase.database.IgnoreExtraProperties;

import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;

/**
 * Created by Developer on 29.12.2016.
 */

@IgnoreExtraProperties
public class UserTraining {

    private String id; // = Recalibrate.Helper.createObjectId();
    private String UserId;
    private String Duration;
    private String Intensity;
    private String Frequency;
    private long created = System.currentTimeMillis();

    public UserTraining(){}

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getIntensity() {
        return Intensity;
    }

    public void setIntensity(String intensity) {
        Intensity = intensity;
    }

    public String getFrequency() {
        return Frequency;
    }

    public void setFrequency(String frequency) {
        Frequency = frequency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public void printFieldValueTraining(){
        Recalibrate.LOG("TRAINING VALUE", " : "
                + "\n01. user id   : " + getUserId()
                + "\n02. id        : " + getId()
                + "\n03. duration  : " + getDuration()
                + "\n04. intensity : " + getIntensity()
                + "\n05. frequency : " + getFrequency()
                + "\n06. created   : " + getCreated()
        );

    }
}
