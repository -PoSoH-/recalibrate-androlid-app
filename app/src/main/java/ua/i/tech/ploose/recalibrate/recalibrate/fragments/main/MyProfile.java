package ua.i.tech.ploose.recalibrate.recalibrate.fragments.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;
import ua.i.tech.ploose.recalibrate.recalibrate.adapters.MenuListAdapter;
import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.CustomAdapterListener;

/**
 * Created by Developer on 26.12.2016.
 */

public class MyProfile extends Fragment{

    private final String TAG = MyProfile.class.getSimpleName() + " : ";

    private CustomAdapterListener listener;

    public static Fragment createFragment(){
        return new MyProfile();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof CustomAdapterListener)
            listener = (CustomAdapterListener) context;
        else
            throw new ClassCastException("Error implements interface - CustomAdapterListener...");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_table_my_profile_view, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Recalibrate.LOG(TAG, "View Created " + System.currentTimeMillis());
        String data[] = getResources().getStringArray(R.array.main_profile_list);
        List info = new LinkedList();
        Collections.addAll(info, data);
        ListView myProfile = (ListView) view.findViewById(R.id.mainProfileView);
//        ArrayAdapter<String> profileAdapter = new ArrayAdapter<String>(getContext(),
//                R.layout.item_profile_view,
//                info);

        MenuListAdapter menuAdapter = new MenuListAdapter(getContext(), info);
        myProfile.setAdapter(menuAdapter);
        myProfile.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                assert listener!=null;
                listener.onPositionSelected(i);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Recalibrate.LOG(TAG, "Activity Created " + System.currentTimeMillis());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
