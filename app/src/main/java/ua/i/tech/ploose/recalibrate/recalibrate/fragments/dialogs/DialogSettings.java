//package ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs;
//
//import android.app.Dialog;
//import android.content.Context;
//import android.graphics.Point;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v4.app.DialogFragment;
//import android.support.v4.app.FragmentActivity;
//import android.support.v4.app.FragmentTransaction;
//import android.view.View;
//import android.view.Window;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.shawnlin.numberpicker.NumberPicker;
//
//import ua.i.tech.ploose.recalibrate.recalibrate.R;
//import ua.i.tech.ploose.recalibrate.recalibrate.interfaces.OnDialogFragmentListener;
//import ua.i.tech.ploose.recalibrate.recalibrate.models.UserTraining;
//
//public class DialogSettings extends DialogFragment {
//
//    private NumberPicker pickerDuration;
//    private NumberPicker pickerIntensity;
//    private NumberPicker pickerFrequency;
//    private String dataDuration[];
//    private String dataIntensity[];
//    private String dataFrequency[];
//    private int selectDuration = 0;
//    private int selectIntensity = 0;
//    private int selectFrequency = 0;
//    private OnDialogFragmentListener dialogFragmentListener;
//    private UserTraining workout = new UserTraining();
//
//    public static void show(FragmentActivity fragmentActivity){ //}, final String userID, final String urlPhoto, final String userName) {
//        DialogSettings dialog = new DialogSettings();
//        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
//        ft.add(dialog, Dialog.class.getSimpleName());
//        ft.commitAllowingStateLoss();
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if(context instanceof OnDialogFragmentListener)
//            dialogFragmentListener = (OnDialogFragmentListener) context;
//        else
//            throw new ClassCastException(getString(R.string.error_implements_interface)
//                    + " OnDialogFragmentListener");
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
////        listener = null;
//    }
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        Dialog dialog = new Dialog(getActivity());
//        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_settings_view,
//                null);
//
//        pickerDuration = (NumberPicker) view
//                .findViewById(R.id.introNumberPickerDuration);
//        pickerIntensity = (NumberPicker) view
//                .findViewById(R.id.introNumberPickerIntensity);
//        pickerFrequency = (NumberPicker) view
//                .findViewById(R.id.introNumberPickerFrequency);
//
//        final int maxMin = 120;
//        final int stepMin = 5;
//        int step = 5;
//        int position = 1;
//        int count = maxMin / stepMin + 1;
//        dataDuration = new String[count];
//
//        while (step <= maxMin){
//            dataDuration[position] = String.valueOf(step);
//            step += stepMin;
//            position ++;
//        }
//
//        dataDuration[0] = getString(R.string.intro_settings_workout_duration_list);
//
//        pickerDuration.setMaxValue(dataDuration.length);
//        pickerDuration.setTextSize(32F);
//        pickerDuration.setTextColor(getResources().getColor(R.color.colorRed_B));
//        pickerDuration.setTextNoColor(getResources().getColor(R.color.colorWhite));
//        pickerDuration.setDividerColor(getResources().getColor(R.color.colorWhite));
//        pickerDuration.setDisplayedValues(dataDuration);
//        pickerDuration.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
//                selectDuration = newVal;
//            }
//        });
//
//        dataIntensity = getResources().getStringArray(R.array.intro_settings_collection_intensity);
//        pickerIntensity.setMaxValue(dataIntensity.length);
//        pickerIntensity.setTextSize(32F);
//        pickerIntensity.setTextColor(getResources().getColor(R.color.colorRed_B));
//        pickerIntensity.setTextNoColor(getResources().getColor(R.color.colorWhite));
//        pickerIntensity.setDividerColor(getResources().getColor(R.color.colorWhite));
//        pickerIntensity.setDisplayedValues(dataIntensity);
//        pickerIntensity.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
//                selectIntensity = newVal;
//            }
//        });
//
//        dataFrequency = getResources().getStringArray(R.array.intro_settings_collection_frequency);
//        pickerFrequency.setMaxValue(dataFrequency.length);
//        pickerFrequency.setTextSize(32F);
//        pickerFrequency.setTextColor(getResources().getColor(R.color.colorRed_B));
//        pickerFrequency.setTextNoColor(getResources().getColor(R.color.colorWhite));
//        pickerFrequency.setDividerColor(getResources().getColor(R.color.colorWhite));
//        pickerFrequency.setDisplayedValues(dataFrequency);
//        pickerFrequency.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
//                selectFrequency = newVal;
//            }
//        });
//
//        TextView save = (TextView) view.findViewById(R.id.introBntWorkoutSave);
//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(changeWorkout()) {
//                    dialogDismiss();
//                }
//            }
//        });
//
//        Point point = new Point();
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_full_rect_transparent);
//        dialog.getWindow().getWindowManager().getDefaultDisplay().getSize(point);
//        dialog.setContentView(view);
//        RelativeLayout base = (RelativeLayout) view.findViewById(R.id.dialogSettingsWorkout);
//        base.getLayoutParams().width = point.x;
//        base.getLayoutParams().height = point.y;
//        return dialog;
//    }
//
//    private boolean changeWorkout(){
//
//        if(selectDuration > 1){
//            workout.setDuration(dataDuration[selectDuration-1]);
//        }else{
//            Toast.makeText(getContext(),
//                    getString(R.string.intro_error_duration),
//                    Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(selectIntensity > 1){
//            workout.setIntensity(dataIntensity[selectIntensity-1]);
//        }else{
//            Toast.makeText(getContext(),
//                    getString(R.string.intro_error_intensity),
//                    Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        if(selectFrequency > 1){
//            workout.setFrequency(dataFrequency[selectFrequency-1]);
//        }else{
//            Toast.makeText(getContext(),
//                    getString(R.string.intro_error_frequency),
//                    Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        return true;
//    }
//
//    private void dialogDismiss(){
//        assert dialogFragmentListener != null;
//        dialogFragmentListener.onDialogWorkoutSaveListener(workout);
//        this.dismiss();
//    }
//
//}
