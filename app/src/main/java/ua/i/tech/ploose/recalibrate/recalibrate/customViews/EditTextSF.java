package ua.i.tech.ploose.recalibrate.recalibrate.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.Recalibrate;

/**
 * Created by Polishchuk Serhii 08.12.2016.
 */
public class EditTextSF extends EditText {

    private final String TAG = EditTextSF.class.getSimpleName() + " ";

    public EditTextSF(Context context) {
        super(context);
    }

    public EditTextSF(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public EditTextSF(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context, attrs);
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {
        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.EditTextSF);
        String customFont = a.getString(R.styleable.EditTextSF_EditableSF);
        setCustomFont(ctx, customFont);
        a.recycle();
    }

    public boolean setCustomFont(Context ctx, String asset) {
        Typeface tf = null;
        try {
//            tf = Typeface.createFromAsset(ctx.getAssets(), asset);
//            tf = Typeface.createFromAsset(ctx.getAssets(),"fonts/brandon/brandon_regular.otf");
            tf = Typeface.createFromAsset(ctx.getAssets(),"fonts/sf/sf-ui-display-regular.ttf");
            setTypeface(tf);
        } catch (Exception e) {
            Recalibrate.LOG("New font","BrandonRegular" + " - " + "Could not get typeface: " + e.getMessage());
            return false;
        }
        return true;
    }
}
