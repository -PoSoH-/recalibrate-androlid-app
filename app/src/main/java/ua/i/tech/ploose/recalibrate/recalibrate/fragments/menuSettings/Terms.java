package ua.i.tech.ploose.recalibrate.recalibrate.fragments.menuSettings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import ua.i.tech.ploose.recalibrate.recalibrate.R;
import ua.i.tech.ploose.recalibrate.recalibrate.activities.Base;
import ua.i.tech.ploose.recalibrate.recalibrate.loaders.Backend;
import ua.i.tech.ploose.recalibrate.recalibrate.models.ProgramInfo;
import ua.i.tech.ploose.recalibrate.recalibrate.models.UserCheckIn;

/**
 * Created by Developer on 26.12.2016.
 */

public class Terms extends Fragment {

    public static Fragment createFragment(){
        return new Terms();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.f_main_menu_settings_c, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView textMacros = (TextView) view.findViewById(R.id.mainSettingsMenuTerms);
        ((Base)getActivity()).showProgressBar();
        Backend.loadTermsValues(new Backend.OnRecalibrateProgramInfo() {
            @Override
            public void onSuccess(ProgramInfo result) {
                if(result != null) {
                    textMacros.setText(result.getTermsConditions());
                    ((Base) getActivity()).hideProgressBar();
                }
            }

            @Override
            public void onError(int error) {
                if(error == Backend.LOAD_ERROR){
                    ((Base) getActivity()).hideProgressBar();
                    Toast.makeText(getContext(), "Loads Error", Toast.LENGTH_SHORT).show();
                }
            }
        });
//        textMacros.setTextSize(22);
    }
}
