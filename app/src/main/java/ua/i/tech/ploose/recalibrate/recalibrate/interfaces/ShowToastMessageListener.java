package ua.i.tech.ploose.recalibrate.recalibrate.interfaces;

/**
 * Created by root on 18.02.17.
 */

public interface ShowToastMessageListener {
    boolean showMessage(int typeMessage);
}
