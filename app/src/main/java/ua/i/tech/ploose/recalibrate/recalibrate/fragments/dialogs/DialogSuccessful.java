package ua.i.tech.ploose.recalibrate.recalibrate.fragments.dialogs;

import android.app.Dialog;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import java.util.Timer;

import ua.i.tech.ploose.recalibrate.recalibrate.R;

public class DialogSuccessful extends DialogFragment {

    private static OnDisableListenerDialog listener = null;

    public static void show(FragmentActivity fragmentActivity, OnDisableListenerDialog l){ //}, final String userID, final String urlPhoto, final String userName) {
        listener = l;
        DialogSuccessful dialog = new DialogSuccessful();
        FragmentTransaction ft = fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(dialog, Dialog.class.getSimpleName());
        ft.commitAllowingStateLoss();
        fragmentActivity.overridePendingTransition(R.anim.activity_start_start_show_slide
                ,R.anim.activity_start_finish_show_slide);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(getActivity());
        final View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_success_view,
                null);
        Point point = new Point();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(R.drawable.background_full_rect_transparent);
        dialog.getWindow().getWindowManager().getDefaultDisplay().getSize(point);
        dialog.setContentView(view);
        RelativeLayout base = (RelativeLayout) view.findViewById(R.id.dialogSuccessful);
        base.getLayoutParams().width = point.x;
        base.getLayoutParams().height = point.y;
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        autoCancel();
    }

    private void autoCancel(){
        new AsyncTask<Void, Void, Boolean>(){

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if(aBoolean){
                    dissmissView();
                }
            }

            @Override
            protected Boolean doInBackground(Void... voids) {
                long milisecond = 1500L;
                long oldSecond = System.currentTimeMillis();
                long currentSecond = 0;
                boolean isTimer = true;
                while (isTimer){
                    currentSecond = System.currentTimeMillis();
                    if(currentSecond - oldSecond >= milisecond){
                        isTimer = false;
                    }
                }
                return true;
            }
        }.execute();
    }

    private void dissmissView(){
        if(listener != null)
            listener.onSuccess();
        this.dismiss();
    }

    public interface OnDisableListenerDialog{
        void onSuccess();
    }
}
